package com.xt.xiaotan;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;
import com.xt.xiaotan.utils.LogUtils;
import com.xt.xiaotan.utils.cache.CacheManager;

@SuppressLint("Registered")
public class MyApplication extends Application {
    private static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        CacheManager.init(this);
        UMConfigure.init(this,"60179806f1eb4f3f9b7f8661","umeng",UMConfigure.DEVICE_TYPE_PHONE,"");
        UMConfigure.setLogEnabled(true);
        //各个平台的配置
        {
            //微信
            PlatformConfig.setWeixin("wxdc1e388c3822c80b", "3baf1193c85774b3fd9d18447d76cab0");
            //新浪微博(第三个参数为回调地址)
            PlatformConfig.setSinaWeibo("3921700954", "04b48b094faeb16683c32669824ebdad","http://sns.whalecloud.com/sina2/callback");
            //QQ
            PlatformConfig.setQQZone("100424468", "c7394704798a158208a74ab60104f0ba");
        }
        context = this;
    }
    public static Context getContext() {
        return context;
    }
}
