package com.xt.xiaotan.oss;

public interface ProgressCallback <Request, Result> extends CallBack<Request, Result> {
    void onProgress(Request request, long currentSize, long totalSize);
}
