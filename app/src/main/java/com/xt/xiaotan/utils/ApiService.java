package com.xt.xiaotan.utils;


import com.xt.xiaotan.first.model.CountListRequst;
import com.xt.xiaotan.first.model.FirstResult;

import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.first.model.FurnitureRequst;
import com.xt.xiaotan.first.model.GoodsClassRequst;
import com.xt.xiaotan.first.model.GoodsClassResult;

import com.xt.xiaotan.first.model.GoodsRequst;
import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.first.model.SaveFurnitureRequst;
import com.xt.xiaotan.first.model.SaveRoomRequst;
import com.xt.xiaotan.first.model.StatisResult;
import com.xt.xiaotan.first.model.TgoodsSaveRequst;
import com.xt.xiaotan.goods.bean.AddGoodRequest;
import com.xt.xiaotan.goods.bean.FurnitureRequest;
import com.xt.xiaotan.goods.bean.PositionRequest;
import com.xt.xiaotan.goods.bean.StatisticRequest;
import com.xt.xiaotan.goods.bean.TypeBean;
import com.xt.xiaotan.goods.bean.TypeRequest;
import com.xt.xiaotan.login.model.CheckCodeMobileRequest;
import com.xt.xiaotan.login.model.LoginRequest;
import com.xt.xiaotan.login.model.LoginResults;
import com.xt.xiaotan.login.model.OssBean;
import com.xt.xiaotan.login.model.RefreshTokenRequest;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseRequest;
import com.xt.xiaotan.search.model.GoodEntityBean;
import com.xt.xiaotan.search.model.GoodsListBean;
import com.xt.xiaotan.search.model.SearchFilter;
import com.xt.xiaotan.set.InfoModel;
import com.xt.xiaotan.set.SaveInfoRequest;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {



        //手机验证码登录
        @POST("/api/app/user/login")
        Observable<BaseHttpResult<LoginResults>> loginByVer(@Body LoginRequest baseRequest);
        //获取首页轮播图list
        @GET("/api/app/advert/list")
        Observable<BaseHttpResult<List<FirstResult>>> getBannerList();

         //获取首页分类列表
         @POST("/api/app/goodtype/list")
         Observable<BaseHttpResult<List<GoodsClassResult>>> getGoodList(@Body GoodsClassRequst goodsClassRequst);
         //2. 分类列表复杂数据接口
        @POST("/api/app/goodtype/count")
        Observable<BaseHttpResult<String>> getCountList(@Body CountListRequst countListRequst);



         //检查手机号是否存在
        @GET("/api/app/user/check")
        Observable<BaseHttpResult<String>> checkNumberisUser(@Query("mobile") String mobile);

        //获取手机验证码
        @GET("/api/app/user/sms/send")
        Observable<BaseHttpResult<String>> sendMsg(@Query("mobile") String mobile);

        //检查手机验证码
        @POST("/api/app/user/sms/check")
        Observable<BaseHttpResult<LoginResults>> checkMsg(@Body CheckCodeMobileRequest checkCodeMobileRequest);

        //更新用户密码
        @GET("/api/app/user/password/update")
        Observable<BaseHttpResult<String>> upDatePsd(@Query("newPassword") String newPassword);

        //更新用户token
        @POST("/api/app/user/refreshToken")
        Observable<BaseHttpResult<LoginResults>> refreshToken(@Body RefreshTokenRequest token);
        //获取用户信息
        @GET("/api/app/user/info")
        Observable<BaseHttpResult<InfoModel>> getInfo();
        //保存用户信息
    @POST("/api/app/user/save")
    Observable<BaseHttpResult<String>> saveInfo(@Body InfoModel saveInfoRequest);


        //获取用户房间列表
        @GET("/api/app/tposition/list")
        Observable<BaseHttpResult<List<PositionResult>>> getRoomList();

        //获取房间明细
        @GET("/api/app/tposition/get/{id}")
        Observable<BaseHttpResult<PositionResult>> getRoomDes(@Path("id") String id);

        //保存房间
        @POST("/api/app/tposition/save")
        Observable<BaseHttpResult<String>> saveRoom(@Body PositionRequest saveRoomRequst);
        //删除房间
        @GET("/api/app/tposition/delete/{id}")
        Observable<BaseHttpResult<String>> deleteRoom(@Path("id") String id);

        //获取房间下家具列表
        @POST("/api/app/tfurniture/list")
        Observable<BaseHttpResult<List<FurnitureListResult>>> furnitureList(@Body FurnitureRequst furnitureRequst);
       //保存家具
        @POST("/api/app/tfurniture/save")
        Observable<BaseHttpResult<Boolean>> saveFurniture(@Body FurnitureRequest saveFurnitureRequst);
        //删除家具
        @GET("/api/app/tfurniture/delete/{id}")
        Observable<BaseHttpResult<Boolean>> deleteFurniture(@Path("id") String id);

        //获取物品列表页筛选参数
        @GET("/api/app/tgoods/page/filter")
        Observable<BaseHttpResult<SearchFilter>> getFilterGoodsList();
        //获取物品列表
        @POST("/api/app/tgoods/page")
        Observable<BaseHttpResult<GoodsListBean>> getGoodsList(@Body GoodsRequst goodsRequst);

        //获取物品明细
        @GET("/api/app/tgoods/get/{id}")
        Observable<BaseHttpResult<GoodEntityBean>> getGoodMsg(@Path("id") String id);

        //保存更新物品
        @POST("/api/app/tgoods/save")
        Observable<BaseHttpResult<String>> saveGood(@Body AddGoodRequest tgoodsSaveRequst);

        //获取OSStoken

      @GET("/api/app/user/oss")
      Observable<BaseHttpResult<OssBean>> oss();
      @POST ("/api/app/goodtype/list")
      Observable<BaseHttpResult<List<TypeBean>>> goodtype(@Body TypeRequest typeRequest);

      @POST("api/app/goodtype/statistics")
      Observable<BaseHttpResult<StatisResult>> statistics(@Body StatisticRequest typeRequest);




    @GET("api/app/bluetoothconfig/list")
    Observable<BaseHttpResult<StatisResult>> bluetoothconfigList();


    @GET("api/app/device/list")
    Observable<BaseHttpResult> deviceList();







}
