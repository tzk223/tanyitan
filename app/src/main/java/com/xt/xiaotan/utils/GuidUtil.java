package com.xt.xiaotan.utils;

import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

public class GuidUtil {
    private static Context context;
    private static String guid;// = "862107040913188";

    public static void init(Context context) {
        GuidUtil.context = context;
    }

    public static String getGuid() {
        if (TextUtils.isEmpty(guid)) {
            guid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            guid = guid == null ? null : guid.toUpperCase();
            Log.e("guid", String.valueOf(guid));
        }
        return guid;
    }
}
