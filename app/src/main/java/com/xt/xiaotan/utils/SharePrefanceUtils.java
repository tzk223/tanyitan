package com.xt.xiaotan.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePrefanceUtils {

    public static void putStr(Context context, String key, String value){
        SharedPreferences sp = context.getSharedPreferences("file", Context.MODE_PRIVATE);
        sp.edit().putString(key,value).commit();
    }

    public static String getStr(Context context,String key){
        SharedPreferences sp = context.getSharedPreferences("file", Context.MODE_PRIVATE);
        return sp.getString(key,null);
    }
}
