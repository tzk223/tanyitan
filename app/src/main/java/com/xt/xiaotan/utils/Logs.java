package com.xt.xiaotan.utils;

import android.util.Log;

/**

 * @date 2017/3/8
 */

public class Logs {

    private static final String TAG = "Logs";

    public static final boolean LOGGER = true;

    public static void d(String text) {
        if (LOGGER) {
            Log.d("Logs", "-->" + text);
        }
    }

    public static void v(String tag, String msg) {
        if (LOGGER) {
            Log.v(TAG, tag + "-->" + msg);
        }
    }

    public static void d(String tag, String msg) {
        if (LOGGER) {
            Log.d(TAG, tag + "-->" + msg);
        }
    }

    public static void i(String tag, String msg) {
        if (LOGGER) {
            Log.i(TAG, tag + "-->" + msg);
        }
    }

    public static void w(String tag, String msg) {
        if (LOGGER) {
            Log.v(TAG, tag + "-->" + msg);
        }
    }

    public static void e(String tag, String msg) {
        if (LOGGER) {
            Log.e(TAG, tag + "-->" + msg);
        }
    }

    public static void e(String tag, String msg, Throwable tr) {
        if (LOGGER) {
            Log.e(TAG, tag + "-->" + msg);
        }
    }

    /**
     * 截断输出日志
     * @param msg
     */
    public static void eLong(String tag, String msg) {
        if (LOGGER) {
            if (tag == null || tag.length() == 0
                    || msg == null || msg.length() == 0)
                return;

            int segmentSize = 3 * 1024;
            long length = msg.length();
            if (length <= segmentSize) {// 长度小于等于限制直接打印
                Log.e(tag, msg);
            } else {
                while (msg.length() > segmentSize) {// 循环分段打印日志
                    String logContent = msg.substring(0, segmentSize);
                    msg = msg.replace(logContent, "");
                    Log.e(tag, logContent);
                }
                Log.e(tag, msg);// 打印剩余日志
            }
        }
    }

}
