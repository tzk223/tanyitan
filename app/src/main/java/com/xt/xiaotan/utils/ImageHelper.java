package com.xt.xiaotan.utils;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xt.xiaotan.R;

public class ImageHelper {
    /**
     * 1.加载图片,无需手动调用此方法
     * 2.使用@BindingAdapter注解设置自定义属性的名称，imageUrl就是属性的名称，
     * 当ImageView中使用imageUrl属性时，会自动调用loadImage方法，
     *
     * @param imageView ImageView
     * @param url       图片url
     */
    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView imageView, String url) {
        //icon_carme
        RequestOptions options = RequestOptions.circleCropTransform();//圆形图片  好多的图片形式都是这么设置的
        options.placeholder(R.mipmap.icon_carme);//占位图
        options.error(R.mipmap.icon_carme);
        Glide.with(imageView.getContext()). load(url)
                .apply(options)
                .into(imageView);
    }


}
