package com.xt.xiaotan.app;



import com.xt.xiaotan.MyApplication;

import java.io.File;

public class AppConstants {

    /**
     * Path
     */
    public static final String PATH_DATA = MyApplication.getContext().getCacheDir().getAbsolutePath() + File.separator + "data";

    public static final String PATH_CACHE = PATH_DATA + "/NetCache";

    static final String BUGLY_ID = "16e54f8921";
}
