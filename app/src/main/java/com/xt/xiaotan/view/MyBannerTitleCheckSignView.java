package com.xt.xiaotan.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import java.util.List;

public class MyBannerTitleCheckSignView extends LinearLayout {
    private List<String> bannerData;
    public MyBannerTitleCheckSignView(Context context) {
        super(context);
    }

    public MyBannerTitleCheckSignView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }
    public void setData(List<String> bannerData){
        this.bannerData = bannerData;

    }

}
