package com.xt.xiaotan.base;

import android.app.Dialog;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.DialogFragment;

import com.xt.xiaotan.R;
import com.xt.xiaotan.utils.DisplayUtils;
import com.xt.xiaotan.utils.ToastUtils;


public class EditDialog extends DialogFragment implements View.OnClickListener {
    private View view;


    private TextView project_name_tv;

    private TextView cancel_tv;

    private TextView sure_tv;

    private TextView title_tv;

    private EditText content_tv;

    private View line_v;


    private String title, content, sureStr,tweTitle;

    private TextView twe_title_tv;


    private int maxEdit;
    private boolean showCancelBtn = true;

    public void setOnSureClickListener(EditDialog.OnSureClickListener onSureClickListener) {
        this.onSureClickListener = onSureClickListener;
    }

    private EditDialog.OnSureClickListener onSureClickListener;

    public static EditDialog getInstance(String title,
                                         String content,
                                         String sureStr) {
        EditDialog tigsDialog = new EditDialog();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("content", content);
        bundle.putString("sureStr", sureStr);
        tigsDialog.setArguments(bundle);
        return tigsDialog;
    }
    public static EditDialog getInstance(String title,
                                         String content,
                                         String sureStr, boolean showCancelBtn) {
        EditDialog tigsDialog = new EditDialog();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("content", content);
        bundle.putString("sureStr", sureStr);
        bundle.putBoolean("showCancelBtn",showCancelBtn);
        tigsDialog.setArguments(bundle);
        return tigsDialog;
    }
    public static EditDialog getInstance(String title, String tweTitle, int maxEdit,
                                         String content,
                                         String sureStr, boolean showCancelBtn) {
        EditDialog tigsDialog = new EditDialog();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("content", content);
        bundle.putString("sureStr", sureStr);
        bundle.putBoolean("showCancelBtn",showCancelBtn);
        bundle.putString("tweTitle",tweTitle);
        bundle.putInt("maxEdit",maxEdit);
        tigsDialog.setArguments(bundle);
        return tigsDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.custom_dialog2);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // 设置Content前设定
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        view = layoutInflater.inflate(R.layout.edit_dialog, null);
        getAugData();
        findView();
        setStrToView();
        initListener();
        dialog.setContentView(view, new RelativeLayout.LayoutParams(DisplayUtils.dip2px(getActivity(), 280), ViewGroup.LayoutParams.WRAP_CONTENT));
        dialog.setCanceledOnTouchOutside(false); // 外部点击取消
        return dialog;
    }

    public void initListener() {
        cancel_tv.setOnClickListener(this);
        sure_tv.setOnClickListener(this);
    }

    private void getAugData() {
        Bundle bundle = getArguments();
        title = bundle.getString("title");
        content = bundle.getString("content");
        sureStr = bundle.getString("sureStr");
        showCancelBtn = bundle.getBoolean("showCancelBtn",true);
        tweTitle = bundle.getString("tweTitle");
        maxEdit = bundle.getInt("maxEdit");
    }

    private void findView() {
        cancel_tv = view.findViewById(R.id.cancel_tv);
        sure_tv = view.findViewById(R.id.sure_tv);
        title_tv = view.findViewById(R.id.title_tv);
        content_tv = view.findViewById(R.id.content_tv);
        line_v = view.findViewById(R.id.line_v);
        twe_title_tv = view.findViewById(R.id.twe_title_tv);
        setShowCancelBtn();
    }

    private void setShowCancelBtn(){
        if(showCancelBtn == false){
            line_v.setVisibility(View.GONE);
            cancel_tv.setVisibility(View.GONE);
        }
    }

    private void setStrToView(){
        sure_tv.setText(sureStr);
        title_tv.setText(title);
        content_tv.setHint(content);
        content_tv.setMaxEms(maxEdit);
        twe_title_tv.setText(tweTitle);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.cancel_tv) {
            dismiss();
        } else if (v.getId() == R.id.sure_tv) {
            if(onSureClickListener!= null){
                if(TextUtils.isEmpty(content_tv.getText().toString())){
                    ToastUtils.show("请填写家具名称",1000);
                    return;
                }
                dismiss();
                onSureClickListener.onSureClick(content_tv.getText().toString());

            }
        }
    }

    public interface OnSureClickListener{
        void onSureClick(String sureStr);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
