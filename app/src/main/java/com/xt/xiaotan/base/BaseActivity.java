package com.xt.xiaotan.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.gyf.immersionbar.ImmersionBar;
import com.leaf.library.StatusBarUtil;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;
import com.xt.xiaotan.R;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public abstract class BaseActivity extends RxAppCompatActivity {

    private WaitDialog waitDialog = null;

    private Context mContext;
    /**
     * 所有活动的Activity
     */
    private static final List<BaseActivity> mActivitys = new LinkedList<BaseActivity>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();
        mContext = this;
        //setContentView(getLayoutId());
        Intent intent = getIntent();
        if (intent != null)
            getIntent(intent);
        if (useEventBus()) {
            EventBus.getDefault().register(this);//注册eventBus
        }
        mActivitys.add(this);
        initView();
        initData();
        initListener();

    }


    protected void setStatusBar() {
        //设置共同沉浸式样式
        ImmersionBar.with(this).statusBarDarkFont(true).fitsSystemWindows(true).statusBarColor(R.color.white).init();

    }



    /**
     * 是否使用eventBus
     *
     * @return
     */
    protected boolean useEventBus() {
        return false;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (useEventBus()) {
            if (EventBus.getDefault().isRegistered(this)) {
                EventBus.getDefault().unregister(this);//注销eventBus
            }
        }

    }

    /**
     * 通过Class跳转界面
     **/
    public void startActivity(Class<?> cls) {
        startActivity(cls, null);
    }

    /**
     * 通过Class跳转界面
     **/
    public void startActivityForResult(Class<?> cls, int requestCode) {
        startActivityForResult(cls, null, requestCode);
    }

    /**
     * 含有Bundle通过Class跳转界面
     **/
    public void startActivityForResult(Class<?> cls, Bundle bundle, int requestCode) {
        Intent intent = new Intent();
        intent.setClass(this, cls);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, requestCode);
    }

    /**
     * 含有Bundle通过Class跳转界面
     **/
    public void startActivity(Class<?> cls, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(this, cls);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    /**
     * 显示带消息的进度框
     *
     * @param title 提示
     */
    protected void showLoadingDialog(String title) {
        createLoadingDialog();
        if (!waitDialog.isShowing())
            waitDialog.show();
    }

    /**
     * 显示进度框
     */
    protected void showLoadingDialog() {
        createLoadingDialog();
        if (!waitDialog.isShowing())
            waitDialog.show();
    }

    /**
     * 创建LoadingDialog
     */
    private void createLoadingDialog() {
        if (waitDialog != null && waitDialog.isShowing()) {
            return;
        } else {
            waitDialog = new WaitDialog(mContext);
            waitDialog.setCancelable(false);
            waitDialog.show();
        }
    }

    /**
     * 隐藏进度框
     */
    protected void hideLoadingDialog() {
        if (waitDialog != null && waitDialog.isShowing()) {
            waitDialog.dismiss();
        }
    }




    /**
     * 获取布局 Id
     * @return
     */
    protected abstract @LayoutRes
    int getLayoutId();
    /** 获取 Intent 数据 **/
    protected abstract void getIntent(Intent intent);

    /** 初始化View的代码写在这个方法中 */
    protected abstract void initView();

    /** 初始化监听器的代码写在这个方法中 */
    protected abstract void initListener();

    /** 初始数据的代码写在这个方法中，用于从服务器获取数据 */
    protected abstract void initData();

    /**
     * 关闭所有Activity，除了参数传递的Activity
     */
    public static void finishAllAct(BaseActivity except) {
        List<BaseActivity> copy;
        synchronized (mActivitys) {
            copy = new ArrayList<BaseActivity>(mActivitys);
        }
        for (BaseActivity activity : copy) {
            if (activity != except) {
                if(!activity.isFinishing()){
                    activity.finish();
                }
            }
        }
    }

}
