package com.xt.xiaotan.base;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.LayoutParams;

import com.xt.xiaotan.R;


/**
 * Created by XiaoHaiLin on 2017/2/26.
 */

public class WaitDialog extends Dialog {

    public LayoutInflater inflater = null;
    public View layout = null;
    public Intent intent = null;
    public Context mContext = null;

    public LayoutParams layoutParams = null;

    public WaitDialog(Context context) {
        super(context, R.style.NoTitleNoFrameDialog);
        mContext = context;
        initView();
        setContentView(layout);
        // 设置window属性
        layoutParams = getWindow().getAttributes();
        layoutParams.gravity = Gravity.CENTER;
        layoutParams.dimAmount = 0; // 去背景遮盖
        layoutParams.alpha = 1.0f;
        getWindow().setAttributes(layoutParams);

    }

    @Override
    public void dismiss() {
        if(mContext!=null){
            super.dismiss();
        }
    }

    /**
     *
     */
    private void initView() {
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = inflater.inflate(R.layout.custom_progressbar, null);
    }



}
