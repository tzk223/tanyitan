package com.xt.xiaotan.net.token;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.xt.xiaotan.login.model.LoginResults;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.utils.RetrofitUtils;
import com.xt.xiaotan.utils.cache.CacheManager;

import java.io.IOException;
import java.nio.charset.Charset;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import okhttp3.Authenticator;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.Route;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Call;

/**
 * Token失效的处理方案二，如果服务端没有遵循设计规范，可以尝试使用如下方法
 * 使用方法：addInterceptor(new TokenInterceptor());
 */
public class TokenInterceptor implements Interceptor {

    private static final Charset UTF8 = Charset.forName("UTF-8");




    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();

        // try the request
        Response originalResponse = chain.proceed(request);

        /**
         * 通过如下方法提前获取到请求完成的数据
         */
        ResponseBody responseBody = originalResponse.body();
        BufferedSource source = responseBody.source();
        source.request(Long.MAX_VALUE); // Buffer the entire body.
        Buffer buffer = source.buffer();
        Charset charset = UTF8;
        MediaType contentType = responseBody.contentType();
        if (contentType != null) {
            charset = contentType.charset(UTF8);
        }

        String bodyString = buffer.clone().readString(charset);

        Log.e("body---------->", bodyString);
        Log.e("body---------->","isTokenExpired(bodyString) = "+isTokenExpired(bodyString));

        if(isTokenExpired(bodyString)){

            Log.e("body ---->","开始newToken = ");
//            String newToken = getNewToken();
//            Log.e("body ---->","newToken = "+newToken);
//            if(TextUtils.isEmpty(newToken)){
//                return null;
//            }
//            // create a new request and modify it accordingly using the new token
//            CacheManager.getInStance().put(CacheManager.TOKENKEY,newToken);
//            Request newRequest = request.newBuilder().header("token", newToken).build();
//
            originalResponse.body().close();
//            // retry the request
            return chain.proceed(request);

        }

//        if ("判断是否过期"){// 根据和服务端的约定判断Token是否过期
//
//            // 通过一个特定的接口获取新的Token，此处要用到同步的Retrofit请求
//
//            // 获取到新的Token
//            String token = "call.execute().body()";
//
//            // create a new request and modify it accordingly using the new token
//            Request newRequest = request.newBuilder().header("token", token).build();
//
//            originalResponse.body().close();
//            // retry the request
//            return chain.proceed(newRequest);
//        }

        // otherwise just pass the original response on
        return originalResponse;
    }


    /**
     * 根据Response，判断Token是否失效
     *
     * @return
     */
    private boolean isTokenExpired(String resultStr) {
        BaseHttpResult requestCode = new Gson().fromJson(resultStr, BaseHttpResult.class);
        //err==3 token过期
        if ("1".equals(requestCode.getCode())) {

            return true;
        }

        return false;
    }

//    private String getNewToken() {
//        // 通过一个特定的接口获取新的token，此处要用到同步的retrofit请求
//        Call<BaseHttpResult<LoginResults>> call = RetrofitUtils.getHttpService().refreshToken(CacheManager.getInStance().getAsString(CacheManager.TOKENKEY));
//
//
//        //要用retrofit的同步方式
//        BaseHttpResult<LoginResults> newToken = null;
//        try {
//            newToken = call.execute().body();
//            if(!newToken.getCode().equals("0")){
//                return "";
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//            return "";
//        }
//
//
//        return newToken.getData().getToken();
//    }



}