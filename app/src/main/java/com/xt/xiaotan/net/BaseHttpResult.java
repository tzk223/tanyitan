package com.xt.xiaotan.net;

import java.io.Serializable;

public class BaseHttpResult<T> implements Serializable {
    private static final long serialVersionUID = 2690553609250007325L;
    public static final int SUCCESS_CODE = 0;

//    private int status;
//    private String message;
//    private T data;
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public T getData() {
//        return data;
//    }
//
//    public void setData(T data) {
//        this.data = data;
//    }
//
//
//    /**
//     * 正常返回
//     *
//     * @return
//     */
//    public boolean isSuccessFul() {
//        return getStatus() == SUCCESS_CODE;
//    }
//
//    @Override
//    public String toString() {
//        return "BaseHttpResult{" +
//                "status=" + status +
//                ", message='" + message + '\'' +
//                ", data=" + data +
//                '}';
//    }




    private T data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return msg;
    }

    public void setMessage(String message) {
        this.msg = message;
    }

    private String code;

    private String msg;

    public boolean isError() {
        return !"0".equals(code);
    }

    public T getData() {
        return data;
    }

    @Override
    public String toString() {
        return "BaseHttpResult{" +
                "data=" + data +
                ", code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }

    /**
     * 正常返回
     *
     * @return
     */
    public boolean isSuccessFul() {
        return !isError();
    }
}
