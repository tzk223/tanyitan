package com.xt.xiaotan.search.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xt.xiaotan.R;
import com.xt.xiaotan.bluetooth.BlueToothSearchActivity;
import com.xt.xiaotan.goods.AddGoodsActivity;
import com.xt.xiaotan.search.model.GoodEntityBean;


import java.util.List;

public class GoodsAdapter extends RecyclerView.Adapter<GoodsAdapter.ViewHolder> {
    public Context context;

    private List<GoodEntityBean> list;
    public GoodsAdapter(Context context){
        this.context = context;
    }

    public void setData(List<GoodEntityBean> list){
        this.list = list;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public GoodsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.goods_item, parent, false);
        return new GoodsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GoodsAdapter.ViewHolder holder, int position) {
        GoodEntityBean goodEntityBean = list.get(position);
        Glide.with(context).load(goodEntityBean.photoUrl1).apply(new RequestOptions().error(R.drawable.ic_launcher_background)).into(holder.image_iv);
        holder.name_tv.setText(goodEntityBean.goodsName);
        if(goodEntityBean.goodsTypeSecond>0){
            holder.type_tv.setText("二级分类");
        }else{
            holder.type_tv.setText("一级分类");
        }
        holder.position_tv.setText(goodEntityBean.furnitureName);
        holder.time_tv.setText(goodEntityBean.createDateStr);
        holder.content_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddGoodsActivity.gotoAddGoodsActivity(context,goodEntityBean);
            }
        });
        holder.search_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BlueToothSearchActivity.gotoBlueToothSearchActivity(context,goodEntityBean);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list!=null?list.size():0;
    }
    class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView image_iv;
        public TextView name_tv;
        public TextView type_tv;
        public TextView position_tv;
        public TextView time_tv;
        public TextView search_tv;
        public LinearLayout content_ll;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image_iv = itemView.findViewById(R.id.image_iv);
            name_tv = itemView.findViewById(R.id.name_tv);
            type_tv = itemView.findViewById(R.id.type_tv);
            position_tv = itemView.findViewById(R.id.position_tv);
            time_tv = itemView.findViewById(R.id.time_tv);
            search_tv = itemView.findViewById(R.id.search_tv);
            content_ll = itemView.findViewById(R.id.content_ll);
        }
    }
}
