package com.xt.xiaotan.search.searchmvp;

import android.util.Log;

import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.first.model.GoodsRequst;
import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.search.model.GoodEntityBean;
import com.xt.xiaotan.search.model.GoodsListBean;
import com.xt.xiaotan.search.model.SearchFilter;

import java.util.List;

/**
 * Created by tiandi
 * on 2020/11/10
 */
public class SearchPresenter extends BasePresenter {
    public SearchContract.View view;
    public SearchModel mainModel;
    @Override
    protected IModel createModel() {
        mainModel = new SearchModel();
        return mainModel;
    }
    public SearchPresenter(SearchContract.View view){
        this.view = view;
    }





    public void getGoodsList(GoodsRequst goodsRequst){
        addDispose(mainModel.getGoodList(goodsRequst), new BaseObserver<GoodsListBean>(view) {
            @Override
            public void onSuccess(BaseHttpResult<GoodsListBean> result) {

                Log.e("tag","onSuccess"+result.getCode());
                view.getGoodsList(result.getData().records);


            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {
                Log.e("tag","onFailure"+errMsg);

            }

        });

    }

    public void getgoodsFilter(){
        addDispose(mainModel.getGoodsFilter(), new BaseObserver<SearchFilter>(view) {
            @Override
            public void onSuccess(BaseHttpResult<SearchFilter> result) {
                view.getGoodFilter(result.getData());

                Log.e("tag","onSuccess"+result.getCode());



            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {
                Log.e("tag","onFailure"+errMsg);

            }

        });
    }


}
