package com.xt.xiaotan.search.searchmvp;

import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.first.model.GoodsRequst;
import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.mvp.IView;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.search.model.GoodEntityBean;
import com.xt.xiaotan.search.model.GoodsListBean;
import com.xt.xiaotan.search.model.SearchFilter;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public interface SearchContract {
    public interface View extends IView {
        void getGoodFilter(SearchFilter searchFilter);

        void getGoodsList(List<GoodEntityBean> list);

    }

    public interface Model extends IModel {

        Observable<BaseHttpResult<SearchFilter>> getGoodsFilter();
        Observable<BaseHttpResult<GoodsListBean>> getGoodList(GoodsRequst goodsRequst);
    }
}
