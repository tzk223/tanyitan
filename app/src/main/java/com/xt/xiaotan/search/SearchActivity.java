package com.xt.xiaotan.search;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.first.model.GoodsRequst;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.search.adapter.GoodsAdapter;
import com.xt.xiaotan.search.model.Categorie;
import com.xt.xiaotan.search.model.GoodEntityBean;
import com.xt.xiaotan.search.model.SearchFilter;
import com.xt.xiaotan.search.searchmvp.SearchContract;
import com.xt.xiaotan.search.searchmvp.SearchPresenter;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends BaseMvpActivity implements View.OnClickListener , SearchContract.View {
    private RecyclerView goods_rv;
    private ImageView goods_type_iv;
    private ImageView back_iv;
    private List<GoodEntityBean> list;
    private ConstraintLayout title_cl;
    private EditText key_et;
    private SearchPresenter searchPresenter;
    private  GoodsTypePopupWindow goodsTypePopupWindow;

    private GoodsRequst goodsRequst;

    private  GoodsAdapter  goodsAdapter;

    SearchFilter searchFilter;


    @Override
    protected BasePresenter createPresenter() {
        searchPresenter = new SearchPresenter(this);
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.search_activity;
    }

    @Override
    protected void getIntent(Intent intent) {

    }

    @Override
    protected void initView() {
        setContentView(R.layout.search_activity);
        goods_rv = findViewById(R.id.goods_rv);
        goods_type_iv = findViewById(R.id.goods_type_iv);
        title_cl = findViewById(R.id.title_cl);
        back_iv = findViewById(R.id.back_iv);
        key_et = findViewById(R.id.key_et);
    }

    @Override
    protected void initListener() {
          goods_type_iv.setOnClickListener(this);
        back_iv.setOnClickListener(this);
        key_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()>1){
                    goodsRequst.goodsName = s.toString();
                    searchPresenter.getGoodsList(goodsRequst);
                }
            }
        });
    }

    private void initGoodsRequest(){
        goodsRequst = new GoodsRequst();
        goodsRequst.current = 1;
        goodsRequst.size = 10;
    }
    @Override
    protected void initData() {
        initGoodsRequest();
        searchPresenter.getGoodsList(goodsRequst);
        list = new ArrayList<>();
        goodsAdapter = new GoodsAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        goods_rv.setLayoutManager(linearLayoutManager);
        goods_rv.setAdapter(goodsAdapter);

    }

    @Override
    public void showError(String msg) {

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.goods_type_iv){
            if(goodsTypePopupWindow==null){
                goodsTypePopupWindow = new GoodsTypePopupWindow(this);
            }
            goodsTypePopupWindow.setOnGetCheckDataListener(new GoodsTypePopupWindow.OnGetCheckDataListener() {
                @Override
                public void onGetCheckData(List<Categorie> categories) {
                    if(categories.size()>0){
                        goodsRequst.goodsType = categories.get(0).getParentId()+"";
                        goodsRequst.goodsTypeSecond = categories.get(0).getId()+"";
                    }
                }
            });
            goodsTypePopupWindow.showAsDropDown(title_cl);
            if(searchFilter!=null){
                goodsTypePopupWindow.setData(searchFilter);
            }else {
                searchPresenter.getgoodsFilter();
            }
        }else if(v.getId() == R.id.back_iv){
            finish();
        }
    }

    @Override
    public void getGoodFilter(SearchFilter searchFilter) {
        this.searchFilter = searchFilter;
        goodsTypePopupWindow.setData(searchFilter);
    }

    @Override
    public void getGoodsList(List<GoodEntityBean> list) {
        goodsAdapter.setData(list);
    }
}
