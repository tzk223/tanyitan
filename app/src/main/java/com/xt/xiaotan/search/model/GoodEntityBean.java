package com.xt.xiaotan.search.model;

import java.io.Serializable;

public class GoodEntityBean implements Serializable {


    /**
     * id : 17
     * goodsName : 在家里玩
     * goodsType : 7
     * goodsTypeSecond : 7
     * positionId : 15
     * furnitureId : 23
     * goodsRfid : 23355
     * price : 23.0
     * photoCode1 : null
     * photoUrl1 : http://imagess.icloudinn.net/AddGoosActivity1611586480578
     * photoCode2 : null
     * photoUrl2 : http://imagess.icloudinn.net/AddGoosActivity1611586488807
     * photoCode3 : null
     * photoUrl3 : http://imagess.icloudinn.net/AddGoosActivity1611586498575
     * userId : 12
     * createDate : 2021-01-25 22:55:32
     * updateDate : null
     * remarks : null
     * positionName : null
     * furnitureName : null
     * userName : null
     * createDateStr : null
     * current : null
     * size : null
     * goodsTypeName : 服饰鞋帽-服饰鞋帽
     */

    public int id;
    public String goodsName;
    public int goodsType;
    public int goodsTypeSecond;
    public int positionId;
    public int furnitureId;
    public String goodsRfid;
    public double price;
    public String photoCode1;
    public String photoUrl1;
    public String photoCode2;
    public String photoUrl2;
    public String photoCode3;
    public String photoUrl3;
    public int userId;
    public String createDate;
    public String updateDate;
    public String remarks;
    public String positionName;
    public String furnitureName;
    public String userName;
    public String createDateStr;
    public String current;
    public String size;
    public String goodsTypeName;
    public boolean isCheck;
   
}
