package com.xt.xiaotan.search.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xt.xiaotan.R;
import com.xt.xiaotan.search.model.Categorie;
import com.xt.xiaotan.search.model.Room;

import java.util.List;

public class GoodsTypePopupWindowAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;

    private List<Categorie> data;


    public void setData(List<Categorie> data){
        this.data = data;
        notifyDataSetChanged();
    }


    public interface OnItemClickListener{
        void onItemClick();
    }
    public GoodsTypePopupWindowAdapter(Context context){
        this.context = context;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        RecyclerView.ViewHolder viewHolder;
        if(viewType == 1){
            view = LayoutInflater.from(context).inflate(R.layout.type_title_item,parent,false);
            viewHolder = new TitleViewHolder(view);
        }else {
            view = LayoutInflater.from(context).inflate(R.layout.type_content_item,parent,false);
            viewHolder = new ContentViewHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Log.e("tag","position = "+position);
        if(holder instanceof ContentViewHolder) {
            final ContentViewHolder contentViewHolder = (ContentViewHolder) holder;
            final Categorie categorie = data.get(contentViewHolder.getAdapterPosition()-1);
            contentViewHolder.checkedTextView.setText(categorie.getTypeName());
            contentViewHolder.check_sign_iv.setVisibility(categorie.isCheck?View.VISIBLE:View.GONE);
            contentViewHolder.checkedTextView.setChecked(categorie.isCheck);
            contentViewHolder.checkedTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categorie.isCheck = !categorie.isCheck;
                    notifyDataSetChanged();
                }
            });
        }else{
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position==0){
            return 1;
        }else {
            return 2;
        }
    }

    @Override
    public int getItemCount() {

        return data!=null?data.size()+1:0;
    }

    public class TitleViewHolder extends RecyclerView.ViewHolder{

        public TitleViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    public class ContentViewHolder extends RecyclerView.ViewHolder{
         CheckedTextView checkedTextView;
         ImageView check_sign_iv;

        public ContentViewHolder(@NonNull View itemView) {

            super(itemView);
            checkedTextView = itemView.findViewById(R.id.check_tv);
            check_sign_iv = itemView.findViewById(R.id.iv_sign);
        }
    }
}
