package com.xt.xiaotan.search;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xt.xiaotan.R;
import com.xt.xiaotan.search.adapter.GoodsTypePopupWindowAdapter;
import com.xt.xiaotan.search.model.Categorie;
import com.xt.xiaotan.search.model.SearchFilter;
import com.xt.xiaotan.utils.DisplayUtils;

import java.util.ArrayList;
import java.util.List;

public class GoodsTypePopupWindow extends PopupWindow {
    private RecyclerView rv;
    private GoodsTypePopupWindowAdapter goodsTypePopupWindowAdapter;


    private Context context;

    private View view;

    private TextView resume_tv;

    private TextView sure_tv;

    private SearchFilter searchFilter;

    public void setOnGetCheckDataListener(OnGetCheckDataListener onGetCheckDataListener) {
        this.onGetCheckDataListener = onGetCheckDataListener;
    }

    private OnGetCheckDataListener onGetCheckDataListener;
    public GoodsTypePopupWindow(Context context) {
        super(context);
        this.context = context;
    }



    @Override
    public void showAsDropDown(View anchor) {
        this.setWidth(DisplayUtils.getScreenWidth(context));
        this.setHeight(DisplayUtils.getScreenHeight(context)-DisplayUtils.dip2px(context,70));
        this.setBackgroundDrawable(null);
        view = LayoutInflater.from(context).inflate(R.layout.goods_type_popupwindow,null);
        rv = view.findViewById(R.id.rv);
        resume_tv = view.findViewById(R.id.resume_tv);
        sure_tv = view.findViewById(R.id.sure_tv);
        view.findViewById(R.id.poup_ll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        view.findViewById(R.id.content_ll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        resume_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resumeData();
            }
        });
        sure_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onGetCheckDataListener!=null){
                    onGetCheckDataListener.onGetCheckData(getCheckData());
                }
                dismiss();
            }
        });
        setContentView(view);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context,3);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if(position==0){
                    return 3;
                }else {
                    return 1;
                }
            }
        });
        goodsTypePopupWindowAdapter = new GoodsTypePopupWindowAdapter(context);
        rv.setLayoutManager(gridLayoutManager);
        rv.setAdapter(goodsTypePopupWindowAdapter);


        super.showAsDropDown(anchor);
    }

    public interface OnGetCheckDataListener{
        void onGetCheckData( List<Categorie> categories);
    }
    private void resumeData(){
        if(searchFilter!=null){
            if(searchFilter.getCategories()!=null){
                for(int i= 0 ;i<searchFilter.getCategories().size();i++){
                    searchFilter.getCategories().get(i).isCheck = false;
                }
                goodsTypePopupWindowAdapter.notifyDataSetChanged();
            }
        }
    }

    private List<Categorie> getCheckData(){
        List<Categorie> list = new ArrayList<>();
        if(searchFilter!=null){
            if(searchFilter.getCategories()!=null){
                for(int i= 0 ;i<searchFilter.getCategories().size();i++){
                    if(searchFilter.getCategories().get(i).isCheck) {
                        list.add(searchFilter.getCategories().get(i));
                    }
                }
            }
        }
        return list;
    }
    public void setData(SearchFilter searchFilter){
        this.searchFilter = searchFilter;
        goodsTypePopupWindowAdapter.setData(searchFilter.getCategories());

    }
}
