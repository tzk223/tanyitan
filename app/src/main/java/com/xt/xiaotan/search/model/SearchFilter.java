package com.xt.xiaotan.search.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tiandi
 * on 2020/11/22
 */
public class SearchFilter implements Serializable {
    private List<Room> rooms;
    private List<Categorie> categories;

    public List<Room> getRooms() {
        if (rooms == null) {
            return new ArrayList<>();
        }
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public List<Categorie> getCategories() {
        if (categories == null) {
            return new ArrayList<>();
        }
        return categories;
    }

    public void setCategories(List<Categorie> categories) {
        this.categories = categories;
    }
}
