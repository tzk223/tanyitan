package com.xt.xiaotan.search.searchmvp;

import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.first.model.FurnitureRequst;
import com.xt.xiaotan.first.model.GoodsRequst;
import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.mvp.BaseModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.search.model.GoodEntityBean;
import com.xt.xiaotan.search.model.GoodsListBean;
import com.xt.xiaotan.search.model.SearchFilter;
import com.xt.xiaotan.utils.RetrofitUtils;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by tiandi
 * on 2020/11/10
 */
public class SearchModel extends BaseModel implements SearchContract.Model {


    @Override
    public Observable<BaseHttpResult<SearchFilter>> getGoodsFilter() {
        return RetrofitUtils.getHttpService().getFilterGoodsList();
    }

    @Override
    public Observable<BaseHttpResult<GoodsListBean>> getGoodList(GoodsRequst goodsRequst) {
        return RetrofitUtils.getHttpService().getGoodsList(goodsRequst);
    }
}
