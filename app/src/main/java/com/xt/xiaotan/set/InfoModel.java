package com.xt.xiaotan.set;

import java.util.List;

/**
 * Created by tiandi
 * on 2020/11/29
 */
public class InfoModel {


    /**
     * photoUrl : 头像地址
     * userName : 用户名
     * birthday : 生日
     * sex : 0
     * surveys : [{"surveyId":1,"surveyName":"选项1","isSelected":true},{"surveyId":2,"surveyName":"选项2","isSelected":false},{"surveyId":3,"surveyName":"选项3","isSelected":false},{"surveyId":4,"surveyName":"选项4","isSelected":false},{"surveyId":5,"surveyName":"选项5","isSelected":false},{"surveyId":6,"surveyName":"选项6","isSelected":false},{"surveyId":7,"surveyName":"选项7","isSelected":false}]
     */

    private String photoUrl;
    private String userName;
    private String birthday;
    private String sex;
    private List<SurveysBean> surveys;
    private String sexName;


    public String getSexName() {
        if("0".equals(getSex())){
            sexName = "男";
        }else{
            sexName = "女";
        }
        return sexName;
    }

    @Override
    public String toString() {
        return "InfoModel{" +
                "photoUrl='" + photoUrl + '\'' +
                ", userName='" + userName + '\'' +
                ", birthday='" + birthday + '\'' +
                ", sex='" + sex + '\'' +
                ", surveys=" + surveys +
                ", sexName='" + sexName + '\'' +
                '}';
    }

    public void setSexName(String sexName) {
        this.sexName = sexName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public List<SurveysBean> getSurveys() {
        return surveys;
    }

    public void setSurveys(List<SurveysBean> surveys) {
        this.surveys = surveys;
    }

    public static class SurveysBean {
        /**
         * surveyId : 1
         * surveyName : 选项1
         * isSelected : true
         */

        private int surveyId;
        private String surveyName;
        private boolean isSelected;

        public int getSurveyId() {
            return surveyId;
        }

        public void setSurveyId(int surveyId) {
            this.surveyId = surveyId;
        }

        public String getSurveyName() {
            return surveyName;
        }

        public void setSurveyName(String surveyName) {
            this.surveyName = surveyName;
        }

        public boolean isIsSelected() {
            return isSelected;
        }

        public void setIsSelected(boolean isSelected) {
            this.isSelected = isSelected;
        }
    }
}
