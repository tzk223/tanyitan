package com.xt.xiaotan.set;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.bigkoo.pickerview.TimePickerView;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseActivity;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.databinding.SetActivityBinding;
import com.xt.xiaotan.login.LoginActivity;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.utils.RetrofitUtils;
import com.xt.xiaotan.utils.SharePrefanceUtils;
import com.xt.xiaotan.utils.cache.CacheManager;

import org.greenrobot.eventbus.Subscribe;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by tiandi
 * on 2020/11/28
 */
public class MySetActivity  extends BaseActivity {
    private SetActivityBinding binding;

    public static void gotoSetActivity(Context context){
        Intent intent = new Intent(context,MySetActivity.class);
        context.startActivity(intent);
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.set_activity);
        super.onCreate(savedInstanceState);
        binding.outBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharePrefanceUtils.putStr(MySetActivity.this,"token","");
                LoginActivity.gotoLoginActivity(MySetActivity.this);
            }
        });
        binding.backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }



    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected void getIntent(Intent intent) {

    }

    @Override
    protected void initView() {
     binding.checkBirthdayLl.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if(infoModel!=null&&!TextUtils.isEmpty(infoModel.getBirthday())){
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date birthDay = format.parse(infoModel.getBirthday());
                showTimePack(birthDay);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else{
            showTimePack(new Date());
        }
    }
});
    }
    private void showTimePack(Date date){
        TimePickerView timePickerView = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                binding.br.setText(formatter.format(date));
            }
        }).setType(new boolean[]{true, true, true, false, false, false})
                .setCancelColor(Color.parseColor("#F77B22"))
                .setTitleText("选择生日时间")
                .setTitleColor(Color.parseColor("#666666"))
                .setSubmitColor(Color.parseColor("#F77B22")).build();
        //注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
        Calendar cal = Calendar.getInstance();
        if(date!=null){
            cal.setTime(date);
        }
        timePickerView.setDate(Calendar.getInstance());
        timePickerView.show();
    }
    @Override
    protected void initListener() {

    }

    InfoModel infoModel;

    @Override
    protected void initData() {
        RetrofitUtils.getHttpService().getInfo().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.<BaseHttpResult<InfoModel>>bindUntilEvent(ActivityEvent.STOP))
                .subscribe(new BaseObserver<InfoModel>(null) {


                    @Override
                    public void onSuccess(BaseHttpResult<InfoModel> result) {
                        infoModel = result.getData();
                        binding.setInfo(result.getData());

                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {

                    }
                });




    }


}
