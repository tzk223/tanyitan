package com.xt.xiaotan.goods.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xt.xiaotan.R;
import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.goods.bean.TypeBean;
import com.xt.xiaotan.utils.DisplayUtils;

import java.util.List;

public class PositionAdapter extends RecyclerView.Adapter<PositionAdapter.ViewHolder> {
    public Context context;

    private List<PositionResult> list;

    public void setOnItemClickListener(PositionAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private PositionAdapter.OnItemClickListener onItemClickListener;
    public PositionAdapter(Context context){
        this.context = context;
    }

    public void setData(List<PositionResult> list){
        this.list = list;
        notifyDataSetChanged();
    }


    public interface OnItemClickListener{
        void onItemClick(PositionResult positionResult,int type);
    }
    @NonNull
    @Override
    public PositionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.position_item, parent, false);
        return new PositionAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PositionAdapter.ViewHolder holder, int position) {
        if(list!=null&&position < list.size()){
            final PositionResult typeBean = list.get(position);
            holder.name_tv.setText(typeBean.getPositionName());
            holder.name_tv.setCompoundDrawables(null,null,null,null);
            holder.name_tv.setChecked(typeBean.isCheck);
            holder.position_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    typeBean.isCheck = !typeBean.isCheck;
                    PositionResult positionResult =  isCheckAll(typeBean);
                    notifyDataSetChanged();
                    onItemClickListener.onItemClick(positionResult,1);
                }
            });
            if(typeBean.isCheck){
                holder.position_ll.setBackgroundResource(R.drawable.bg_ffbc4c_ff6f3c);
            }else{
                holder.position_ll.setBackgroundResource(R.drawable.bg_withe);
            }

        }else{
            holder.name_tv.setText("添加");
            Drawable drawable = context.getResources().getDrawable(R.mipmap.add_positon);
            drawable.setBounds(0,0, DisplayUtils.dip2px(context,15),DisplayUtils.dip2px(context,15));
            holder.name_tv.setCompoundDrawables(drawable,null,null,null);
            holder.name_tv.setChecked(false);
            holder.position_ll.setBackgroundResource(R.drawable.bg_withe);
            holder.position_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(null,0);
                }
            });
        }
    }

    private PositionResult isCheckAll(PositionResult positionResult){
        if(list!=null){
            for(PositionResult result:list){
                if(positionResult.isCheck&&positionResult.getId()!=result.getId()){
                    result.isCheck = false;
                }

            }
            if(!positionResult.isCheck){
                list.get(0).isCheck = true;
            }
            return positionResult.isCheck?positionResult:list.get(0);
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return list!=null?list.size()+1:1;
    }
    class ViewHolder extends RecyclerView.ViewHolder{

        public CheckedTextView name_tv;

        public LinearLayout position_ll;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name_tv = itemView.findViewById(R.id.name_tv);
            position_ll = itemView.findViewById(R.id.position_ll);
        }
    }
}
