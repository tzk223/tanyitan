package com.xt.xiaotan.goods;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.callback.OSSCompletedCallback;
import com.alibaba.sdk.android.oss.callback.OSSProgressCallback;
import com.alibaba.sdk.android.oss.internal.OSSAsyncTask;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.xt.xiaotan.R;
import com.xt.xiaotan.app.AppConfig;
import com.xt.xiaotan.base.BaseDialogFragment;
import com.xt.xiaotan.base.ViewHolder;
import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.first.model.FurnitureRequst;
import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.first.model.SaveFurnitureRequst;
import com.xt.xiaotan.first.model.SaveRoomRequst;
import com.xt.xiaotan.goods.adapter.PositionAdapter;
import com.xt.xiaotan.goods.adapter.PositionChildAdapter;
import com.xt.xiaotan.goods.bean.FurnitureRequest;
import com.xt.xiaotan.goods.bean.PositionRequest;
import com.xt.xiaotan.goods.bean.TypeBean;
import com.xt.xiaotan.login.model.OssBean;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.oss.OSSService;
import com.xt.xiaotan.utils.RetrofitUtils;
import com.xt.xiaotan.utils.ToastUtils;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AddGoodsDialog extends BaseDialogFragment {
    private NestedScrollView nest_view;

    private RecyclerView position_rv;

    private RecyclerView position_child_rv;

    private LinearLayout position_ll;

    private LinearLayout position_edit_ll;

    private ImageView room_iv;

    private ImageView carme_iv;

    private EditText eidt_et;

    private Button sure_btn;

    private TextView close_tv;

    private TextView title_tv;

    private ImageView back_iv;

    private PositionAdapter positionAdapter;

    private PositionChildAdapter positionChildAdapter;

    private List<PositionResult> positionResults;

    private PositionResult positionResult;

    private  List<FurnitureListResult> furnitureListResults;

    private FurnitureListResult furnitureListResult;

    private int checkType; //选择是添加房间还是家具

    private OssBean ossBean;

    private String imageUrl;

    private String fileUrl;


    public void setImageUrl(String imageUrl) {
        this.imageUrl = "AddGoodsDialog"+System.currentTimeMillis();
        this.fileUrl = imageUrl;
        if(room_iv!=null){
            room_iv.setVisibility(View.VISIBLE);
            carme_iv.setVisibility(View.GONE);
            Glide.with(getActivity()).load(imageUrl).apply(new RequestOptions().error(R.drawable.ic_launcher_background)).into(room_iv);
            getOss();
        }
    }

    public boolean checkPicture = false;


    public static AddGoodsDialog getInstance(){
        return new AddGoodsDialog();
    }
    @Override
    public int setUpLayoutId() {
        return R.layout.add_goods_dialog;
    }

    private CheckPositionSuccessListener checkPositionSuccessListener;

    public void setCheckPositionSuccessListener(CheckPositionSuccessListener checkPositionSuccessListener) {
        this.checkPositionSuccessListener = checkPositionSuccessListener;
    }

    public interface  CheckPositionSuccessListener{
        void checkPositionSuccess(FurnitureListResult furnitureListResult,PositionResult positionResult);
    }
    @Override
    public void convertView(ViewHolder holder, BaseDialogFragment dialog) {
        nest_view = holder.getView(R.id.nest_view);
        position_rv = holder.getView(R.id.position_rv);
        position_child_rv = holder.getView(R.id.position_child_rv);
        position_ll = holder.getView(R.id.position_ll);
        position_edit_ll = holder.getView(R.id.position_edit_ll);
        room_iv = holder.getView(R.id.room_iv);
        carme_iv = holder.getView(R.id.carme_iv);
        eidt_et = holder.getView(R.id.eidt_et);
        sure_btn = holder.getView(R.id.sure_btn);
        close_tv = holder.getView(R.id.close_tv);
        title_tv = holder.getView(R.id.title_tv);
        back_iv = holder.getView(R.id.back_iv);
        positionAdapter = new PositionAdapter(getActivity());
        positionChildAdapter = new PositionChildAdapter(getActivity());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),3);
        GridLayoutManager gridLayoutManager1 = new GridLayoutManager(getActivity(),3);
        position_rv.setLayoutManager(gridLayoutManager);
        position_child_rv.setLayoutManager(gridLayoutManager1);
        position_rv.setAdapter(positionAdapter);
        position_child_rv.setAdapter(positionChildAdapter);
        close_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        positionAdapter.setOnItemClickListener(new PositionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(PositionResult positionResult, int type) {
                  if(type ==1&&positionResult!=null){
                      AddGoodsDialog.this.positionResult = positionResult;
                      getPositionDetail(positionResult);
                  }else{
                      showAddPosition();
                  }
            }
        });
        positionChildAdapter.setOnItemClickListener(new PositionChildAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(FurnitureListResult typeBean, int type) {
                if(type ==1&&typeBean!=null){
                    furnitureListResult = typeBean;
                    checkPositionSuccessListener.checkPositionSuccess(furnitureListResult,positionResult);
                }else{
                    showAddChildPosition();
                }
            }
        });
        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCheckPosition();
            }
        });
        position_edit_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlbum();
            }
        });
        sure_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitData();
            }
        });
    }



    private void showCheckPosition(){
        nest_view.setVisibility(View.VISIBLE);
        position_ll.setVisibility(View.GONE);
        back_iv.setVisibility(View.GONE);
        title_tv.setText("选择存放位置");
    }

    private void showAddPosition(){
        checkType = 1;
        nest_view.setVisibility(View.GONE);
        position_ll.setVisibility(View.VISIBLE);
        position_edit_ll.setVisibility(View.VISIBLE);
        back_iv.setVisibility(View.VISIBLE);
        title_tv.setText("添加位置");
    }

    private void showAddChildPosition(){
        checkType = 2;
        nest_view.setVisibility(View.GONE);
        position_ll.setVisibility(View.VISIBLE);
        position_edit_ll.setVisibility(View.GONE);
        back_iv.setVisibility(View.VISIBLE);
        title_tv.setText("添加家具");
    }

    private void submitData(){
        if(checkType == 2){ //保存家具
            if(positionResult!=null){
                FurnitureRequest saveFurnitureRequst = new FurnitureRequest();
                saveFurnitureRequst.positionId = (positionResult.getId()+"");
                if(TextUtils.isEmpty(eidt_et.getText().toString())){
                    ToastUtils.show("您还没有填写家具名称",1000);
                    return;
                }
                saveFurnitureRequst.furnitureName = eidt_et.getText().toString();
                saveFurniture(saveFurnitureRequst);
            }
        }else if(checkType == 1){//保存房间
            PositionRequest saveRoomRequst = new PositionRequest();
            if(TextUtils.isEmpty(imageUrl)){
                ToastUtils.show("您还没有选择位置图片",1000);
                return;
            }
            if(TextUtils.isEmpty(eidt_et.getText().toString())){
                ToastUtils.show("您还没有填写位置名称",1000);
                return;
            }
            if(imageUrl.contains(AppConfig.PICTURE_URL)){
                saveRoomRequst.pictureUrl = imageUrl;
            }else{
                saveRoomRequst.pictureUrl = AppConfig.PICTURE_URL+imageUrl;
            }

            saveRoomRequst.positionName = eidt_et.getText().toString();
            savaPosition(saveRoomRequst);
        }
    }

    private void showAlbum() {
        //参数很多，根据需要添加
        checkPicture = true;
        PictureSelector.create(getActivity())
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .isCamera(true)
                .maxSelectNum(1)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .imageSpanCount(4)// 每行显示个数
                .selectionMode(PictureConfig.MULTIPLE)// 多选 or 单选PictureConfig.MULTIPLE : PictureConfig.SINGLE
                .previewImage(true)// 是否可预览图片
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                //.sizeMultiplier(0.5f)// glide 加载图片大小 0~1之间 如设置 .glideOverride()无效
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .withAspectRatio(1, 1)// 裁剪比例 如16:9 3:2 3:4 1:1 可自定义
                //.selectionMedia(selectList)// 是否传入已选图片//   .previewEggs()// 预览图片时 是否增强左右滑动图片体验(图片滑动一半即可看到上一张是否选中)
                //.cropCompressQuality(90)// 裁剪压缩质量 默认100
                //.compressMaxKB()//压缩最大值kb compressGrade()为Luban.CUSTOM_GEAR有效
                //.compressWH() // 压缩宽高比 compressGrade()为Luban.CUSTOM_GEAR有效
                //.cropWH()// 裁剪宽高比，设置如果大于图片本身宽高则无效
                .rotateEnabled(false) // 裁剪是否可旋转图片
                //.scaleEnabled()// 裁剪是否可放大缩小图片
                //.recordVideoSecond()//录制视频秒数 默认60s
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == PictureConfig.CHOOSE_REQUEST && data != null) {
            Log.d("tag", data.toString());
            List<LocalMedia> localMedia = PictureSelector.obtainMultipleResult(data);
            if(localMedia!=null&&localMedia.size()>0){
                getOss();
                imageUrl = localMedia.get(0).getCompressPath();
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getPosition();
    }

    private void getPosition(){
        RetrofitUtils.getHttpService().getRoomList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<List<PositionResult>>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<List<PositionResult>> result) {
                        Log.d("tag","result"+result);
                        if(result!=null&&result.getData().size()>0){
                            positionResults = result.getData();
                            result.getData().get(0).isCheck =true;
                            positionResult =  result.getData().get(0);
                            positionAdapter.setData(result.getData());
                            getPositionDetail(positionResult);
                        }else{
                            position_child_rv.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {


                    }
                });
    }

    private void saveFurniture(FurnitureRequest saveFurnitureRequst){
        RetrofitUtils.getHttpService().saveFurniture(saveFurnitureRequst).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<Boolean>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<Boolean> result) {
                        Log.d("tag","是否成功");
                        ToastUtils.show("上传家具数据成功",1000);
                        showCheckPosition();
                        eidt_et.setText(null);
                        if(positionResult!=null){
                            getPositionDetail(positionResult);
                        }

                    }
                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {
                        ToastUtils.show("上传家具数据失败",1000);
                    }
                });
    }
    private void savaPosition(PositionRequest saveRoomRequst){
        RetrofitUtils.getHttpService().saveRoom(saveRoomRequst).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<String>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<String> result) {
                        Log.d("tag","是否成功");
                        ToastUtils.show("上传房间数据成功",1000);
                        eidt_et.setText(null);
                        imageUrl = null;
                        room_iv.setVisibility(View.GONE);
                        carme_iv.setVisibility(View.VISIBLE);
                        showCheckPosition();
                        getPosition();
                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {
                        ToastUtils.show("上传房间数据失败",1000);
                    }
                });
    }
    private void getOss(){
        RetrofitUtils.getHttpService().oss().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<OssBean>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<OssBean> result) {
                        ossBean = result.getData();
                        uploadImage(fileUrl);

                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {


                    }
                });
    }
    private void uploadImage(String imageFilePath){

        // 构造上传请求
        PutObjectRequest put = new PutObjectRequest("kdtantan", imageUrl, imageFilePath);

// 异步上传时可以设置进度回调
        put.setProgressCallback(new OSSProgressCallback<PutObjectRequest>() {
            @Override
            public void onProgress(PutObjectRequest request, long currentSize, long totalSize) {
                Log.d("PutObject", "currentSize: " + currentSize + " totalSize: " + totalSize);
            }
        });

        OSSAsyncTask task = OSSService.getOss(ossBean).asyncPutObject(put, new OSSCompletedCallback<PutObjectRequest, PutObjectResult>() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                Log.d("PutObject", "UploadSuccess");
                imageUrl = AppConfig.PICTURE_URL+imageUrl;
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientExcepion, ServiceException serviceException) {
                // 请求异常
                if (clientExcepion != null) {
                    // 本地异常如网络异常等
                    clientExcepion.printStackTrace();
                }
                if (serviceException != null) {
                    // 服务异常
                    Log.e("ErrorCode", serviceException.getErrorCode());
                    Log.e("RequestId", serviceException.getRequestId());
                    Log.e("HostId", serviceException.getHostId());
                    Log.e("RawMessage", serviceException.getRawMessage());
                }
            }
        });
        task.waitUntilFinished();
    }
    private void getPositionDetail(PositionResult positionResult){
        FurnitureRequst furnitureRequst = new FurnitureRequst();
        furnitureRequst.setPositionId(positionResult.getId()+"");
        furnitureRequst.setNeedGoodsCount(false);
        RetrofitUtils.getHttpService().furnitureList(furnitureRequst).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<List<FurnitureListResult>>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<List<FurnitureListResult>> result) {
                        Log.d("tag","result"+result);
                        if(result!=null){
                            furnitureListResults = result.getData();
                            positionChildAdapter.setData(result.getData());
                        }
                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {


                    }
                });
    }


}
