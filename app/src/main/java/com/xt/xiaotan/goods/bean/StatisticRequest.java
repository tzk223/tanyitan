package com.xt.xiaotan.goods.bean;

import java.io.Serializable;

public class StatisticRequest implements Serializable {
    private String goodTypeId;
    private String month;

    public String getGoodTypeId() {
        return goodTypeId;
    }

    public void setGoodTypeId(String goodTypeId) {
        this.goodTypeId = goodTypeId;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
