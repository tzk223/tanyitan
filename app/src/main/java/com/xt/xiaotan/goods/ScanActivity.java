package com.xt.xiaotan.goods;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;

import com.king.zxing.CaptureActivity;
import com.xt.xiaotan.R;
import com.xt.xiaotan.utils.ToastUtils;


public class ScanActivity extends CaptureActivity {

    public static void gotoScanActivity(Context context, int getScanReslut){
        Intent intent = new Intent(context,ScanActivity.class);
        if(context instanceof Activity){
            ((Activity)context).startActivityForResult(intent,getScanReslut);
        }

    }

    @Override
    public int getLayoutId() {
        return R.layout.scan_activity;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findViewById(R.id.back_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onResultCallback(String result) {
        if(TextUtils.isEmpty(result)){
            ToastUtils.show("扫码未成功",1000);
        }
        setResult(10,new Intent().putExtra("result",result));
        finish();
        return true;
    }
}
