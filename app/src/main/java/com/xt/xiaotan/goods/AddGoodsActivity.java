package com.xt.xiaotan.goods;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.callback.OSSCompletedCallback;
import com.alibaba.sdk.android.oss.callback.OSSProgressCallback;
import com.alibaba.sdk.android.oss.internal.OSSAsyncTask;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.bumptech.glide.Glide;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.xt.xiaotan.R;
import com.xt.xiaotan.app.AppConfig;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.bluetooth.BlueToothSearchActivity;
import com.xt.xiaotan.databinding.AddGoodsActivityBinding;
import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.goods.bean.AddGoodRequest;
import com.xt.xiaotan.goods.bean.TypeBean;
import com.xt.xiaotan.goods.bean.TypeRequest;
import com.xt.xiaotan.goods.view.GrideView;
import com.xt.xiaotan.login.model.OssBean;
import com.xt.xiaotan.login.mvp.LoginContract;
import com.xt.xiaotan.login.mvp.LoginPresenter;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.oss.OSSService;
import com.xt.xiaotan.search.model.GoodEntityBean;
import com.xt.xiaotan.set.InfoModel;
import com.xt.xiaotan.utils.ApiService;
import com.xt.xiaotan.utils.DisplayUtils;
import com.xt.xiaotan.utils.RetrofitUtils;
import com.xt.xiaotan.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddGoodsActivity extends BaseMvpActivity implements
        LoginContract.View {
    AddGoodsActivityBinding binding;

    private OssBean ossBean;
    private LoginPresenter loginPresenter;

    private String managerUrl;

    private List<String> imageUrl;

    private TypeRequest typeRequest;

    private TypeGoodsDialog typeGoodsDialog;

    private AddGoodsDialog addGoodsDialog;

    private FurnitureListResult furnitureListResultCheck;

    private PositionResult positionResultCheck;

    private AddGoodRequest addGoodRequest;

    private GoodEntityBean goodEntityBean;
    @Override
    protected BasePresenter createPresenter() {
        loginPresenter = new LoginPresenter(this);
        return loginPresenter;
    }


    public static void gotoAddGoodsActivity(Context context){
        Intent intent = new Intent(context,AddGoodsActivity.class);
        context.startActivity(intent);
    }

    public static void gotoAddGoodsActivity(Context context, GoodEntityBean goodEntityBean){
        Intent intent = new Intent(context,AddGoodsActivity.class);
        intent.putExtra("goodEntityBean",goodEntityBean);
        context.startActivity(intent);
    }
    @Override
    protected int getLayoutId() {
        return R.layout.add_goods_activity;
    }

    @Override
    protected void getIntent(Intent intent) {

    }

    private boolean createGoodRequest(){
         addGoodRequest = new AddGoodRequest();
        if(imageUrl == null||imageUrl.size() == 0){
            ToastUtils.show("请先选择物品图片",1000);
            return false;
        }
        if(imageUrl.size()<3){
            ToastUtils.show("必须选择3张图片",1000);
            return false;
        }
        String name = binding.nameEt.getText().toString().trim();
        if(TextUtils.isEmpty(name)){
            ToastUtils.show("请填写物品名称",1000);
            return false;
        }
        if(checkTypeBean == null||checkChildBean == null){
            ToastUtils.show("请选择物品类型",1000);
            return false;
        }
        if(positionResultCheck == null||furnitureListResultCheck == null){
            ToastUtils.show("请选择物品摆放位置",1000);
            return false;
        }
        String scanText = binding.scanTextTv.getText().toString();
        if(TextUtils.isEmpty(scanText)){
            ToastUtils.show("请扫码",1000);
            return false;
        }
        String price = binding.priceEt.getText().toString();
        if(TextUtils.isEmpty(price)){
            ToastUtils.show("请输入物品价格",1000);
            return false;
        }
        String remarks = binding.remarksEt.getText().toString();
        if (TextUtils.isEmpty(remarks)) {
            ToastUtils.show("请输入备注信息",1000);
            return false;
        }


       if(imageUrl.size() ==3){
           addGoodRequest.photoUrl1 = imageUrl.get(0);
           addGoodRequest.photoUrl2 = imageUrl.get(1);
           addGoodRequest.photoUrl3 = imageUrl.get(2);
       }
       addGoodRequest.goodsName = name;
       addGoodRequest.goodsType = checkTypeBean.id;
       addGoodRequest.goodsTypeSecond = checkChildBean.id;
       addGoodRequest.positionId = positionResultCheck.getId();
       addGoodRequest.furnitureId = furnitureListResultCheck.getId();
       addGoodRequest.goodsRfid = scanText;
       addGoodRequest.remarks = remarks;
       addGoodRequest.price = price.replace("¥ ","");
       if(goodEntityBean!=null){
           addGoodRequest.id = goodEntityBean.id+"";
       }
       return true;
    }

    private void setDefaultData(){
        if(goodEntityBean!=null){
            imageUrl = new ArrayList<>();
            if(!TextUtils.isEmpty(goodEntityBean.photoUrl1)){
                imageUrl.add(goodEntityBean.photoUrl1);
            }if(!TextUtils.isEmpty(goodEntityBean.photoUrl2)){
                imageUrl.add(goodEntityBean.photoUrl2);
            }if(!TextUtils.isEmpty(goodEntityBean.photoUrl3)) {
                imageUrl.add(goodEntityBean.photoUrl3);
            }
            if(checkTypeBean == null){
                checkTypeBean = new TypeBean();
                checkTypeBean.id = goodEntityBean.goodsType;
            }
            if(checkChildBean == null){
                checkChildBean = new TypeBean();
                checkTypeBean.id = goodEntityBean.goodsTypeSecond;
            }

            if(positionResultCheck == null||furnitureListResultCheck == null){
                positionResultCheck = new PositionResult();
                positionResultCheck.setId(goodEntityBean.positionId);
            }
            if(furnitureListResultCheck == null){
                furnitureListResultCheck = new FurnitureListResult();
                furnitureListResultCheck.setId(goodEntityBean.furnitureId+"");
            }
            binding.titleTv.setText("商品详情");
            binding.editTv.setVisibility(View.VISIBLE);
            binding.editTv.setText("编辑");
            binding.checkImageGv.setDatas(imageUrl);
            binding.nameEt.setText(goodEntityBean.goodsName);
            binding.checkTypeTv.setText(goodEntityBean.goodsTypeName+"");
            binding.checkPositionTv.setText(goodEntityBean.positionName+"-"+goodEntityBean.furnitureName);
            binding.scanTextTv.setText(goodEntityBean.goodsRfid);
            binding.priceEt.setText("¥ "+goodEntityBean.price);
            binding.remarksEt.setText(goodEntityBean.remarks);
            binding.sureBtn.setText("探一探");
            binding.checkTypeTv.setCompoundDrawables(null,null,null,null);
            binding.checkPositionTv.setCompoundDrawables(null,null,null,null);
            binding.scanTv.setVisibility(View.GONE);
            binding.priceEt.setEnabled(false);
            binding.nameEt.setEnabled(false);
            binding.checkTypeLl.setEnabled(false);
            binding.checkPositionLl.setEnabled(false);
            binding.remarksEt.setEnabled(false);
            binding.checkImageGv.setShowAdd(false);
        }
    }

    private void showNewEdit(){
        binding.sureBtn.setText("删除");
        binding.titleTv.setText("编辑商品");
        binding.sureBtn.setTextColor(Color.parseColor("#ffffff"));
        binding.sureBtn.setBackgroundResource(R.drawable.bg_333333_10);
        Drawable drawable = getResources().getDrawable(R.mipmap.icon_gray_right);
        drawable.setBounds(0,0, DisplayUtils.dip2px(this,8),DisplayUtils.dip2px(this,14));
        binding.checkTypeTv.setCompoundDrawables(null,null,drawable,null);
        binding.checkPositionTv.setCompoundDrawables(null,null,drawable,null);
        binding.scanTv.setVisibility(View.VISIBLE);
        binding.scanTv.setText("重扫");
        binding.priceEt.setEnabled(true);
        binding.nameEt.setEnabled(true);
        binding.checkTypeLl.setEnabled(true);
        binding.checkPositionLl.setEnabled(true);
        binding.remarksEt.setEnabled(true);
        binding.checkImageGv.setShowAdd(true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        goodEntityBean  = (GoodEntityBean) getIntent().getSerializableExtra("goodEntityBean");
        binding = DataBindingUtil.setContentView(this,R.layout.add_goods_activity);
        binding.setAddUserInfo(this);
        super.onCreate(savedInstanceState);
        if(goodEntityBean!=null){
            searchGoods(goodEntityBean.id+"");
        }
        binding.checkPositionLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPositionDialog();
            }
        });
        binding.checkTypeLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getType(null);
            }
        });
        imageUrl = new ArrayList<>();
        binding.checkImageGv.setOnCheckPictureListener(new GrideView.OnCheckPictureListener() {
            @Override
            public void onCheckPitcture(int type) {
                if(type == 0){
                    if(imageUrl.size()==3){
                        ToastUtils.show("最多只能选择三张",500);
                    }else{
                        showAlbum();
                    }

                }
            }
        });


        binding.sureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.sureBtn.getText().toString().equals("确定")){
                    addGoods();
                }else if(binding.sureBtn.getText().toString().equals("探一探")){
                    BlueToothSearchActivity.gotoBlueToothSearchActivity(AddGoodsActivity.this,goodEntityBean);
                }

            }
        });
        binding.backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        binding.scanTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 ScanActivity.gotoScanActivity(AddGoodsActivity.this,123);
            }
        });
        setDefaultData();
        binding.editTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.editTv.getText().toString().equals("编辑")){
                    binding.editTv.setText("保存");
                    showNewEdit();
                }else if(binding.editTv.getText().toString().equals("保存")){
                     addGoods();
                }
            }
        });
        loginPresenter.oss();
    }
    private void showAlbum() {
        //参数很多，根据需要添加
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .isCamera(true)
                .maxSelectNum(1)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .imageSpanCount(4)// 每行显示个数
                .selectionMode(PictureConfig.MULTIPLE)// 多选 or 单选PictureConfig.MULTIPLE : PictureConfig.SINGLE
                .previewImage(true)// 是否可预览图片
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                //.sizeMultiplier(0.5f)// glide 加载图片大小 0~1之间 如设置 .glideOverride()无效
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .withAspectRatio(1, 1)// 裁剪比例 如16:9 3:2 3:4 1:1 可自定义
                //.selectionMedia(selectList)// 是否传入已选图片//   .previewEggs()// 预览图片时 是否增强左右滑动图片体验(图片滑动一半即可看到上一张是否选中)
                //.cropCompressQuality(90)// 裁剪压缩质量 默认100
                //.compressMaxKB()//压缩最大值kb compressGrade()为Luban.CUSTOM_GEAR有效
                //.compressWH() // 压缩宽高比 compressGrade()为Luban.CUSTOM_GEAR有效
                //.cropWH()// 裁剪宽高比，设置如果大于图片本身宽高则无效
                .rotateEnabled(false) // 裁剪是否可旋转图片
                //.scaleEnabled()// 裁剪是否可放大缩小图片
                //.recordVideoSecond()//录制视频秒数 默认60s
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }
    @Override
    protected void initView() {

    }


    @Override
    protected void initListener() {

    }

    private List<String> fileUrl = new ArrayList<>();
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PictureConfig.CHOOSE_REQUEST && data != null) {
            Log.d("tag", data.toString());
            List<LocalMedia> localMedia = PictureSelector.obtainMultipleResult(data);
            if(addGoodsDialog!=null&&addGoodsDialog.checkPicture){
                addGoodsDialog.setImageUrl(localMedia.get(0).getCompressPath());
            }else{
                if(localMedia!=null&&localMedia.size()>0){
                    fileUrl.add(localMedia.get(0).getCompressPath());
                    binding.checkImageGv.setDatas(fileUrl);
                    uploadImage(localMedia.get(0).getCompressPath());
                }
            }
        }else if(requestCode == 123&&data!=null){
           String result =  data.getStringExtra("result");
           binding.scanTextTv.setText(result);
        }
    }


    private void showPositionDialog()  {
        addGoodsDialog = AddGoodsDialog.getInstance();
        addGoodsDialog.checkPicture = false;
        addGoodsDialog.setCheckPositionSuccessListener(new AddGoodsDialog.CheckPositionSuccessListener() {
            @Override
            public void checkPositionSuccess(FurnitureListResult furnitureListResult, PositionResult positionResult) {
                furnitureListResultCheck = furnitureListResult;
                positionResultCheck = positionResult;
                String checkPositionStr = positionResultCheck.getPositionName()+"-"+furnitureListResultCheck.getFurnitureName();
                binding.checkPositionTv.setText(checkPositionStr);
                addGoodsDialog.dismiss();
            }
        });
        addGoodsDialog.setShowGravity(Gravity.BOTTOM);
        addGoodsDialog.show(getSupportFragmentManager());
    }

    @Override
    protected void initData() {

    }

    @Override
    public void showError(String msg) {

    }
    private void uploadImage(String imageFilePath){
        managerUrl = "AddGoosActivity"+System.currentTimeMillis();
        // 构造上传请求
        PutObjectRequest put = new PutObjectRequest("kdtantan", managerUrl, imageFilePath);

         // 异步上传时可以设置进度回调
        put.setProgressCallback(new OSSProgressCallback<PutObjectRequest>() {
            @Override
            public void onProgress(PutObjectRequest request, long currentSize, long totalSize) {
                Log.d("PutObject", "currentSize: " + currentSize + " totalSize: " + totalSize);
            }
        });

        OSSAsyncTask task = OSSService.getOss(ossBean).asyncPutObject(put, new OSSCompletedCallback<PutObjectRequest, PutObjectResult>() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                Log.d("PutObject", "UploadSuccess");
                if(imageUrl ==null){
                    imageUrl = new ArrayList<>();
                    imageUrl.add(AppConfig.PICTURE_URL+managerUrl);

                }else{
                    imageUrl.add(AppConfig.PICTURE_URL+managerUrl);
                }

            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientExcepion, ServiceException serviceException) {
                // 请求异常
                if (clientExcepion != null) {
                    // 本地异常如网络异常等
                    clientExcepion.printStackTrace();
                }
                if (serviceException != null) {
                    // 服务异常
                    Log.e("ErrorCode", serviceException.getErrorCode());
                    Log.e("RequestId", serviceException.getRequestId());
                    Log.e("HostId", serviceException.getHostId());
                    Log.e("RawMessage", serviceException.getRawMessage());
                }
            }
        });
        task.waitUntilFinished();
    }

    private void initTypeRequest(TypeBean typeBean){
        typeRequest = new TypeRequest();
        if(typeBean!=null){
            typeRequest.parentId = typeBean.parentId;
        }else{
            typeRequest.parentId = 0;
        }
        typeRequest.needGoodsCount = false;
    }
    private void searchGoods(String id){
        RetrofitUtils.getHttpService().getGoodMsg(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.<BaseHttpResult<GoodEntityBean>>bindUntilEvent(ActivityEvent.STOP))
                .subscribe(new BaseObserver<GoodEntityBean>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<GoodEntityBean> result) {
                        Log.d("tag",result.getData().toString());
                        goodEntityBean = result.getData();
                        setDefaultData();
                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {
                        showError(errMsg);

                    }
                });
    }
    private void addGoods(){
        if (createGoodRequest()) {
            RetrofitUtils.getHttpService().saveGood(addGoodRequest).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .compose(this.<BaseHttpResult<String>>bindUntilEvent(ActivityEvent.STOP))
                    .subscribe(new BaseObserver<String>(null) {
                        @Override
                        public void onSuccess(BaseHttpResult<String> result) {
                            ToastUtils.show("添加商品成功",1000);
                            finish();
                        }

                        @Override
                        public void onFailure(String errMsg, boolean isNetError) {
                            showError(errMsg);

                        }
                    });

        }

    }


    private void getType(final TypeBean typeBean){
        initTypeRequest(typeBean);
        RetrofitUtils.getHttpService().goodtype(typeRequest).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.<BaseHttpResult<List<TypeBean>>>bindUntilEvent(ActivityEvent.STOP))
                .subscribe(new BaseObserver<List<TypeBean>>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<List<TypeBean>> result) {
                          if(result!=null&&typeBean == null){
                              showTypeGoodsDialog(result.getData());
                          }else if(result!=null&&typeBean!=null&&typeBean.parentId==0&&checkTypeBean !=null){
                              showChildTypeDialog(result.getData());
                          }
                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {
                        showError(errMsg);

                    }
                });
    }

    public TypeBean checkTypeBean;

    public TypeBean checkChildBean;

    private void showTypeGoodsDialog(List<TypeBean> list){
        new  TypeGoodsDialog().setData(list).setOnItemClickListener(new TypeGoodsDialog.OnItemClickListener() {
            @Override
            public void onItemClick(TypeGoodsDialog typeGoodsDialog , TypeBean typeBean, int type) {
                checkTypeBean = typeBean;
                getType(typeBean);
                if(typeGoodsDialog!=null){
                    typeGoodsDialog.dismiss();
                }

            }
        }).setShowGravity(Gravity.BOTTOM)
                .show(getSupportFragmentManager()).setOutCancel(false);
    }

    private void showChildTypeDialog(List<TypeBean> list){
        new  TypeChildGoodsDialog().setData(list).setOnItemClickListener(new TypeChildGoodsDialog.OnItemClickListener() {
            @Override
            public void onItemClick(TypeChildGoodsDialog typeChildGoodsDialog,TypeBean typeBean, int type) {
                checkChildBean = typeBean;
                binding.checkTypeTv.setText(checkChildBean.typeName);
                typeChildGoodsDialog.dismiss();

            }
        }).setShowGravity(Gravity.BOTTOM)
                .show(getSupportFragmentManager()).setOutCancel(false);
    }

    @Override
    public void loginSuccess(String token) {

    }

    @Override
    public void ossGetTokenSuccess(OssBean ossBean) {
         this.ossBean = ossBean;
    }
}
