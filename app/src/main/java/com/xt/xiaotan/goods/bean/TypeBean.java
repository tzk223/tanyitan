package com.xt.xiaotan.goods.bean;

import java.io.Serializable;

public class TypeBean implements Serializable {

    /**
     * id : 9
     * typeName : 箱包手袋
     * typeIco : http://imagess.icloudinn.net/38662%E9%9E%8B%EF%BC%BC%E7%AE%B1%E5%8C%85%403x.png
     * typeIcoGray : http://imagess.icloudinn.net/29986%E9%9E%8B%EF%BC%BC%E7%AE%B1%E5%8C%85%403x.png
     * colorStart : #FFBC4C
     * colorEnd : #FF6F3C
     * parentId : 0
     * remark :
     * version : null
     * sortNum : 3
     * number : 0
     * price : null
     * dateStr : null
     * typeCount : null
     * createDateStr : null
     * subGoodType : null
     * needGoodsCount : null
     * goodsCount : null
     */

    public int id;
    public String typeName;
    public String typeIco;
    public String typeIcoGray;
    public String colorStart;
    public String colorEnd;
    public int parentId;
    public String remark;
    public Object version;
    public int sortNum;
    public int number;
    public Object price;
    public Object dateStr;
    public Object typeCount;
    public Object createDateStr;
    public Object subGoodType;
    public Object needGoodsCount;
    public Object goodsCount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeIco() {
        return typeIco;
    }

    public void setTypeIco(String typeIco) {
        this.typeIco = typeIco;
    }

    public String getTypeIcoGray() {
        return typeIcoGray;
    }

    public void setTypeIcoGray(String typeIcoGray) {
        this.typeIcoGray = typeIcoGray;
    }

    public String getColorStart() {
        return colorStart;
    }

    public void setColorStart(String colorStart) {
        this.colorStart = colorStart;
    }

    public String getColorEnd() {
        return colorEnd;
    }

    public void setColorEnd(String colorEnd) {
        this.colorEnd = colorEnd;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Object getVersion() {
        return version;
    }

    public void setVersion(Object version) {
        this.version = version;
    }

    public int getSortNum() {
        return sortNum;
    }

    public void setSortNum(int sortNum) {
        this.sortNum = sortNum;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public Object getDateStr() {
        return dateStr;
    }

    public void setDateStr(Object dateStr) {
        this.dateStr = dateStr;
    }

    public Object getTypeCount() {
        return typeCount;
    }

    public void setTypeCount(Object typeCount) {
        this.typeCount = typeCount;
    }

    public Object getCreateDateStr() {
        return createDateStr;
    }

    public void setCreateDateStr(Object createDateStr) {
        this.createDateStr = createDateStr;
    }

    public Object getSubGoodType() {
        return subGoodType;
    }

    public void setSubGoodType(Object subGoodType) {
        this.subGoodType = subGoodType;
    }

    public Object getNeedGoodsCount() {
        return needGoodsCount;
    }

    public void setNeedGoodsCount(Object needGoodsCount) {
        this.needGoodsCount = needGoodsCount;
    }

    public Object getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(Object goodsCount) {
        this.goodsCount = goodsCount;
    }
}
