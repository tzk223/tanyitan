package com.xt.xiaotan.goods.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xt.xiaotan.R;
import com.xt.xiaotan.goods.adapter.GridViewAdapter;

import java.util.List;

public class GrideView extends LinearLayout implements GridViewAdapter.OnCheckPictureListener{
    private View view;

    private Context context;

    private RecyclerView rv;

    private int lineNumber;


    private GridViewAdapter gridViewAdapter;

    private GrideView.OnCheckPictureListener onCheckPictureListener;

    public void setOnCheckPictureListener(GrideView.OnCheckPictureListener onCheckPictureListener) {
        this.onCheckPictureListener = onCheckPictureListener;
    }

    public GrideView(Context context) {
        super(context);
        this.context = context;
    }
    public GrideView(Context context, @Nullable AttributeSet attrs) {
        super(context,attrs);
        this.context = context;
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.GridLayoutView);
        lineNumber = array.getInt(R.styleable.GridLayoutView_lineNumber,3);
        array.recycle();
        initView();
        findContentView();
    }
    private void initView(){
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.territory_image_view,null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        this.addView(view,layoutParams);
    }

    private void findContentView(){
        rv = view.findViewById(R.id.rv);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context,lineNumber);
        rv.setLayoutManager(gridLayoutManager);
        gridViewAdapter = new GridViewAdapter(context);
        gridViewAdapter.setOnCheckPictureListener(this);
        rv.setAdapter(gridViewAdapter);
    }

    public void setDatas(List<String> list){

        gridViewAdapter.setData(list);
    }

    public void setShowAdd(boolean isShowAdd){
        gridViewAdapter.showAdd(isShowAdd);
    }

    @Override
    public void onCheckPicture(int type) {
        if(onCheckPictureListener!=null){
            onCheckPictureListener.onCheckPitcture(type);
        }
    }


    public interface OnCheckPictureListener{
        void onCheckPitcture(int type);
    }
   
}
