package com.xt.xiaotan.goods.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xt.xiaotan.R;
import com.xt.xiaotan.goods.bean.TypeBean;

import java.util.List;

public class TypeChildAdapter extends RecyclerView.Adapter<TypeChildAdapter.ViewHolder> {
public Context context;

private List<TypeBean> list;

public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        }

private OnItemClickListener onItemClickListener;
public TypeChildAdapter(Context context){
        this.context = context;
        }

public void setData(List<TypeBean> list){
        this.list = list;
        notifyDataSetChanged();
        }


public interface OnItemClickListener{
    void onItemClick(TypeBean typeBean, int type);
}
    @NonNull
    @Override
    public TypeChildAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.type_child_view, parent, false);
        return new TypeChildAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TypeChildAdapter.ViewHolder holder, int position) {
        final TypeBean typeBean = list.get(position);
        holder.name_tv.setText(typeBean.typeName);
        holder.check_type_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(typeBean,0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list!=null?list.size():0;
    }
class ViewHolder extends RecyclerView.ViewHolder{

    public TextView name_tv;
    public LinearLayout check_type_ll;


    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        name_tv = itemView.findViewById(R.id.name_tv);
        check_type_ll = itemView.findViewById(R.id.check_type_ll);
    }
}
}
