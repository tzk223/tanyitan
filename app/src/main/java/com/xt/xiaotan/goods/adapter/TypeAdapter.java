package com.xt.xiaotan.goods.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xt.xiaotan.R;
import com.xt.xiaotan.goods.bean.TypeBean;
import com.xt.xiaotan.search.adapter.GoodsAdapter;
import com.xt.xiaotan.search.model.GoodEntityBean;

import java.util.List;

public class TypeAdapter extends RecyclerView.Adapter<TypeAdapter.ViewHolder> {
    public Context context;

    private List<TypeBean> list;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private OnItemClickListener onItemClickListener;
    public TypeAdapter(Context context){
        this.context = context;
    }

    public void setData(List<TypeBean> list){
        this.list = list;
        notifyDataSetChanged();
    }


    public interface OnItemClickListener{
        void onItemClick(TypeBean typeBean,int type);
    }
    @NonNull
    @Override
    public TypeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.type_item, parent, false);
        return new TypeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TypeAdapter.ViewHolder holder, int position) {
        final TypeBean typeBean = list.get(position);
        Glide.with(context).load(typeBean.typeIcoGray).apply(new RequestOptions().error(R.drawable.ic_launcher_background)).into(holder.image_iv);
        holder.name_tv.setText(typeBean.typeName);
        holder.check_type_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              onItemClickListener.onItemClick(typeBean,0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list!=null?list.size():0;
    }
    class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView image_iv;
        public TextView name_tv;
        public LinearLayout check_type_ll;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image_iv = itemView.findViewById(R.id.image_iv);
            name_tv = itemView.findViewById(R.id.name_tv);
            check_type_ll = itemView.findViewById(R.id.check_type_ll);
        }
    }
}
