package com.xt.xiaotan.goods.adapter;


import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xt.xiaotan.R;
import com.xt.xiaotan.bluetooth.BlueToothSearchActivity;
import com.xt.xiaotan.search.model.GoodEntityBean;

import java.util.List;

public class GoodCanEditAdapter extends BaseQuickAdapter<GoodEntityBean, BaseViewHolder> {
    private boolean showDelete;
    private List<GoodEntityBean> data;
    private GoodCanEditAdapter.OnItemClickListener onItemClickListener;
    private Context context;
    public GoodCanEditAdapter(@Nullable List<GoodEntityBean> data, Context context) {
        super(R.layout.good_can_edit_item, data);
        this.data = data;
        this.context = context;
    }

    public void setOnItemClickListener(GoodCanEditAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setShowDelete(boolean showDelete) {
        this.showDelete = showDelete;
        notifyDataSetChanged();
    }

    @Override
    protected void convert(BaseViewHolder helper, GoodEntityBean item) {
        RadioButton radioButton = helper.itemView.findViewById(R.id.check_rb);
        radioButton.setVisibility(showDelete?View.VISIBLE:View.GONE);
        ImageView imageView = helper.itemView.findViewById(R.id.iamge);
        TextView name = helper.itemView.findViewById(R.id.name);
        TextView type = helper.itemView.findViewById(R.id.type);
        TextView position = helper.itemView.findViewById(R.id.position);
        TextView time = helper.itemView.findViewById(R.id.time);
        TextView search_tv = helper.itemView.findViewById(R.id.search_tv);
        LinearLayout check_good_ll = helper.itemView.findViewById(R.id.check_good_ll);
        Glide.with(context).load(item.photoUrl1).apply(new RequestOptions().error(R.drawable.ic_launcher_background)).into(imageView);
        name.setText(item.goodsName);
        if(item.goodsTypeSecond>0){
            type.setText("二级分类");
        }else{
            type.setText("一级分类");
        }
        position.setText(item.positionName+"-"+item.furnitureName);
        time.setText(item.createDateStr);
        radioButton.setChecked(item.isCheck);
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.isCheck = radioButton.isChecked();
                setDataNotCheck(item);
                notifyDataSetChanged();
                if(item.isCheck){
                    onItemClickListener.OnItemClick(item,1);
                }
            }
        });
        check_good_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.OnItemClick(item,2);
            }
        });
        search_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BlueToothSearchActivity.gotoBlueToothSearchActivity(context,item);
            }
        });

    }

    public interface OnItemClickListener{
        void OnItemClick(GoodEntityBean item,int type);
    }

    private void setDataNotCheck(GoodEntityBean furnitureListResult ){
        for(int i= 0;i<data.size();i++){
            GoodEntityBean furnitureListResult1 = data.get(i);
            if(furnitureListResult.isCheck&&furnitureListResult.id!=furnitureListResult1.id){
                furnitureListResult1.isCheck = false;
            }
        }
    }

}
