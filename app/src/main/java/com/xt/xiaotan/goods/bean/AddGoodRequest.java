package com.xt.xiaotan.goods.bean;

import java.io.Serializable;

public class AddGoodRequest implements Serializable {

    public String goodsName;

    public int goodsType;

    public int goodsTypeSecond;

    public int positionId;

    public String furnitureId;

    public String goodsRfid;

    public String price;

    public String photoUrl1;

    public String photoUrl2;

    public String photoUrl3;

    public String id;

    public String remarks;
}
