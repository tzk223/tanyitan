package com.xt.xiaotan.goods.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xt.xiaotan.R;
import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.goods.bean.TypeBean;
import com.xt.xiaotan.utils.DisplayUtils;

import java.util.List;

public class PositionChildAdapter extends RecyclerView.Adapter<PositionChildAdapter.ViewHolder> {
    public Context context;

    private List<FurnitureListResult> list;

    public void setOnItemClickListener(PositionChildAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private PositionChildAdapter.OnItemClickListener onItemClickListener;
    public PositionChildAdapter(Context context){
        this.context = context;
    }

    public void setData(List<FurnitureListResult> list){
        this.list = list;
        notifyDataSetChanged();
    }


    public interface OnItemClickListener{
        void onItemClick(FurnitureListResult typeBean, int type);
    }
    @NonNull
    @Override
    public PositionChildAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.position_child_item, parent, false);
        return new PositionChildAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PositionChildAdapter.ViewHolder holder, int position) {
        if(list!=null&&position < list.size()){
            final FurnitureListResult typeBean = list.get(position);
            holder.name_tv.setText(typeBean.getFurnitureName());
            holder.name_tv.setCompoundDrawables(null,null,null,null);
            holder.name_tv.setChecked(typeBean.isCheck);
            holder.position_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    typeBean.isCheck = !typeBean.isCheck;
                    FurnitureListResult positionResult =  isCheckAll(typeBean);
                    notifyDataSetChanged();
                    onItemClickListener.onItemClick(positionResult,1);
                }
            });
            if(typeBean.isCheck){
                holder.position_ll.setBackgroundResource(R.drawable.bg_ffbc4c_ff6f3c);
            }else{
                holder.position_ll.setBackgroundResource(R.drawable.bg_withe);
            }

        }else{
            holder.name_tv.setText("添加");
            Drawable drawable = context.getResources().getDrawable(R.mipmap.add_positon);
            drawable.setBounds(0,0, DisplayUtils.dip2px(context,15),DisplayUtils.dip2px(context,15));
            holder.name_tv.setCompoundDrawables(drawable,null,null,null);
            holder.name_tv.setChecked(false);
            holder.position_ll.setBackgroundResource(R.drawable.bg_withe);
            holder.position_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(null,0);
                }
            });
        }

    }
    private FurnitureListResult isCheckAll(FurnitureListResult positionResult){
        if(list!=null){
            for(FurnitureListResult result:list){
                if(positionResult.isCheck&&positionResult.getId()!=result.getId()){
                    result.isCheck = false;
                }

            }
            if(!positionResult.isCheck){
                list.get(0).isCheck = true;
            }
            return positionResult.isCheck?positionResult:list.get(0);
        }
        return null;
    }
    @Override
    public int getItemCount() {
        return list!=null?list.size()+1:1;
    }
    class ViewHolder extends RecyclerView.ViewHolder{

        public CheckedTextView name_tv;

        public LinearLayout position_ll;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name_tv = itemView.findViewById(R.id.name_tv);
            position_ll = itemView.findViewById(R.id.position_ll);
        }
    }
}
