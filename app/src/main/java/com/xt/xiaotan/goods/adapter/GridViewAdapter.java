package com.xt.xiaotan.goods.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xt.xiaotan.R;

import java.util.List;

public class GridViewAdapter extends RecyclerView.Adapter {
    private Context context;

    private List<String> list;

    private GridViewAdapter.OnCheckPictureListener onCheckPictureListener;

    private boolean showAdd = true;

    public void setOnCheckPictureListener(GridViewAdapter.OnCheckPictureListener onCheckPictureListener) {
        this.onCheckPictureListener = onCheckPictureListener;
    }

    public GridViewAdapter(Context context){
        this.context = context;
    }

    public void setData(List<String> list){
        this.list = list;
        notifyDataSetChanged();
    }

    public void showAdd(boolean showAdd){
        this.showAdd = showAdd;
        notifyDataSetChanged();
    }

    public interface OnCheckPictureListener{
        void onCheckPicture(int type);
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 1){
            View view = LayoutInflater.from(context).inflate(R.layout.check_image_item_new, parent, false);
            return new GridViewAdapter.ViewImageHolder(view);
        }else {
            View view = LayoutInflater.from(context).inflate(R.layout.image_item_new, parent, false);
            return new GridViewAdapter.ViewCheckHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof GridViewAdapter.ViewImageHolder){
            GridViewAdapter.ViewImageHolder viewCheckHolder = (GridViewAdapter.ViewImageHolder) holder;
            viewCheckHolder.check_imge_ll.setVisibility(showAdd||(list!=null&&list.size()<3)?View.VISIBLE:View.GONE);
            if(list!=null){
                viewCheckHolder.check_num_tv.setText("还可传"+(3-list.size())+"张");
            }else{
                viewCheckHolder.check_num_tv.setText("还可传"+3+"张");
            }
            viewCheckHolder.check_imge_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onCheckPictureListener!=null){
                        onCheckPictureListener.onCheckPicture(0);
                    }
                }
            });
        }else if(holder instanceof GridViewAdapter.ViewCheckHolder){
            GridViewAdapter.ViewCheckHolder viewCheckHolder = (GridViewAdapter
                    .ViewCheckHolder) holder;
            final String localMedia = list.get(position);
            viewCheckHolder.delete_tv.setVisibility(showAdd?View.VISIBLE:View.GONE);
            viewCheckHolder.delete_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list.remove(localMedia);
                    notifyDataSetChanged();
                }
            });
            viewCheckHolder.look_picture_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onCheckPictureListener!=null){
                        onCheckPictureListener.onCheckPicture(1);
                    }
                }
            });
            Glide.with(context).load(localMedia).apply(new RequestOptions().error(R.drawable.ic_launcher_background)).into(viewCheckHolder.image_iv);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(list == null||list.size() <=0||(list.size()>0&&position == list.size())){
            return 1;
        }
        return 2;
    }

    @Override
    public int getItemCount() {
        return list == null||list.size()==0?1:list.size()+1;
    }

    public class ViewImageHolder extends RecyclerView.ViewHolder{
        public FrameLayout check_imge_ll;

        public TextView check_num_tv;

        public ViewImageHolder(View itemView) {
            super(itemView);
            check_imge_ll = itemView.findViewById(R.id.check_imge_ll);
            check_num_tv = itemView.findViewById(R.id.check_num_tv);
        }


    }

    public class ViewCheckHolder extends RecyclerView.ViewHolder{
        public ImageView image_iv;

        public ImageView delete_tv;

        public LinearLayout look_picture_ll;

        public ViewCheckHolder(View itemView) {
            super(itemView);
            image_iv = itemView.findViewById(R.id.image_iv);
            delete_tv = itemView.findViewById(R.id.delete_tv);
            look_picture_ll = itemView.findViewById(R.id.look_picture_ll);
        }
    }
}
