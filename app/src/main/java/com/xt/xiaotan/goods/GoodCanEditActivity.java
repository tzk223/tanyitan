package com.xt.xiaotan.goods;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.databinding.GoodCanEditActivityBinding;
import com.xt.xiaotan.first.model.GoodsRequst;
import com.xt.xiaotan.goods.adapter.GoodCanEditAdapter;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.search.model.GoodEntityBean;
import com.xt.xiaotan.search.model.SearchFilter;
import com.xt.xiaotan.search.searchmvp.SearchContract;
import com.xt.xiaotan.search.searchmvp.SearchPresenter;

import java.util.ArrayList;
import java.util.List;

public class GoodCanEditActivity extends BaseMvpActivity implements  SearchContract.View{
    private GoodCanEditActivityBinding binding;

    private GoodsRequst goodsRequst;
    private SearchPresenter searchPresenter;
    private GoodCanEditAdapter goodCanEditAdapter;

    private List<GoodEntityBean> list = new ArrayList<>();

    public static void gotoGoodCanEditActivity(Context context,GoodsRequst goodsRequst){
        Intent intent = new Intent(context,GoodCanEditActivity.class);
        intent.putExtra("GoodsRequst",goodsRequst);
        context.startActivity(intent);
    }
    @Override
    protected BasePresenter createPresenter() {
        searchPresenter = new SearchPresenter(this);
        return null;
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected void getIntent(Intent intent) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.good_can_edit_activity);
        binding.setGoodCanEditActivity(this);
        super.onCreate(savedInstanceState);
        goodsRequst = (GoodsRequst) getIntent().getSerializableExtra("GoodsRequst");
        searchPresenter.getGoodsList(goodsRequst);
        goodCanEditAdapter = new GoodCanEditAdapter(list,this);
        goodCanEditAdapter.setOnItemClickListener(new GoodCanEditAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(GoodEntityBean item,int type) {
                if(type == 2){
                    AddGoodsActivity.gotoAddGoodsActivity(GoodCanEditActivity.this,item);
                }

            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.rlFurniture.setLayoutManager(linearLayoutManager);
        binding.rlFurniture.setAdapter(goodCanEditAdapter);
        if(!TextUtils.isEmpty(goodsRequst.title)){
            binding.title.setText(goodsRequst.title+"物品");
        }

        binding.backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        binding.editTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.editTv.getText().equals("编辑")){
                    binding.editTv.setText("取消");
                    goodCanEditAdapter.setShowDelete(true);
                    binding.deleteBtn.setVisibility(View.VISIBLE);
                }else if(binding.editTv.getText().equals("取消")){
                    binding.editTv.setText("编辑");
                    goodCanEditAdapter.setShowDelete(false);
                    binding.deleteBtn.setVisibility(View.GONE);
                }

            }
        });
        binding.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public void showError(String msg) {

    }

    @Override
    public void getGoodFilter(SearchFilter searchFilter) {

    }

    @Override
    public void getGoodsList(List<GoodEntityBean> list) {
        this.list.addAll(list);
        goodCanEditAdapter.notifyDataSetChanged();
    }
}
