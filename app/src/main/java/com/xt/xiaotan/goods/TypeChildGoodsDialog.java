package com.xt.xiaotan.goods;

import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseDialogFragment;
import com.xt.xiaotan.base.ViewHolder;
import com.xt.xiaotan.goods.adapter.TypeAdapter;
import com.xt.xiaotan.goods.adapter.TypeChildAdapter;
import com.xt.xiaotan.goods.bean.TypeBean;

import java.util.List;

public class TypeChildGoodsDialog extends BaseDialogFragment {
    private RecyclerView type_rv;

    private TypeChildAdapter typeAdapter;

    private List<TypeBean> typeBeans;

    public TypeChildGoodsDialog setOnItemClickListener(TypeChildGoodsDialog.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        return this;
    }

    private TypeChildGoodsDialog.OnItemClickListener onItemClickListener;

    public TypeChildGoodsDialog setData(List<TypeBean> typeBeans){
        this.typeBeans = typeBeans;
        return this;
    }
    @Override
    public int setUpLayoutId() {
        return R.layout.add_type_dialog;
    }

    @Override
    public void convertView(ViewHolder holder, BaseDialogFragment dialog) {
        holder.getView(R.id.close_tv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TypeChildGoodsDialog.this.dismiss();
            }
        });
        type_rv = holder.getView(R.id.type_rv);
        typeAdapter = new TypeChildAdapter(getActivity());
        typeAdapter.setOnItemClickListener(new TypeChildAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(TypeBean typeBean, int type) {
                onItemClickListener.onItemClick(TypeChildGoodsDialog.this,typeBean,type);
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),1);
        type_rv.setLayoutManager(gridLayoutManager);
        type_rv.setAdapter(typeAdapter);
        typeAdapter.setData(typeBeans);
    }

    public interface OnItemClickListener{
        void onItemClick(TypeChildGoodsDialog typeChildGoodsDialog,TypeBean typeBean, int type);
    }
}
