package com.xt.xiaotan.first;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ConditionVariable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.base.EditDialog;
import com.xt.xiaotan.databinding.EditFurnitureActivityBinding;
import com.xt.xiaotan.first.adapter.EditFurnAdapter;
import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.first.mvp.EditFContract;
import com.xt.xiaotan.first.mvp.EditFPesenter;
import com.xt.xiaotan.goods.bean.FurnitureRequest;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.utils.RetrofitUtils;
import com.xt.xiaotan.utils.ToastUtils;
import com.xt.xiaotan.view.SpaceItemDecoration;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by tiandi
 * on 2020/11/26
 */
public class EditFurnitureActivity extends BaseMvpActivity<EditFPesenter> implements EditFContract.View{
    private EditFurnitureActivityBinding binding;
    private EditFurnAdapter editFurnAdapter;
    private List<FurnitureListResult> listResults = new ArrayList<>();
    private String id;
    private FurnitureListResult deleteFurnitureListResult;

    public static void gotoEditFurnActivity(Context context,String id){
        Intent intent = new Intent(context,EditFurnitureActivity.class);
        intent.putExtra("id",id);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this,R.layout.edit_furniture_activity);
        binding.setEditFActivity(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected EditFPesenter createPresenter() {
        return new EditFPesenter(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.edit_furniture_activity;
    }

    @Override
    protected void getIntent(Intent intent) {
         id = intent.getStringExtra("id");
        mPresenter.getFurnitureList(id,true);
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {
        editFurnAdapter = new EditFurnAdapter(listResults);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.rlFurniture.setLayoutManager(linearLayoutManager);
        View view  = LayoutInflater.from(this).inflate(R.layout.item_foot_add,null);
        LinearLayout cardView = view.findViewById(R.id.foot_cv);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditDialog editDialog = EditDialog.getInstance("卧室家具","家具名称",12,
                        "请输入家具名称","确定",true);
                editDialog.setOnSureClickListener(new EditDialog.OnSureClickListener() {
                    @Override
                    public void onSureClick(String sureStr) {
                        FurnitureRequest furnitureRequest = new FurnitureRequest();
                        furnitureRequest.positionId = id;
                        furnitureRequest.furnitureName = sureStr;
                        saveFurniture(furnitureRequest);
                    }
                });
                editDialog.show(getSupportFragmentManager(),"editDialog");
            }
        });
        binding.backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        binding.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(deleteFurnitureListResult!=null){
                    deleteFurniture(deleteFurnitureListResult.getId());
                }
            }
        });
        binding.editTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editFurnAdapter!=null){
                    editFurnAdapter.setShowDelete(true);
                    binding.deleteBtn.setVisibility(View.VISIBLE);
                }
            }
        });

        editFurnAdapter.addFooterView(view);
        editFurnAdapter.setOnItemClickListener(new EditFurnAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(FurnitureListResult item) {
                if(item!=null){
                    deleteFurnitureListResult = item;
                }
            }
        });
        binding.rlFurniture.setAdapter(editFurnAdapter);

    }
    private void saveFurniture(FurnitureRequest saveFurnitureRequst){
        RetrofitUtils.getHttpService().saveFurniture(saveFurnitureRequst).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<Boolean>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<Boolean> result) {
                        Log.d("tag","是否成功");
                        ToastUtils.show("上传家具数据成功",1000);
                        mPresenter.getFurnitureList(id,true);
                    }
                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {
                        ToastUtils.show("上传家具数据失败",1000);
                    }
                });
    }

    private void deleteFurniture(String funId){
        RetrofitUtils.getHttpService().deleteFurniture(funId).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<Boolean>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<Boolean> result) {
                        Log.d("tag","是否成功");
                        ToastUtils.show("删除房间成功",5000);
                        editFurnAdapter.setShowDelete(false);
                        binding.deleteBtn.setVisibility(View.GONE);
                        mPresenter.getFurnitureList(id,true);
                    }
                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {
                        ToastUtils.show("删除家具失败",5000);
                    }
                });
    }
    @Override
    public void showError(String msg) {

    }

    @Override
    public void getFurnitureList(List<FurnitureListResult> listResults) {

        this.listResults.clear();
        this.listResults.addAll(listResults);
        editFurnAdapter.notifyDataSetChanged();


    }
}
