package com.xt.xiaotan.first;

import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.stx.xhb.androidx.XBanner;
import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseFragment;
import com.xt.xiaotan.first.adapter.MainPositionAdapter;
import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.first.model.GoodsRequst;
import com.xt.xiaotan.first.model.PositionResult;


import com.xt.xiaotan.first.positionmvp.MainPositionContract;
import com.xt.xiaotan.first.positionmvp.MainPositionPresenter;
import com.xt.xiaotan.goods.GoodCanEditActivity;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.utils.DisplayUtils;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class MainPositionFragment extends BaseFragment implements MainPositionContract.View {
    private XBanner xBanner;
    private List<PositionResult> list = new ArrayList<>();
    private LinearLayout banner_sign;
    private List<View> views = new ArrayList<>();
    private RecyclerView position_rv;

    private TextView empity_tv;
    private MainPositionPresenter mainPositionPresenter;
    private MainPositionAdapter positionAdapter;
    private List<FurnitureListResult> listResults = new ArrayList<>();

    private PositionResult checkPositionResult ;

    private boolean isAddFoot;

    public static Fragment getMainPositionFragment(){
        return new MainPositionFragment();
    }
    @Override
    protected int getLayoutId() {
        return R.layout.mian_position_fragment;
    }

    @Override
    protected BasePresenter createPresenter() {
        mainPositionPresenter = new MainPositionPresenter(this);
        return mainPositionPresenter;
    }



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void getRoomList(List<PositionResult> list) {
        this.list = list;
        if(this.list ==null ||this.list.size() == 0){
            empity_tv.setVisibility(View.VISIBLE);
            return;
        }
        empity_tv.setVisibility(View.GONE);
        View footView = LayoutInflater.from(getActivity()).inflate(R.layout.position_add_item,null);
        if(!isAddFoot){
            isAddFoot = true;
            positionAdapter.addFooterView(footView);
        }

        footView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkPositionResult!=null){
                    EditFurnitureActivity.gotoEditFurnActivity(getActivity(),checkPositionResult.getId()+"");
                }
            }
        });
        if(null != list&&list.size()>0) {
            checkPositionResult = list.get(0);
            mainPositionPresenter.getFurnitureList(list.get(0).getId()+"",true);
        }
        xBanner.setBannerData(R.layout.main_position_item,list);
        xBanner.loadImage(new XBanner.XBannerAdapter() {
            @Override
            public void loadBanner(XBanner banner, Object model, View view, int position) {
                ImageView imageView = view.findViewById(R.id.position_iv);
                TextView textView = view.findViewById(R.id.tv_room_name);
                PositionResult positionResult = (PositionResult) model;
                Log.e("tag","positionResult.getPositionName() = "+positionResult.getPositionName());
                Glide.with(getActivity()).load(positionResult.getXBannerUrl()).into(imageView);
                textView.setText(positionResult.getPositionName());
            }
        });
        xBanner.setOnItemClickListener(new XBanner.OnItemClickListener() {
            @Override
            public void onItemClick(XBanner banner, Object model, View view, int position) {
                PositionResult positionResult = (PositionResult) model;
                RoomDesActivity.gotoRoomDesActivity(getActivity(),positionResult.getId()+"");
            }
        });
        xBanner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for(int i=0;i<views.size();i++){
                    if(position == i){
                        views.get(i).setBackground(getResources().getDrawable(R.drawable.bg_banner_check_sign_fff36612,null));
                    }else {
                        views.get(i).setBackground(getResources().getDrawable(R.drawable.bg_banner_not_check_ffdcdcdc,null));
                    }
                }
                checkPositionResult = MainPositionFragment.this.list.get(position);
                mainPositionPresenter.getFurnitureList(MainPositionFragment.this.list.get(position).getId()+"",true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setBannerSign();
    }

    @Override
    public void getFurnitureList(List<FurnitureListResult> listResults) {
        this.listResults.clear();
        this.listResults.addAll(listResults);
        positionAdapter.notifyDataSetChanged();


    }

    @Override
    public void onResume() {
        super.onResume();
        mainPositionPresenter.getRoomList();
    }

    @Override
    public void onStop() {
        super.onStop();
        xBanner.stopAutoPlay();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void initView() {
        mainPositionPresenter.getRoomList();
        xBanner = mRootView.findViewById(R.id.banner);
        banner_sign = mRootView.findViewById(R.id.banner_sign);
        position_rv = mRootView.findViewById(R.id.position_rv);
        empity_tv = mRootView.findViewById(R.id.empity_tv);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),3);
        position_rv.setLayoutManager(gridLayoutManager);
        positionAdapter = new MainPositionAdapter(listResults);
        positionAdapter.setOnItemClickListener(new MainPositionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(FurnitureListResult item) {
                GoodsRequst goodsRequst = new GoodsRequst();
                goodsRequst.title = item.getFurnitureName();
                goodsRequst.current = 1;
                goodsRequst.size = 10;
                goodsRequst.furnitureId = item.getId();
                goodsRequst.positionId = item.getPositionId();
                GoodCanEditActivity.gotoGoodCanEditActivity(getActivity(),goodsRequst);
            }
        });
        views.clear();
        position_rv.setAdapter(positionAdapter);
        xBanner.setAutoPlayAble(list.size() > 1);
        xBanner.setIsClipChildrenMode(true);

        xBanner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onPageSelected(int position) {
                for(int i=0;i<views.size();i++){
                    if(position == i){
                        views.get(i).setBackground(getResources().getDrawable(R.drawable.bg_banner_check_sign_fff36612,null));
                    }else {
                        views.get(i).setBackground(getResources().getDrawable(R.drawable.bg_banner_not_check_ffdcdcdc,null));
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setBannerSign(){
        views.clear();
        banner_sign.removeAllViews();
        LayoutInflater layoutInflater = getLayoutInflater();
        if(list!=null&&list.size()>0){
            int width = DisplayUtils.dip2px(getActivity(),20);
            int height = DisplayUtils.dip2px(getActivity(),3);
            for(int i=0;i<list.size();i++){
                View view = layoutInflater.inflate(R.layout.banner_sign_item,null);
                LinearLayout banner_sign_iv = view.findViewById(R.id.banner_sign_iv);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, height);
                layoutParams.leftMargin = 5;
                layoutParams.rightMargin = 5;
                views.add(banner_sign_iv);
                if(i==0){
                    view.setBackground(getResources().getDrawable(R.drawable.bg_banner_check_sign_fff36612,null));
                }else{
                    view.setBackground(getResources().getDrawable(R.drawable.bg_banner_not_check_ffdcdcdc,null));
                }
                banner_sign.addView(view,layoutParams);
            }

        }
    }

    @Override
    protected void initListener() {

    }


    @Override
    protected void initData() {

    }

    @Override
    protected boolean useEventBus() {
        return false;
    }

    @Override
    public void showError(String msg) {

    }

}
