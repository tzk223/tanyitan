package com.xt.xiaotan.first.mvp;

import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.mvp.BaseModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.utils.RetrofitUtils;

import io.reactivex.Observable;

/**
 * Created by tiandi
 * on 2020/11/24
 */
class RoomDesModel extends BaseModel implements RoomDesContract.Model {
    @Override
    public Observable<BaseHttpResult<PositionResult>> getRoomDes(String id) {
        return RetrofitUtils.getHttpService().getRoomDes(id);
    }
}
