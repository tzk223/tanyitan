package com.xt.xiaotan.first.mvp;

import com.xt.xiaotan.first.model.FirstResult;


import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.mvp.IView;
import com.xt.xiaotan.net.BaseHttpResult;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by tiandi
 * on 2020/11/10
 */
public interface MainContract {
    public interface View extends IView {
       void getBannerList(List<FirstResult> firstResults);
        void getCountList(String firstResults);


    }

    public interface Model extends IModel {
        Observable<BaseHttpResult<List<FirstResult>>> getBannerList();

        Observable<BaseHttpResult<String>> getCountList();

    }
}
