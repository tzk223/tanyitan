package com.xt.xiaotan.first.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xt.xiaotan.R;
import com.xt.xiaotan.set.InfoModel;

import java.util.List;

public class LoveUserAdapter extends BaseQuickAdapter<InfoModel.SurveysBean, BaseViewHolder> {
    public LoveUserAdapter( @Nullable List<InfoModel.SurveysBean> data) {
        super(R.layout.type_content_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, InfoModel.SurveysBean item) {
        helper.setText(R.id.check_tv,item.getSurveyName())
                .setChecked(R.id.check_tv,item.isIsSelected());
        helper.setVisible(R.id.iv_sign,item.isIsSelected());
        helper.setBackgroundRes(R.id.check_tv,item.isIsSelected()?R.drawable.bg_gray_f7:R.drawable.bg_gray);


    }
}
