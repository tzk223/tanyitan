package com.xt.xiaotan.first.mvp;

import android.util.Log;

import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;

import java.util.List;

/**
 * Created by tiandi
 * on 2020/11/26
 */
public class AddGoodPesenter extends BasePresenter<AddGoodModel,AddGoodContract.View> {
    private AddGoodContract.View view;
    public AddGoodPesenter(AddGoodContract.View view){
        this.view = view;

    }


    @Override
    protected AddGoodModel createModel() {
        return new AddGoodModel();
    }

    public void getFurnitureList(String id,boolean isNeed){
        addDispose(createModel().furnitureList(id,isNeed), new BaseObserver<List<FurnitureListResult>>(view) {
            @Override
            public void onSuccess(BaseHttpResult<List<FurnitureListResult>> result) {
                view.getFurnitureList(result.getData());
                Log.e("tag","onSuccess"+result.getCode());



            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {
                Log.e("tag","onFailure"+errMsg);

            }

        });
    }
}
