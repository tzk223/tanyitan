package com.xt.xiaotan.first.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public class GoodsClassResult implements Serializable {


        /**
         * id : 7
         * typeName : 服饰鞋帽
         * typeIco : http://kdtantan.oss-cn-beijing.aliyuncs.com/27116%E6%9C%8D%E9%A5%B0%403x.png
         * typeIcoGray : http://kdtantan.oss-cn-beijing.aliyuncs.com/37747%E6%9C%8D%E9%A5%B0%403x.png
         * colorStart : #4F8FFF
         * colorEnd : #5A7EFF
         * parentId : 0
         * remark :
         * version : null
         * sortNum : 1
         * number : 0
         * price : null
         * dateStr : null
         * typeCount : null
         * createDateStr : null
         * subGoodType : null
         * needGoodsCount : null
         * goodsCount : null
         */

        private int id;
        private String typeName;
        private String typeIco;
        private String typeIcoGray;
        private String colorStart;
        private String colorEnd;
        private int parentId;
        private String remark;
        private Object version;
        private int sortNum;
        private int number;
        private Object price;
        private Object dateStr;
        private Object typeCount;
        private Object createDateStr;
        private Object subGoodType;
        private Object needGoodsCount;
        private int goodsCount;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTypeName() {
            return typeName;
        }

        public void setTypeName(String typeName) {
            this.typeName = typeName;
        }

        public String getTypeIco() {
            return typeIco;
        }

        public void setTypeIco(String typeIco) {
            this.typeIco = typeIco;
        }

        public String getTypeIcoGray() {
            return typeIcoGray;
        }

        public void setTypeIcoGray(String typeIcoGray) {
            this.typeIcoGray = typeIcoGray;
        }

        public String getColorStart() {
            return colorStart;
        }

        public void setColorStart(String colorStart) {
            this.colorStart = colorStart;
        }

        public String getColorEnd() {
            return colorEnd;
        }

        public void setColorEnd(String colorEnd) {
            this.colorEnd = colorEnd;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public Object getVersion() {
            return version;
        }

        public void setVersion(Object version) {
            this.version = version;
        }

        public int getSortNum() {
            return sortNum;
        }

        public void setSortNum(int sortNum) {
            this.sortNum = sortNum;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public Object getPrice() {
            return price;
        }

        public void setPrice(Object price) {
            this.price = price;
        }

        public Object getDateStr() {
            return dateStr;
        }

        public void setDateStr(Object dateStr) {
            this.dateStr = dateStr;
        }

        public Object getTypeCount() {
            return typeCount;
        }

        public void setTypeCount(Object typeCount) {
            this.typeCount = typeCount;
        }

        public Object getCreateDateStr() {
            return createDateStr;
        }

        public void setCreateDateStr(Object createDateStr) {
            this.createDateStr = createDateStr;
        }

        public Object getSubGoodType() {
            return subGoodType;
        }

        public void setSubGoodType(Object subGoodType) {
            this.subGoodType = subGoodType;
        }

        public Object getNeedGoodsCount() {
            return needGoodsCount;
        }

        public void setNeedGoodsCount(Object needGoodsCount) {
            this.needGoodsCount = needGoodsCount;
        }

        public int getGoodsCount() {
            return goodsCount;
        }

        public void setGoodsCount(int goodsCount) {
            this.goodsCount = goodsCount;
        }
    }

