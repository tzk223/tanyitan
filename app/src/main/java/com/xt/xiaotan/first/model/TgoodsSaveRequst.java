package com.xt.xiaotan.first.model;

import java.io.Serializable;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public class TgoodsSaveRequst implements Serializable {
//    {
//        "id":1,                //类型：Number  可有字段  备注：物品id，创建时为空
//            "goodsName":"玩具车",                //类型：String  必有字段  备注：物品名称
//            "goodsType":2,                //类型：Number  必有字段  备注：一级分类
//            "goodsTypeSecond":1,                //类型：Number  必有字段  备注：二级分类
//            "positionId":1,                //类型：Number  必有字段  备注：房间id
//            "furnitureId":1,                //类型：Number  必有字段  备注：家具id
//            "goodsRfid":"3000E0001",                //类型：String  必有字段  备注：RFID
//            "price":50,                //类型：Number  必有字段  备注：物品价格
//            "photoUrl1":"http://localhost:8088/blade/upload/image/20180423/1524466587187.jpg",                //类型：String  必有字段  备注：图片1
//            "photoUrl2":"http://localhost:8088/blade/upload/image/20180423/1524466587187.jpg",                //类型：String  必有字段  备注：图片2
//            "photoUrl3":"http://localhost:8088/blade/upload/image/20180423/1524466587187.jpg"                //类型：String  必有字段  备注：图片3
//    }

    private String id;
    private String goodsName;
    private String goodsType;
    private String goodsTypeSecond;
    private String positionId;
    private String furnitureId;
    private String goodsRfid;
    private String price;
    private String photoUrl1;
    private String photoUrl2;
    private String photoUrl3;


    public String getId() {
        return id == null ? "" : id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGoodsName() {
        return goodsName == null ? "" : goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsType() {
        return goodsType == null ? "" : goodsType;
    }

    public void setGoodsType(String goodsType) {
        this.goodsType = goodsType;
    }

    public String getGoodsTypeSecond() {
        return goodsTypeSecond == null ? "" : goodsTypeSecond;
    }

    public void setGoodsTypeSecond(String goodsTypeSecond) {
        this.goodsTypeSecond = goodsTypeSecond;
    }

    public String getPositionId() {
        return positionId == null ? "" : positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getFurnitureId() {
        return furnitureId == null ? "" : furnitureId;
    }

    public void setFurnitureId(String furnitureId) {
        this.furnitureId = furnitureId;
    }

    public String getGoodsRfid() {
        return goodsRfid == null ? "" : goodsRfid;
    }

    public void setGoodsRfid(String goodsRfid) {
        this.goodsRfid = goodsRfid;
    }

    public String getPrice() {
        return price == null ? "" : price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPhotoUrl1() {
        return photoUrl1 == null ? "" : photoUrl1;
    }

    public void setPhotoUrl1(String photoUrl1) {
        this.photoUrl1 = photoUrl1;
    }

    public String getPhotoUrl2() {
        return photoUrl2 == null ? "" : photoUrl2;
    }

    public void setPhotoUrl2(String photoUrl2) {
        this.photoUrl2 = photoUrl2;
    }

    public String getPhotoUrl3() {
        return photoUrl3 == null ? "" : photoUrl3;
    }

    public void setPhotoUrl3(String photoUrl3) {
        this.photoUrl3 = photoUrl3;
    }
}
