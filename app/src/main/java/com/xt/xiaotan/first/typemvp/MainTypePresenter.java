package com.xt.xiaotan.first.typemvp;

import android.util.Log;

import com.xt.xiaotan.first.model.GoodsClassResult;

import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;

import java.util.List;

/**
 * Created by tiandi
 * on 2020/11/10
 */
public class MainTypePresenter extends BasePresenter {
    public MainTypeContract.View view;
    public MainTypeModel mainModel;
    @Override
    protected IModel createModel() {
        mainModel = new MainTypeModel();
        return mainModel;
    }
    public MainTypePresenter(MainTypeContract.View view){
        this.view = view;
    }





    public void getGoodClassList(){
        addDispose(mainModel.getGoodClassList(), new BaseObserver<List<GoodsClassResult>>(view) {
            @Override
            public void onSuccess(BaseHttpResult<List<GoodsClassResult>> result) {
                Log.e("tag","onSuccess"+result.getCode());
                view.getGoodsList(result.getData());


            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {
                Log.e("tag","onFailure"+errMsg);

            }

        });

    }


}
