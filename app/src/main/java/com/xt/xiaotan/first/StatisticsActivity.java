package com.xt.xiaotan.first;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;

import android.view.Gravity;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;


import com.alibaba.sdk.android.oss.ClientConfiguration;

import com.alibaba.sdk.android.oss.OSS;
import com.alibaba.sdk.android.oss.OSSClient;

import com.alibaba.sdk.android.oss.common.OSSLog;
import com.alibaba.sdk.android.oss.common.auth.OSSAuthCredentialsProvider;
import com.alibaba.sdk.android.oss.common.auth.OSSCredentialProvider;

import com.bigkoo.pickerview.TimePickerView;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseActivity;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.databinding.StatisticsActivityBinding;
import com.xt.xiaotan.first.model.StatisResult;
import com.xt.xiaotan.first.typemvp.StatisPresenter;
import com.xt.xiaotan.first.typemvp.StatisView;
import com.xt.xiaotan.goods.TypeGoodsDialog;
import com.xt.xiaotan.goods.bean.TypeBean;
import com.xt.xiaotan.goods.bean.TypeRequest;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.oss.Config;
import com.xt.xiaotan.oss.UIDisplayer;
import com.xt.xiaotan.oss.service.OssService;
import com.xt.xiaotan.utils.Logs;
import com.xt.xiaotan.utils.RetrofitUtils;
import com.xt.xiaotan.view.NumberLineView;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import pub.devrel.easypermissions.EasyPermissions;

public class StatisticsActivity extends BaseMvpActivity<StatisPresenter> implements
        View.OnClickListener , EasyPermissions.PermissionCallbacks, StatisView {
    private StatisticsActivityBinding binding;
    private StatisResult statisResult;

    private TextView check_date_tv;

    private ArrayList<Integer> timeList;

    private ArrayList<String> dataList;

    private TypeRequest typeRequest;

    private List<TypeBean> typeBeans;
    @Override
    public int getLayoutId() {
        return 0;
    }

    @Override
    public void getIntent(Intent intent) {

    }

    public static void gotoStatisticsActivity(Context context){
        Intent intent = new Intent(context,StatisticsActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this,R.layout.statistics_activity);
//        if(statisResult == null){
//            statisResult = new StatisResult();
//            statisResult.setPriceSum("100");
//        }

        binding.setStatisModel(statisResult);
        super.onCreate(savedInstanceState);
        getType(null);
    }

    @Override
    protected StatisPresenter createPresenter() {
        return new StatisPresenter(this);
    }

    @Override
    public void initView() {
//        setContentView(getLayoutId());
    }

    @Override
    public void initListener() {
        binding.backIv.setOnClickListener(this);
        binding.checkDateTv.setOnClickListener(this);
        binding.checkTypeTv.setOnClickListener(this);
    }

    @Override
    public void initData() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月");
        binding.checkDateTv.setText(formatter.format(new Date()));
        mPresenter.getStatis(checkTypeBean!=null?checkTypeBean.id+"":"",formatter.format(new Date()));
        timeList = new ArrayList<>();
        dataList = new ArrayList<>();
        for(int i=0;i<7;i++){
            Random random = new Random();
            int methed = (random.nextInt(30));
            timeList.add(methed);
        }

        for(int i=0;i<7;i++){
            dataList.add("1号"+i);
        }
        binding.numberLv.updateTime(timeList,dataList);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.back_iv){
            finish();
        }else if(v.getId() == R.id.check_date_tv){
            showTimePack(binding.checkDateTv);
        }else if(v.getId() == R.id.check_type_tv){
            if(typeBeans == null){
                getType(null);
            }else{
                showTypeGoodsDialog(typeBeans);
            }

        }
    }
    private void initTypeRequest(TypeBean typeBean){
        typeRequest = new TypeRequest();
        if(typeBean!=null){
            typeRequest.parentId = typeBean.parentId;
        }else{
            typeRequest.parentId = 0;
        }
        typeRequest.needGoodsCount = false;
    }
    private void getType(final TypeBean typeBean){
        initTypeRequest(typeBean);
        RetrofitUtils.getHttpService().goodtype(typeRequest).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.<BaseHttpResult<List<TypeBean>>>bindUntilEvent(ActivityEvent.STOP))
                .subscribe(new BaseObserver<List<TypeBean>>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<List<TypeBean>> result) {
                        if(result!=null&&typeRequest.parentId ==0&&checkTypeBean == null){
                            typeBeans =  result.getData();
                            if(typeBeans!=null&&typeBeans.size()>0){
                                checkTypeBean = typeBeans.get(0);
                                binding.checkTypeTv.setText(typeBeans.get(0).typeName);
                                mPresenter.getStatis(checkTypeBean!=null?checkTypeBean.id+"":"",binding.checkDateTv.getText().toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {
                        showError(errMsg);

                    }
                });
    }
    public TypeBean checkTypeBean;
    private void showTypeGoodsDialog(List<TypeBean> list){
        new TypeGoodsDialog().setData(list).setOnItemClickListener(new TypeGoodsDialog.OnItemClickListener() {
            @Override
            public void onItemClick(TypeGoodsDialog typeGoodsDialog , TypeBean typeBean, int type) {
                checkTypeBean = typeBean;
                mPresenter.getStatis(checkTypeBean!=null?checkTypeBean.id+"":"",binding.checkDateTv.getText().toString());
                if(typeGoodsDialog!=null){
                    typeGoodsDialog.dismiss();
                }

            }
        }).setShowGravity(Gravity.BOTTOM)
                .show(getSupportFragmentManager()).setOutCancel(false);
    }

    private void showTimePack(TextView textView){
        Calendar calendar = Calendar.getInstance();

        TimePickerView timePickerView = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月");
                textView.setText(formatter.format(date));

                mPresenter.getStatis(checkTypeBean!=null?checkTypeBean.id+"":"",binding.checkDateTv.getText().toString());
            }
        }).setType(new boolean[]{true, true, false, false, false, false})
                .setCancelColor(Color.parseColor("#F77B22"))
                .setTitleText("选择时间")
                .setRangDate(null,calendar)
                .setTitleColor(Color.parseColor("#666666"))
                .setSubmitColor(Color.parseColor("#F77B22")).build();
        //注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
        timePickerView.setDate(Calendar.getInstance());
        timePickerView.show();
    }


    @Override
    public void onPermissionsGranted(int i, @NonNull List<String> list) {

    }

    @Override
    public void onPermissionsDenied(int i, @NonNull List<String> list) {

    }


    @Override
    public void showError(String msg) {

    }

    @Override
    public void getStatisResult(StatisResult s) {

        this.statisResult = s;
        binding.setStatisModel(statisResult);
        Logs.e("tag","statisResult.getPriceSum()="+statisResult.getPriceSum());

    }
}
