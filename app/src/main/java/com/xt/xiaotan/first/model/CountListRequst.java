package com.xt.xiaotan.first.model;

import java.io.Serializable;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public class CountListRequst implements Serializable {
//    "goodTypeId":1,                //类型：Number  可有字段  备注：分类ID，不填查一级汇总数据
//            "needSum":false,                //类型：Boolean  必有字段  备注：false
//            "month":"mock"                //类型：String  必有字段  备注：202011
    private String goodTypeId;
    private boolean needSum;
    private String month;

    public String getGoodTypeId() {
        return goodTypeId == null ? "" : goodTypeId;
    }

    public void setGoodTypeId(String goodTypeId) {
        this.goodTypeId = goodTypeId;
    }

    public boolean isNeedSum() {
        return needSum;
    }

    public void setNeedSum(boolean needSum) {
        this.needSum = needSum;
    }

    public String getMonth() {
        return month == null ? "" : month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
