package com.xt.xiaotan.first.mvp;

import android.util.Log;

import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;

import java.util.List;

/**
 * Created by tiandi
 * on 2020/11/26
 */
public class EditFPesenter extends BasePresenter<EditFModel,EditFContract.View> {
    private EditFContract.View view;
    public EditFPesenter(EditFContract.View view){
        this.view = view;

    }


    @Override
    protected EditFModel createModel() {
        return new EditFModel();
    }

    public void getFurnitureList(String id,boolean isNeed){
        addDispose(createModel().furnitureList(id,isNeed), new BaseObserver<List<FurnitureListResult>>(view) {
            @Override
            public void onSuccess(BaseHttpResult<List<FurnitureListResult>> result) {
                view.getFurnitureList(result.getData());
                Log.e("tag","onSuccess"+result.getCode());



            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {
                Log.e("tag","onFailure"+errMsg);

            }

        });
    }
}
