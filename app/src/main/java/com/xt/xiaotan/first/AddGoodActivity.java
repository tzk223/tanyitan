package com.xt.xiaotan.first;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.xt.xiaotan.R;
import com.xt.xiaotan.add.AddActivity;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.databinding.AddActivityBinding;
import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.first.mvp.AddGoodContract;
import com.xt.xiaotan.first.mvp.AddGoodPesenter;

import java.util.List;

public class AddGoodActivity extends BaseMvpActivity<AddGoodPesenter> implements AddGoodContract.View {
    private AddActivityBinding binding;

    public static void gotoAddGoodActivity(Context context){
        Intent intent = new Intent(context,AddGoodActivity.class);
        context.startActivity(intent);

    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.add_activity);
        binding.setAddGoodActivity(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected AddGoodPesenter createPresenter() {

        return new AddGoodPesenter(this);
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected void getIntent(Intent intent) {

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public void getFurnitureList(List<FurnitureListResult> listResults) {

    }

    @Override
    public void showError(String msg) {

    }
}
