package com.xt.xiaotan.first.model;

import java.io.Serializable;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public class GoodsRequst implements Serializable {
//    current	必填	分页页码，从1开始
//    size	必填	每页数量
//    goodsName	选填	物品名称
//    goodsType	选填	一级分类id
//    goodsTypeSecond	选填	二级分类id
//    positionId	选填	房间id
//    furnitureId	选填	位置id
    public int current = 1;
    public int size = 5;
    public String goodsName;
    public String goodsType;
    public String goodsTypeSecond;
    public String positionId;
    public String furnitureId;

    public String title;

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getGoodsName() {
        return goodsName == null ? "" : goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }


    public String getPositionId() {
        return positionId == null ? "" : positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getFurnitureId() {
        return furnitureId == null ? "" : furnitureId;
    }

    public void setFurnitureId(String furnitureId) {
        this.furnitureId = furnitureId;
    }
}
