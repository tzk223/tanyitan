package com.xt.xiaotan.first.model;

import java.io.Serializable;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public class SaveFurnitureRequst implements Serializable {
//    {
//        "id":1,                //类型：Number  必有字段  备注：家具id，创建时不填
//            "positionId":1,                //类型：Number  必有字段  备注：房间id
//            "furnitureName":"我的家具呀～"                //类型：String  必有字段  备注：家具名称
//    }
    private String id;
    private String positionId;
    private String furnitureName;

    public String getId() {
        return id == null ? "" : id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPositionId() {
        return positionId == null ? "" : positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getFurnitureName() {
        return furnitureName == null ? "" : furnitureName;
    }

    public void setFurnitureName(String furnitureName) {
        this.furnitureName = furnitureName;
    }
}
