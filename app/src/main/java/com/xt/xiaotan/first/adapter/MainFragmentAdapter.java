package com.xt.xiaotan.first.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

public class MainFragmentAdapter extends FragmentPagerAdapter {
    private final FragmentManager fragmentManager;
    private List<Fragment> fragments;
    public MainFragmentAdapter (FragmentManager fragmentManager, List<Fragment> fragments){
        super(fragmentManager);
        this.fragmentManager = fragmentManager;
        this.fragments = fragments;
    };
    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
