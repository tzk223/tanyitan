package com.xt.xiaotan.first.typemvp;

import com.xt.xiaotan.first.model.GoodsClassResult;
import com.xt.xiaotan.first.model.StatisResult;
import com.xt.xiaotan.mvp.IView;

import java.util.List;

public interface StatisView extends IView {
    void getStatisResult(StatisResult s);

}
