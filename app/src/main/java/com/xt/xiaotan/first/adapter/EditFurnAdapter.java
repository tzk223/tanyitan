package com.xt.xiaotan.first.adapter;

import android.view.View;
import android.widget.RadioButton;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xt.xiaotan.R;
import com.xt.xiaotan.first.model.FurnitureListResult;

import java.util.List;

public class EditFurnAdapter extends BaseQuickAdapter<FurnitureListResult, BaseViewHolder> {
    private boolean showDelete;
    private List<FurnitureListResult> data;
    private OnItemClickListener onItemClickListener;
    public EditFurnAdapter(@Nullable List<FurnitureListResult> data) {
        super(R.layout.edit_furn_item, data);
        this.data = data;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setShowDelete(boolean showDelete) {
        this.showDelete = showDelete;
        notifyDataSetChanged();
    }

    @Override
    protected void convert(BaseViewHolder helper, FurnitureListResult item) {
        helper.setText(R.id.tv_f_name,item.getFurnitureName())
                .setText(R.id.tv_f_sum,item.getGoodsCount());
        RadioButton radioButton =  helper.itemView.findViewById(R.id.delete_rb);
        radioButton.setVisibility(showDelete? View.VISIBLE:View.GONE);
        radioButton.setChecked(item.isCheck);
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.isCheck = radioButton.isChecked();
                setDataNotCheck(item);
                notifyDataSetChanged();
                if(item.isCheck){
                    onItemClickListener.OnItemClick(item);
                }

            }
        });
    }

    public interface OnItemClickListener{
        void OnItemClick(FurnitureListResult item);
    }
    private void setDataNotCheck(FurnitureListResult furnitureListResult ){
        for(int i= 0;i<data.size();i++){
            FurnitureListResult furnitureListResult1 = data.get(i);
            if(furnitureListResult.isCheck&&!furnitureListResult.getId().equals(furnitureListResult1.getId())){
                furnitureListResult1.isCheck = false;
            }
        }
    }
}
