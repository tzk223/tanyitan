package com.xt.xiaotan.first.model;

import com.stx.xhb.androidx.entity.SimpleBannerInfo;

import java.io.Serializable;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public class PositionResult extends SimpleBannerInfo implements Serializable {

    /**
     * id : 4
     * positionName : 房间1
     * positionCode :
     * userId : 3
     * pictureCode : null
     * pictureUrl : http://kdtantan.oss-cn-beijing.aliyuncs.com/335*190-1.jpg
     * createDate : null
     * createName :
     * updateDate : null
     * updateName :
     * remarks :
     * version : null
     */

    private int id;
    private String positionName;
    private String positionCode;
    private int userId;
    private Object pictureCode;
    private String pictureUrl;
    private Object createDate;
    private String createName;
    private Object updateDate;
    private String updateName;
    private String remarks;
    private Object version;
    public boolean isCheck;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionCode() {
        return positionCode;
    }

    public void setPositionCode(String positionCode) {
        this.positionCode = positionCode;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Object getPictureCode() {
        return pictureCode;
    }

    public void setPictureCode(Object pictureCode) {
        this.pictureCode = pictureCode;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Object getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Object createDate) {
        this.createDate = createDate;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public Object getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Object updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateName() {
        return updateName;
    }

    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Object getVersion() {
        return version;
    }

    public void setVersion(Object version) {
        this.version = version;
    }

    @Override
    public Object getXBannerUrl() {
        return getPictureUrl();
    }
}
