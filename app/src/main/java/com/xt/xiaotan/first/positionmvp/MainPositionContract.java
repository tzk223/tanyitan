package com.xt.xiaotan.first.positionmvp;

import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.mvp.IView;
import com.xt.xiaotan.net.BaseHttpResult;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public interface MainPositionContract {
    public interface View extends IView {

        void getRoomList(List<PositionResult> string);
        void getFurnitureList(List<FurnitureListResult> listResults);

    }

    public interface Model extends IModel {

        Observable<BaseHttpResult<List<PositionResult>>> getRoomList();
        Observable<BaseHttpResult<List<FurnitureListResult>>> furnitureList(String id,boolean needGoodsCount);
    }
}
