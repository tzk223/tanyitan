package com.xt.xiaotan.first.typemvp;

import com.xt.xiaotan.first.model.StatisResult;
import com.xt.xiaotan.goods.bean.StatisticRequest;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.utils.RetrofitUtils;

import io.reactivex.Observable;

public class StatisPresenter extends BasePresenter {
    private StatisView statisView;
    @Override
    protected IModel createModel() {
        return null;
    }
    public StatisPresenter(StatisView statisView){
        this.statisView = statisView;
    }
    public void getStatis(String goodTypeId,String month) {
        StatisticRequest statisticRequest = new StatisticRequest();
        statisticRequest.setGoodTypeId(goodTypeId);
        statisticRequest.setMonth(month);
        addDispose(RetrofitUtils.getHttpService().statistics(statisticRequest), new BaseObserver<StatisResult>(statisView) {

            @Override
            public void onSuccess(BaseHttpResult<StatisResult> result) {
                statisView.getStatisResult(result.getData());
            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {
                statisView.showError(errMsg);

            }

        });
        ;
    }
}
