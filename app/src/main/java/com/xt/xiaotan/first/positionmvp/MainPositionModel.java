package com.xt.xiaotan.first.positionmvp;

import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.first.model.FurnitureRequst;
import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.mvp.BaseModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.utils.RetrofitUtils;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by tiandi
 * on 2020/11/10
 */
public class MainPositionModel extends BaseModel implements MainPositionContract.Model {


    @Override
    public Observable<BaseHttpResult<List<PositionResult>>> getRoomList() {
        return RetrofitUtils.getHttpService().getRoomList();
    }



    @Override
    public Observable<BaseHttpResult<List<FurnitureListResult>>> furnitureList(String id,boolean isNeed) {
        FurnitureRequst furnitureRequst = new FurnitureRequst();
        furnitureRequst.setPositionId(id);
        furnitureRequst.setNeedGoodsCount(isNeed);
        return RetrofitUtils.getHttpService().furnitureList(furnitureRequst);
    }


}
