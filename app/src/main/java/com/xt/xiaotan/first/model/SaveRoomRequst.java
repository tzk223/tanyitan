package com.xt.xiaotan.first.model;

import java.io.Serializable;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public class SaveRoomRequst implements Serializable {

    /**
     * positionName : 主卧
     * id : 1
     * pictureUrl : http://localhost:8088/blade/upload/image/20180423/1524466587187.jpg
     */

    private String positionName;
    private int id;
    private String pictureUrl;

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
