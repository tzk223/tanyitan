package com.xt.xiaotan.first.mvp;

import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.mvp.IView;
import com.xt.xiaotan.net.BaseHttpResult;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by tiandi
 * on 2020/11/26
 */
public interface AddGoodContract {
    interface View extends IView{
        void getFurnitureList(List<FurnitureListResult> listResults);

    }
    interface Model extends IModel{
        Observable<BaseHttpResult<List<FurnitureListResult>>> furnitureList(String id, boolean needGoodsCount);

    }
}
