package com.xt.xiaotan.first.mvp;

import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.mvp.IView;
import com.xt.xiaotan.net.BaseHttpResult;

import io.reactivex.Observable;

/**
 * Created by tiandi
 * on 2020/11/24
 */
public interface RoomDesContract {
    interface View extends IView{
        void getRoomDes(PositionResult positionResult);

    }
    interface Model extends IModel{
        Observable<BaseHttpResult<PositionResult>>  getRoomDes(String id);
    }
}
