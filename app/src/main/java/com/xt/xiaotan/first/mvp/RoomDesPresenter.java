package com.xt.xiaotan.first.mvp;

import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;

/**
 * Created by tiandi
 * on 2020/11/24
 */
public class RoomDesPresenter extends BasePresenter<RoomDesModel,RoomDesContract.View> {
    private RoomDesContract.View view;
    @Override
    protected RoomDesModel createModel() {
        return new RoomDesModel();
    }
    public RoomDesPresenter(RoomDesContract.View view){
        this.view = view;

    }

    public void getRoomDes(String id){
        addDispose(getModel().getRoomDes(id), new BaseObserver<PositionResult>(view) {
            @Override
            public void onSuccess(BaseHttpResult<PositionResult> result) {
                view.getRoomDes(result.getData());

            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {

            }

        });
    }
}
