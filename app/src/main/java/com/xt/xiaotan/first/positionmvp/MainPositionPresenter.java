package com.xt.xiaotan.first.positionmvp;

import android.util.Log;

import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;

import java.security.PublicKey;
import java.util.List;

/**
 * Created by tiandi
 * on 2020/11/10
 */
public class MainPositionPresenter extends BasePresenter {
    public MainPositionContract.View view;
    public MainPositionModel mainModel;
    @Override
    protected IModel createModel() {
        mainModel = new MainPositionModel();
        return mainModel;
    }
    public MainPositionPresenter(MainPositionContract.View view){
        this.view = view;
    }





    public void getRoomList(){
        addDispose(mainModel.getRoomList(), new BaseObserver<List<PositionResult>>(view) {
            @Override
            public void onSuccess(BaseHttpResult<List<PositionResult>> result) {
                view.getRoomList(result.getData());
                Log.e("tag","onSuccess"+result.getCode());



            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {
                Log.e("tag","onFailure"+errMsg);

            }

        });

    }

    public void getFurnitureList(String id,boolean isNeed){
        addDispose(mainModel.furnitureList(id,isNeed), new BaseObserver<List<FurnitureListResult>>(view) {
            @Override
            public void onSuccess(BaseHttpResult<List<FurnitureListResult>> result) {
                view.getFurnitureList(result.getData());
                Log.e("tag","onSuccess"+result.getCode());



            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {
                Log.e("tag","onFailure"+errMsg);

            }

        });
    }


}
