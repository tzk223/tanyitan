package com.xt.xiaotan.first.mvp;

import com.xt.xiaotan.first.model.CountListRequst;
import com.xt.xiaotan.first.model.FirstResult;

import com.xt.xiaotan.first.model.GoodsClassRequst;

import com.xt.xiaotan.mvp.BaseModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.utils.RetrofitUtils;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by tiandi
 * on 2020/11/10
 */
public class MainModel extends BaseModel implements MainContract.Model {
    @Override
    public Observable<BaseHttpResult<List<FirstResult>>> getBannerList() {

        return RetrofitUtils.getHttpService().getBannerList();
    }



    @Override
    public Observable<BaseHttpResult<String>> getCountList() {

        return RetrofitUtils.getHttpService().getCountList(new CountListRequst());
    }


}
