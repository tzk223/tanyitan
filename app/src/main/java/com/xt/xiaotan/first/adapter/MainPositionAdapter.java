package com.xt.xiaotan.first.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xt.xiaotan.R;
import com.xt.xiaotan.first.model.FurnitureListResult;
import com.xt.xiaotan.first.model.PositionResult;

import java.util.List;

public class MainPositionAdapter extends BaseQuickAdapter<FurnitureListResult, BaseViewHolder> {

   private    OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public MainPositionAdapter(@Nullable List<FurnitureListResult> data) {
        super(R.layout.main_position_type_item, data);
    }
    @Override
    protected void convert(BaseViewHolder helper, FurnitureListResult item) {
        helper.setText(R.id.tv_name,item.getFurnitureName())
                .setText(R.id.tv_sum,item.getGoodsCount());
        RelativeLayout content_rl = helper.itemView.findViewById(R.id.content_rl);
        content_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(item);
            }
        });

    }

    public interface OnItemClickListener{
       void onItemClick(FurnitureListResult item);
    }



}
