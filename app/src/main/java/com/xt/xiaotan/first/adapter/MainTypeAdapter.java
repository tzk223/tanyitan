package com.xt.xiaotan.first.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.xt.xiaotan.R;
import com.xt.xiaotan.first.MainActivity;
import com.xt.xiaotan.first.model.GoodsClassResult;


import java.util.List;

public class MainTypeAdapter extends RecyclerView.Adapter<MainTypeAdapter.ViewHolder> {
    public Context context;

    private List<GoodsClassResult> list;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private OnItemClickListener onItemClickListener;
    public MainTypeAdapter(Context context){
           this.context = context;
    }

    public void setData(List<GoodsClassResult> list){
        this.list = list;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public MainTypeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.main_type_item, parent, false);
        return new ViewHolder(view);
    }

    public interface OnItemClickListener{
        void onItemClick(GoodsClassResult goodsClassResult);
    }

    @Override
    public void onBindViewHolder(@NonNull MainTypeAdapter.ViewHolder holder, int position) {
       GoodsClassResult goodsClassResult = list.get(position);
        holder.type_goodName.setText(goodsClassResult.getTypeName());
        Log.e("tag","url = "+goodsClassResult.getTypeIco());
        Glide.with(context).load(goodsClassResult.getTypeIco()).into(holder.type_img);
        holder.tv_good_sum.setText(goodsClassResult.getGoodsCount()+"");
        holder.type_cardview.setCardBackgroundColor(Color.parseColor(goodsClassResult.getColorStart()));
        holder.type_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(goodsClassResult);
            }
        });
    }

    @Override
    public int getItemCount() {
        return null == list?0:list.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView type_img;
        TextView type_goodName;
        TextView tv_good_sum;
        LinearLayout type_ll;
        CardView type_cardview;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            type_img = itemView.findViewById(R.id.type_img);
            type_goodName = itemView.findViewById(R.id.type_goodname);
            tv_good_sum = itemView.findViewById(R.id.tv_good_sum);
            type_ll = itemView.findViewById(R.id.type_ll);
            type_cardview = itemView.findViewById(R.id.type_cardview);

        }
    }
}
