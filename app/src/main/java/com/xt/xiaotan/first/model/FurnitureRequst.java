package com.xt.xiaotan.first.model;

import java.io.Serializable;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public class FurnitureRequst implements Serializable {
//      "positionId":1,                //类型：Number  必有字段  备注：房间id
//              "needGoodsCount":true                //类型：Boolean  必有字段  备注：是否获取物品统计
    private String positionId;
    private boolean needGoodsCount;

    public String getPositionId() {
        return positionId == null ? "" : positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public boolean isNeedGoodsCount() {
        return needGoodsCount;
    }

    public void setNeedGoodsCount(boolean needGoodsCount) {
        this.needGoodsCount = needGoodsCount;
    }
}
