package com.xt.xiaotan.first.typemvp;

import com.xt.xiaotan.first.model.GoodsClassRequst;

import com.xt.xiaotan.first.model.GoodsClassResult;
import com.xt.xiaotan.mvp.BaseModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.utils.RetrofitUtils;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by tiandi
 * on 2020/11/10
 */
public class MainTypeModel extends BaseModel implements MainTypeContract.Model {


    @Override
    public Observable<BaseHttpResult<List<GoodsClassResult>>> getGoodClassList() {
        return RetrofitUtils.getHttpService().getGoodList(new GoodsClassRequst());
    }


}
