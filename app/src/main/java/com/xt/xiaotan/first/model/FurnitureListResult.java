package com.xt.xiaotan.first.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public class FurnitureListResult implements Serializable {

//        {                //类型：Object  必有字段  备注：无
//            "id":2,                //类型：Number  必有字段  备注：家具id
//                "furnitureName":"茶机",                //类型：String  必有字段  备注：家具名称
//                "positionId":1,                //类型：Number  必有字段  备注：房间id
//                "goodsCount":0                //类型：Number  必有字段  备注：家具内物品数量
//        }

        private String id;
        private String furnitureName;
        private String positionId;
        private String goodsCount;

        public boolean isCheck;
        public String getId() {
            return id == null ? "" : id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFurnitureName() {
            return furnitureName == null ? "" : furnitureName;
        }

        public void setFurnitureName(String furnitureName) {
            this.furnitureName = furnitureName;
        }

        public String getPositionId() {
            return positionId == null ? "" : positionId;
        }

        public void setPositionId(String positionId) {
            this.positionId = positionId;
        }

        public String getGoodsCount() {
            return goodsCount == null ? "" : goodsCount;
        }

        public void setGoodsCount(String goodsCount) {
            this.goodsCount = goodsCount;
        }

}
