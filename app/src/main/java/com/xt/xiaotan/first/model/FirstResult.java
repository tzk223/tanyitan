package com.xt.xiaotan.first.model;

import com.stx.xhb.androidx.entity.SimpleBannerInfo;

import java.io.Serializable;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public class FirstResult extends SimpleBannerInfo implements Serializable {
    /**
     * id : 1
     * advertName : 测试名称12
     * advertCode : null
     * advertPhoto : http://kdtantan.oss-cn-beijing.aliyuncs.com/23834345*140-1.jpg
     * createTime : null
     * sortNum : 1
     * remark : null
     */

    private int id;
    private String advertName;
    private String advertCode;
    private String advertPhoto;
    private String createTime;
    private int sortNum;
    private String remark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAdvertName() {
        return advertName;
    }

    public void setAdvertName(String advertName) {
        this.advertName = advertName;
    }

    public Object getAdvertCode() {
        return advertCode;
    }


    public String getAdvertPhoto() {
        return advertPhoto;
    }

    public void setAdvertPhoto(String advertPhoto) {
        this.advertPhoto = advertPhoto;
    }

    public Object getCreateTime() {
        return createTime;
    }



    public int getSortNum() {
        return sortNum;
    }

    public void setSortNum(int sortNum) {
        this.sortNum = sortNum;
    }

    public Object getRemark() {
        return remark;
    }

    @Override
    public Object getXBannerUrl() {
        return getAdvertPhoto();
    }
}
