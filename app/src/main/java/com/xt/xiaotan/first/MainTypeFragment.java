package com.xt.xiaotan.first;

import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseFragment;
import com.xt.xiaotan.first.adapter.MainTypeAdapter;

import com.xt.xiaotan.first.model.GoodsClassResult;
import com.xt.xiaotan.first.model.GoodsRequst;
import com.xt.xiaotan.first.typemvp.MainTypeContract;

import com.xt.xiaotan.first.typemvp.MainTypePresenter;
import com.xt.xiaotan.goods.GoodCanEditActivity;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.view.SpaceItemDecoration;

import java.util.List;

public class MainTypeFragment extends BaseFragment implements MainTypeContract.View {
    private RecyclerView rv;
    private MainTypePresenter mainPresenter;
    MainTypeAdapter mainTypeAdapter;
    public static Fragment getMainTypeFragment(){
        return new MainTypeFragment();
    }
    @Override
    protected int getLayoutId() {
        return R.layout.mian_type_fragment;
    }

    @Override
    protected BasePresenter createPresenter() {
        mainPresenter = new MainTypePresenter(this);
        return null;
    }

    @Override
    protected void initView() {
           rv = mRootView.findViewById(R.id.rv);

    }

    @Override
    public void onResume() {
        super.onResume();
        mainPresenter.getGoodClassList();
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {
        mainPresenter.getGoodClassList();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),2);
        rv.setLayoutManager(gridLayoutManager);
        rv.addItemDecoration(new SpaceItemDecoration(2,10));
         mainTypeAdapter = new MainTypeAdapter(getActivity());
        rv.setAdapter(mainTypeAdapter);

        mainTypeAdapter.setOnItemClickListener(new MainTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(GoodsClassResult goodsClassResult) {
                GoodsRequst goodsRequst = new GoodsRequst();
                goodsRequst.size = 10;
                goodsRequst.current = 1;
                goodsRequst.title = goodsClassResult.getTypeName();
                goodsRequst.goodsType = goodsClassResult.getId()+"";
                GoodCanEditActivity.gotoGoodCanEditActivity(getActivity(),goodsRequst);
            }
        });
    }

    @Override
    protected boolean useEventBus() {
        return false;
    }

    @Override
    public void showError(String msg) {

    }

    @Override
    public void getGoodsList(List<GoodsClassResult> goodsClassResults) {
        Log.d("MainTypeFragment",goodsClassResults.toString());
        mainTypeAdapter.setData(goodsClassResults);

    }
}
