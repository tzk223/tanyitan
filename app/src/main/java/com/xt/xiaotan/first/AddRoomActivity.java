package com.xt.xiaotan.first;

import android.content.Context;
import android.content.Intent;

import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.mvp.BasePresenter;

public class AddRoomActivity extends BaseMvpActivity {

    public static void gotoAddRoomActivity(Context context){
        Intent intent = new Intent(context,AddRoomActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected void getIntent(Intent intent) {

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public void showError(String msg) {

    }
}
