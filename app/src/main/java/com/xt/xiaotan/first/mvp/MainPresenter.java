package com.xt.xiaotan.first.mvp;

import android.util.Log;

import com.xt.xiaotan.first.model.FirstResult;

import com.xt.xiaotan.login.mvp.LoginContract;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;

import java.util.List;

/**
 * Created by tiandi
 * on 2020/11/10
 */
public class MainPresenter extends BasePresenter {
    private MainContract.View view;
    private MainModel mainModel;
    @Override
    protected IModel createModel() {
        mainModel = new MainModel();
        return mainModel;
    }
    public MainPresenter(MainContract.View view){
        this.view = view;
    }



    public void getBannerList(){
        addDispose(mainModel.getBannerList(), new BaseObserver<List<FirstResult>>(view) {
            @Override
            public void onSuccess(BaseHttpResult<List<FirstResult>> result) {
                view.getBannerList(result.getData());

            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {

            }

        });

    }




}
