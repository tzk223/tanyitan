package com.xt.xiaotan.first.typemvp;

import com.xt.xiaotan.first.model.GoodsClassResult;

import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.mvp.IView;
import com.xt.xiaotan.net.BaseHttpResult;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public interface MainTypeContract {
     interface View extends IView {

        void getGoodsList(List<GoodsClassResult> goodsClassResults);

    }

     interface Model extends IModel {

        Observable<BaseHttpResult<List<GoodsClassResult>>> getGoodClassList();
    }
}
