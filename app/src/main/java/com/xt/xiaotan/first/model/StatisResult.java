package com.xt.xiaotan.first.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;


import com.xt.xiaotan.BR;

import java.io.Serializable;
import java.util.List;

public class StatisResult extends BaseObservable implements Serializable {


    /**
     * goodTypes : null
     * dailyCount : []
     * payCategory : []
     * goodsPay : []
     * goodsNumber : []
     * typeCount : 0
     * goodsCount : 0
     * priceSum : 0.0
     */

    private Object goodTypes;
    private String typeCount;
    private String goodsCount;
    private String priceSum;
    private List<?> dailyCount;
    private List<?> payCategory;
    private List<?> goodsPay;
    private List<?> goodsNumber;

    public Object getGoodTypes() {
        return goodTypes;
    }

    public void setGoodTypes(Object goodTypes) {
        this.goodTypes = goodTypes;
    }
    @Bindable
    public String getTypeCount() {
        return typeCount;
    }

    public void setTypeCount(String typeCount) {
        this.typeCount = typeCount;
        notifyPropertyChanged(BR.typeCount); // 需要手动刷新
    }
    @Bindable
    public String getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(String goodsCount) {
        this.goodsCount = goodsCount;
        notifyPropertyChanged(BR.goodsCount); // 需要手动刷新
    }
    @Bindable
    public String getPriceSum() {
        return priceSum;
    }

    public void setPriceSum(String priceSum) {
        this.priceSum = priceSum;
        notifyPropertyChanged(BR.priceSum); // 需要手动刷新
    }

    public List<?> getDailyCount() {
        return dailyCount;
    }

    public void setDailyCount(List<?> dailyCount) {
        this.dailyCount = dailyCount;
    }

    public List<?> getPayCategory() {
        return payCategory;
    }

    public void setPayCategory(List<?> payCategory) {
        this.payCategory = payCategory;
    }

    public List<?> getGoodsPay() {
        return goodsPay;
    }

    public void setGoodsPay(List<?> goodsPay) {
        this.goodsPay = goodsPay;
    }

    public List<?> getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(List<?> goodsNumber) {
        this.goodsNumber = goodsNumber;
    }
}
