package com.xt.xiaotan.first;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.gyf.immersionbar.ImmersionBar;
import com.leaf.library.StatusBarUtil;
import com.stx.xhb.androidx.XBanner;
import com.stx.xhb.androidx.entity.SimpleBannerInfo;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.first.adapter.MainFragmentAdapter;
import com.xt.xiaotan.first.model.FirstResult;


import com.xt.xiaotan.first.mvp.MainContract;
import com.xt.xiaotan.first.mvp.MainPresenter;
import com.xt.xiaotan.first.positionmvp.MainPositionPresenter;
import com.xt.xiaotan.goods.AddGoodsActivity;
import com.xt.xiaotan.login.AddUserInfoActivity;
import com.xt.xiaotan.login.LoginActivity;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.search.SearchActivity;
import com.xt.xiaotan.set.InfoModel;
import com.xt.xiaotan.set.MySetActivity;
import com.xt.xiaotan.utils.DisplayUtils;
import com.xt.xiaotan.utils.RetrofitUtils;
import com.xt.xiaotan.utils.ToastUtils;
import com.xt.xiaotan.view.MyViewPager;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends BaseMvpActivity implements View.OnClickListener , MainContract.View {
    private MainPresenter mainPresenter;
    private MyViewPager viewpager;

    private CardView card_view;
    private XBanner banner;
    private List<FirstResult> list = new ArrayList<>();
    private CheckedTextView type_tv,position_tv;
    private NavigationView navigationView;
    private TextView nviSet;
    private DrawerLayout dl;
    private CircleImageView user_iv;
    private LinearLayout search_ll;
    private LinearLayout banner_sign;
    private TextView add_goods_tv;
    private TextView statistics_tv;
    private TextView user_name;
    private CircleImageView imageView;
    private TextView head_name_tv;
    private ImageView position_iv;

    private List<View> views = new ArrayList<>();

    @Override
    protected void setStatusBar() {
        ImmersionBar.with(this).statusBarDarkFont(true).statusBarColor(R.color.white).init();
    }

    private List<Fragment> fragments = new ArrayList<>();
    @Override
    protected BasePresenter createPresenter() {
        mainPresenter = new MainPresenter(this);
        return mainPresenter;
    }

    public static void gotoMainactivity(Context context){
        Intent intent = new Intent(context,MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.main_activity;
    }

    @Override
    protected void getIntent(Intent intent) {

    }

    @Override
    public void getBannerList(List<FirstResult> firstResults) {
        list.clear();
        list.addAll(firstResults);
        banner.setAutoPlayAble(list.size() > 1);
        banner.setBannerData(R.layout.banner_item,list);
        banner.loadImage(new XBanner.XBannerAdapter() {
            @Override
            public void loadBanner(XBanner banner, Object model, View view, int position) {
                ImageView imageView = view.findViewById(R.id.banner_iv);
                FirstResult bannerStrs = (FirstResult) model;
                Log.e("tag","bannerStrs = "+bannerStrs.getAdvertPhoto());
                Glide.with(MainActivity.this).load(bannerStrs.getAdvertPhoto()).into(imageView);
            }
        });
        setBannerSign();
    }


    @Override
    public void getCountList(String firstResults) {

    }




    @Override
    protected void initView() {
        setContentView(R.layout.main_activity);
        viewpager = findViewById(R.id.viewpager);
        banner = findViewById(R.id.banner);
        type_tv = findViewById(R.id.type_tv);
        position_tv = findViewById(R.id.position_tv);
        position_iv = findViewById(R.id.position_iv);
        navigationView = findViewById(R.id.navigationView);
        dl = findViewById(R.id.dl);
        user_iv = findViewById(R.id.user_iv);
        search_ll = findViewById(R.id.search_ll);
        banner_sign = findViewById(R.id.banner_sign);
        add_goods_tv = findViewById(R.id.add_goods_tv);
        statistics_tv = findViewById(R.id.statistics_tv);
        user_name = findViewById(R.id.user_name);
        View headerLayout = navigationView.inflateHeaderView(R.layout.home_nav_header);
        imageView = headerLayout.findViewById(R.id.imageView);
        head_name_tv = headerLayout.findViewById(R.id.head_name_tv);
        getUserInfo();
    }

    private void getUserInfo(){
        RetrofitUtils.getHttpService().getInfo().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.<BaseHttpResult<InfoModel>>bindUntilEvent(ActivityEvent.STOP))
                .subscribe(new BaseObserver<InfoModel>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<InfoModel> result) {
                        if(result!=null&&result.getData()!=null){
                             Glide.with(MainActivity.this).load(result.getData().getPhotoUrl()).into(user_iv);
                             Glide.with(MainActivity.this).load(result.getData().getPhotoUrl()).into(imageView);
                             head_name_tv.setText("您好  "+result.getData().getUserName());
                             user_name.setText("您好  "+result.getData().getUserName());

                        }
                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {
                        showError(errMsg);

                    }
                });
    }
    @Override
    protected void initListener() {
        type_tv.setOnClickListener(this);
        position_tv.setOnClickListener(this);
        user_iv.setOnClickListener(this);
        search_ll.setOnClickListener(this);
        add_goods_tv.setOnClickListener(this);
        statistics_tv.setOnClickListener(this);
        user_name.setOnClickListener(this);
        position_iv.setOnClickListener(this);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return false;
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if(item.getItemId() == R.id.nav_set){
                    AddUserInfoActivity.gotoAddUserInfoActivity(MainActivity.this);
                   // MySetActivity.gotoSetActivity(MainActivity.this);
                }  if(item.getItemId()== R.id.nav_aboat){
                   // AddUserInfoActivity.gotoAddUserInfoActivity(MainActivity.this);
                }

                return true;
            }
        });
        viewPagerChange();

    }

    @Override
    protected void initData() {
        mainPresenter.getBannerList();
        type_tv.setChecked(true);
        fragments.add(MainTypeFragment.getMainTypeFragment());
        fragments.add(MainPositionFragment.getMainPositionFragment());
        MainFragmentAdapter adapter = new MainFragmentAdapter(getSupportFragmentManager(),fragments);
        viewpager.setAdapter(adapter);

        banner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onPageSelected(int position) {
                for(int i=0;i<views.size();i++){
                    if(position == i){
                        views.get(i).setBackground(getResources().getDrawable(R.drawable.bg_banner_check_sign_fff36612,null));
                    }else {
                        views.get(i).setBackground(getResources().getDrawable(R.drawable.bg_banner_not_check_ffdcdcdc,null));
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
    private void setBannerSign(){
        banner_sign.removeAllViews();
        LayoutInflater layoutInflater = getLayoutInflater();
        if(list!=null&&list.size()>0){
            int width = DisplayUtils.dip2px(this,20);
            int height = DisplayUtils.dip2px(this,3);
            for(int i=0;i<list.size();i++){
                View view = layoutInflater.inflate(R.layout.banner_sign_item,null);
                LinearLayout banner_sign_iv = view.findViewById(R.id.banner_sign_iv);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, height);
                layoutParams.leftMargin = 5;
                layoutParams.rightMargin = 5;
                views.add(banner_sign_iv);
                if(i==0){
                    view.setBackground(getResources().getDrawable(R.drawable.bg_banner_check_sign_fff36612,null));
                }else{
                    view.setBackground(getResources().getDrawable(R.drawable.bg_banner_not_check_ffdcdcdc,null));
                }
                banner_sign.addView(view,layoutParams);
            }

        }
    }
    @Override
    public void showError(String msg) {

    }

    private void viewPagerChange(){
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                     if(position == 0){
                         if(!type_tv.isChecked()){
                             type_tv.setChecked(true);
                             position_tv.setChecked(false);
                         }
                     }else if(position == 1){
                         if(!position_tv.isChecked()){
                             type_tv.setChecked(false);
                             position_tv.setChecked(true);
                         }
                     }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.type_tv){
           if(!type_tv.isChecked()){
               type_tv.setChecked(true);
               position_tv.setChecked(false);
               viewpager.setCurrentItem(0);
           }
        }else if(v.getId() == R.id.position_tv){
            if(!position_tv.isChecked()){
                type_tv.setChecked(false);
                position_tv.setChecked(true);
                viewpager.setCurrentItem(1);
            }
        }else if(v.getId() == R.id.user_iv){
           dl.openDrawer(navigationView);
//            Intent intent = new Intent(this, LoginActivity.class);
//            startActivity(intent);
        }else if(v.getId() == R.id.search_ll){
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
        }else if(v.getId() == R.id.add_goods_tv){
            AddGoodsActivity.gotoAddGoodsActivity(this);
        }else if(v.getId() == R.id.nav_set){
            Intent intent = new Intent(this, MySetActivity.class);
            startActivity(intent);
        }else if(v.getId() == R.id.statistics_tv){
            StatisticsActivity.gotoStatisticsActivity(this);
        }else if(v.getId() == R.id.position_iv){
            RoomDesActivity.gotoRoomDesActivity(this,null);
        }else if(v.getId() == R.id.user_name){
            dl.openDrawer(navigationView);
        }
    }


}
