package com.xt.xiaotan.first;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.callback.OSSCompletedCallback;
import com.alibaba.sdk.android.oss.callback.OSSProgressCallback;
import com.alibaba.sdk.android.oss.internal.OSSAsyncTask;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.bumptech.glide.Glide;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.xt.xiaotan.R;
import com.xt.xiaotan.app.AppConfig;
import com.xt.xiaotan.base.BaseActivity;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.databinding.RoomdesActivityBinding;
import com.xt.xiaotan.first.model.PositionResult;
import com.xt.xiaotan.first.mvp.RoomDesContract;
import com.xt.xiaotan.first.mvp.RoomDesPresenter;
import com.xt.xiaotan.goods.bean.PositionRequest;
import com.xt.xiaotan.login.model.OssBean;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.oss.OSSService;
import com.xt.xiaotan.utils.RetrofitUtils;
import com.xt.xiaotan.utils.ToastUtils;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by tiandi
 * on 2020/11/24
 */
public class RoomDesActivity extends BaseMvpActivity<RoomDesPresenter> implements RoomDesContract.View {
    private String id;
    private RoomdesActivityBinding binding;

    private OssBean ossBean;

    private String imageUrl;

    private String fileUrl;

    private ImageView carmerIv;

    private ImageView iv_position;

    public static void gotoRoomDesActivity(Context context,String id){
        Intent intent = new Intent(context,RoomDesActivity.class);
        intent.putExtra("id",id);
        context.startActivity(intent);
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this,R.layout.roomdes_activity);
        binding.setRoomDesActivity(this);
        super.onCreate(savedInstanceState);

    }
    @Override
    protected int getLayoutId() {
        return R.layout.roomdes_activity;
    }
    @Override
    protected RoomDesPresenter createPresenter() {
        return new RoomDesPresenter(this);
    }


    @Override
    protected void getIntent(Intent intent) {
        id = intent.getStringExtra("id");
        if(!TextUtils.isEmpty(id)){
            mPresenter.getRoomDes(id);
        }


    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {
        carmerIv = findViewById(R.id.carmer_iv);
        iv_position = findViewById(R.id.iv_position);
        binding.checkPictureCv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlbum();
            }
        });
        binding.addRoomBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PositionRequest saveRoomRequst = new PositionRequest();
                if(TextUtils.isEmpty(imageUrl)){
                    ToastUtils.show("您还没有选择位置图片",1000);
                    return;
                }
                if(TextUtils.isEmpty(binding.positionNameEt.getText().toString())){
                    ToastUtils.show("您还没有填写位置名称",1000);
                    return;
                }
                if(imageUrl.contains(AppConfig.PICTURE_URL)){
                    saveRoomRequst.pictureUrl = imageUrl;
                }else{
                    saveRoomRequst.pictureUrl = AppConfig.PICTURE_URL+imageUrl;
                }

                saveRoomRequst.positionName = binding.positionNameEt.getText().toString();
                savaPosition(saveRoomRequst);
            }
        });
    }
    private void savaPosition(PositionRequest saveRoomRequst){
        RetrofitUtils.getHttpService().saveRoom(saveRoomRequst).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<String>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<String> result) {
                        Log.d("tag","是否成功");
                        ToastUtils.show("上传房间数据成功",1000);
                        finish();
                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {
                        ToastUtils.show("上传房间数据失败",1000);
                    }
                });
    }
    @Override
    public void showLoading() {

    }
    private void showAlbum() {
        //参数很多，根据需要添加
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .isCamera(true)
                .maxSelectNum(1)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .imageSpanCount(4)// 每行显示个数
                .selectionMode(PictureConfig.MULTIPLE)// 多选 or 单选PictureConfig.MULTIPLE : PictureConfig.SINGLE
                .previewImage(true)// 是否可预览图片
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                //.sizeMultiplier(0.5f)// glide 加载图片大小 0~1之间 如设置 .glideOverride()无效
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .withAspectRatio(1, 1)// 裁剪比例 如16:9 3:2 3:4 1:1 可自定义
                //.selectionMedia(selectList)// 是否传入已选图片//   .previewEggs()// 预览图片时 是否增强左右滑动图片体验(图片滑动一半即可看到上一张是否选中)
                //.cropCompressQuality(90)// 裁剪压缩质量 默认100
                //.compressMaxKB()//压缩最大值kb compressGrade()为Luban.CUSTOM_GEAR有效
                //.compressWH() // 压缩宽高比 compressGrade()为Luban.CUSTOM_GEAR有效
                //.cropWH()// 裁剪宽高比，设置如果大于图片本身宽高则无效
                .rotateEnabled(false) // 裁剪是否可旋转图片
                //.scaleEnabled()// 裁剪是否可放大缩小图片
                //.recordVideoSecond()//录制视频秒数 默认60s
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PictureConfig.CHOOSE_REQUEST && data != null) {
            Log.d("tag", data.toString());
            List<LocalMedia> localMedia = PictureSelector.obtainMultipleResult(data);

            if(localMedia!=null&&localMedia.size()>0){
                fileUrl = localMedia.get(0).getCompressPath();
                carmerIv.setVisibility(View.GONE);
                Glide.with(RoomDesActivity.this).load(fileUrl).into(iv_position);
                getOss();
            }
        }
    }

    private void getOss(){
        RetrofitUtils.getHttpService().oss().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<OssBean>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<OssBean> result) {
                        ossBean = result.getData();
                        uploadImage(fileUrl);
                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {
                    }
                });
    }
    private void uploadImage(String imageFilePath){
        imageUrl = "RoomDescActivity"+System.currentTimeMillis();
        // 构造上传请求
        PutObjectRequest put = new PutObjectRequest("kdtantan", imageUrl, imageFilePath);

// 异步上传时可以设置进度回调
        put.setProgressCallback(new OSSProgressCallback<PutObjectRequest>() {
            @Override
            public void onProgress(PutObjectRequest request, long currentSize, long totalSize) {
                Log.d("PutObject", "currentSize: " + currentSize + " totalSize: " + totalSize);
            }
        });

        OSSAsyncTask task = OSSService.getOss(ossBean).asyncPutObject(put, new OSSCompletedCallback<PutObjectRequest, PutObjectResult>() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                Log.d("PutObject", "UploadSuccess");
                imageUrl = AppConfig.PICTURE_URL+imageUrl;

            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientExcepion, ServiceException serviceException) {
                // 请求异常
                if (clientExcepion != null) {
                    // 本地异常如网络异常等
                    clientExcepion.printStackTrace();
                }
                if (serviceException != null) {
                    // 服务异常
                    Log.e("ErrorCode", serviceException.getErrorCode());
                    Log.e("RequestId", serviceException.getRequestId());
                    Log.e("HostId", serviceException.getHostId());
                    Log.e("RawMessage", serviceException.getRawMessage());
                }
            }
        });
        task.waitUntilFinished();
    }
    @Override
    public void hideLoading() {

    }


    @Override
    public void showError(String msg) {

    }

    @Override
    public void getRoomDes(PositionResult positionResult) {
        binding.setPositionResult(positionResult);
        binding.carmerIv.setVisibility(View.GONE);
        Glide.with(this).load(positionResult.getPictureUrl()).into(binding.ivPosition);
    }
    public void onClickEdfurniture(){
        EditFurnitureActivity.gotoEditFurnActivity(this,id);
    }

    public void onDeleteRoom(){
        RetrofitUtils.getHttpService().deleteRoom(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.<BaseHttpResult<String>>bindUntilEvent(ActivityEvent.STOP))
                .subscribe(new BaseObserver<String>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<String> result) {
                        ToastUtils.show("删除位置成功",1000);
                        finish();
                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {
                        showError(errMsg);

                    }
                });
    }

    public void onFinish(){
        finish();
    }
}
