package com.xt.xiaotan.first.model;

import java.io.Serializable;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public class GoodsClassRequst implements Serializable {
//    {
//        "parentId":0,                //类型：Number  必有字段  备注：一级类目填0，二级类目填一级id
//            "needGoodsCount":true                //类型：Boolean  必有字段  备注：是否获取物品统计
//    }

    private int parentId = 0;
    private boolean needGoodsCount = true;

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public boolean isNeedGoodsCount() {
        return needGoodsCount;
    }

    public void setNeedGoodsCount(boolean needGoodsCount) {
        this.needGoodsCount = needGoodsCount;
    }
}
