package com.xt.xiaotan.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.sdk.android.oss.ClientConfiguration;
import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.OSS;
import com.alibaba.sdk.android.oss.OSSClient;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.callback.OSSCompletedCallback;
import com.alibaba.sdk.android.oss.callback.OSSProgressCallback;
import com.alibaba.sdk.android.oss.common.OSSLog;
import com.alibaba.sdk.android.oss.common.auth.OSSCredentialProvider;
import com.alibaba.sdk.android.oss.common.auth.OSSStsTokenCredentialProvider;
import com.alibaba.sdk.android.oss.internal.OSSAsyncTask;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.bigkoo.pickerview.OptionsPickerView;
import com.bigkoo.pickerview.TimePickerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gyf.immersionbar.ImmersionBar;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.databinding.AddUserinfoActivityBinding;
import com.xt.xiaotan.first.MainActivity;
import com.xt.xiaotan.first.adapter.LoveUserAdapter;
import com.xt.xiaotan.login.model.OssBean;
import com.xt.xiaotan.login.mvp.LoginContract;
import com.xt.xiaotan.login.mvp.LoginPresenter;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.oss.OSSService;
import com.xt.xiaotan.set.InfoModel;
import com.xt.xiaotan.set.SaveInfoRequest;
import com.xt.xiaotan.utils.Logs;
import com.xt.xiaotan.utils.RetrofitUtils;
import com.xt.xiaotan.utils.ToastUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


/**
 * 添加个人信息
 */
public class AddUserInfoActivity extends BaseMvpActivity implements LoginContract.View{
   public AddUserinfoActivityBinding addUserinfoActivityBinding;
    private CompositeDisposable compositeDisposable;
   private LoveUserAdapter loveUserAdapter;
    private LoginPresenter loginPresenter;
    private OssBean ossBean;
    private List<String> sexList;
    private InfoModel infoModel;
    private List<InfoModel.SurveysBean> surveys = new ArrayList<>();
    public static void gotoAddUserInfoActivity(Context context) {
        Intent intent = new Intent(context, AddUserInfoActivity.class);
        context.startActivity(intent);
    }



    @Override
    protected BasePresenter createPresenter() {
        loginPresenter = new LoginPresenter(this);
        return loginPresenter;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        addUserinfoActivityBinding = DataBindingUtil.setContentView(this,R.layout.add_userinfo_activity);

        sexList = new ArrayList<>();
        for(int i=0;i<2;i++){
            if(i==0){
                sexList.add("男");
            }else if(i==1){
                sexList.add("女");
            }
        }
        super.onCreate(savedInstanceState);
        loginPresenter.oss();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.add_userinfo_activity;
    }

    @Override
    protected void getIntent(Intent intent) {

    }

    @Override
    protected void initView() {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this,3);
        loveUserAdapter = new LoveUserAdapter(surveys);
        addUserinfoActivityBinding.loveShowRv.setLayoutManager(layoutManager);
        addUserinfoActivityBinding.loveShowRv.setAdapter(loveUserAdapter);
        loveUserAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                List<InfoModel.SurveysBean> surveysBeanLists = (List<InfoModel.SurveysBean>) adapter.getData();
                InfoModel.SurveysBean surveysBean = surveysBeanLists.get(position);
                surveysBean.setIsSelected(!surveysBean.isIsSelected());
                loveUserAdapter.notifyDataSetChanged();


            }
        });

    }

    @Override
    protected void initListener() {

        addUserinfoActivityBinding.backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        addUserinfoActivityBinding.checkImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlbum();
            }
        });
        addUserinfoActivityBinding.checkBirthDayLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(infoModel!=null&&!TextUtils.isEmpty(infoModel.getBirthday())){
                   SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                   try {
                      Date birthDay = format.parse(infoModel.getBirthday());
                      showTimePack(birthDay);
                   } catch (ParseException e) {
                       e.printStackTrace();
                   }
               }else{
                   showTimePack(new Date());
               }

            }
        });
        addUserinfoActivityBinding.checkSexLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSexPack();
            }
        });
        addUserinfoActivityBinding.tvDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addUserinfoActivityBinding.loveShowRv.getVisibility() == View.VISIBLE){
                    Drawable drawable = getResources().getDrawable(R.mipmap.icon_gray_down);

                    addUserinfoActivityBinding.tvDrop.setCompoundDrawablesWithIntrinsicBounds(null,
                            null, drawable, null);
                    addUserinfoActivityBinding.loveShowRv.setVisibility(View.GONE);
                }else{
                    Drawable drawable2 = getResources().getDrawable(R.mipmap.icon_gray_down);
                    addUserinfoActivityBinding.tvDrop.setCompoundDrawablesWithIntrinsicBounds(null,
                            null, drawable2, null);
                    addUserinfoActivityBinding.loveShowRv.setVisibility(View.VISIBLE);
                }
            }
        });

        addUserinfoActivityBinding.sureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Logs.e("tag","infoModel.getUserName() = "+infoModel.toString());

                addUserinfoActivityBinding.sureBtn.setEnabled(false);
                RetrofitUtils.getHttpService().saveInfo(infoModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .compose(AddUserInfoActivity.this.<BaseHttpResult<String>>bindUntilEvent(ActivityEvent.STOP))

                        .subscribe(new BaseObserver<String>(null) {
                            @Override
                            public void onSuccess(BaseHttpResult<String> result) {
                                addUserinfoActivityBinding.sureBtn.setEnabled(true);
                                ToastUtils.show("保存成功",2000);
                                MainActivity.gotoMainactivity(AddUserInfoActivity.this);
                                finish();
                            }

                            @Override
                            public void onFailure(String errMsg, boolean isNetError) {
                                addUserinfoActivityBinding.sureBtn.setEnabled(true);
                                showError(errMsg);

                            }
                        });

            }
        });
    }

    private void showSexPack(){
        OptionsPickerView pvOptions = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                if(sexList!=null){
                    addUserinfoActivityBinding.sexTv.setText(sexList.get(options1));
                }
            }
        })
                .isDialog(false)
                .setBgColor(0xffffffff)
                .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                .setDividerColor(0xffe6e6e6)
                .setLineSpacingMultiplier(2.0f)
                .setTitleColor(0xff333333)
                .setTitleSize(18)
                .setTitleText("选择性别")
                .setTitleBgColor(0xfff1f1f1)
                .setCancelText("取消")
                .setCancelColor(0xfffd9153)
                .setSubmitText("确定")
                .setSubmitColor(0xfffd9153)
                .setSubCalSize(12)
                .setContentTextSize(18)
                .setSelectOptions(0)//默认选中项
                .setLabels("", "", "")
                .setTextColorOut(0x99333333)
                .setTextColorCenter(0xff333333)
                .build();
        pvOptions.setPicker(sexList);
        pvOptions.show();
    }


    private void showTimePack(Date date){
        TimePickerView timePickerView = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                addUserinfoActivityBinding.birthdayTv.setText(formatter.format(date));
            }
        }).setType(new boolean[]{true, true, true, false, false, false})
                .setCancelColor(Color.parseColor("#F77B22"))
                .setTitleText("选择生日时间")
                .setTitleColor(Color.parseColor("#666666"))
                .setSubmitColor(Color.parseColor("#F77B22")).build();
        //注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
        Calendar cal = Calendar.getInstance();
        cal.setTime(date );
        timePickerView.setDate(cal);
        timePickerView.show();
    }

    @Override
    protected void initData() {
        RetrofitUtils.getHttpService().getInfo().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.<BaseHttpResult<InfoModel>>bindUntilEvent(ActivityEvent.STOP))
                .subscribe(new BaseObserver<InfoModel>(null) {
                    @Override
                    public void onSuccess(BaseHttpResult<InfoModel> result) {
                         infoModel = result.getData();
                        surveys.clear();
                        surveys.addAll(infoModel.getSurveys());
                        loveUserAdapter.notifyDataSetChanged();
                        addUserinfoActivityBinding.setInfo(infoModel);

                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {
                        showError(errMsg);

                    }
                });


    }

    @Override
    public void showError(String msg) {
        ToastUtils.show(msg,2000);

    }
    private void showAlbum() {
        //参数很多，根据需要添加
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .isCamera(true)
                .maxSelectNum(1)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .imageSpanCount(4)// 每行显示个数
                .selectionMode(PictureConfig.MULTIPLE)// 多选 or 单选PictureConfig.MULTIPLE : PictureConfig.SINGLE
                .previewImage(true)// 是否可预览图片
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                //.sizeMultiplier(0.5f)// glide 加载图片大小 0~1之间 如设置 .glideOverride()无效
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .withAspectRatio(1, 1)// 裁剪比例 如16:9 3:2 3:4 1:1 可自定义
                //.selectionMedia(selectList)// 是否传入已选图片//   .previewEggs()// 预览图片时 是否增强左右滑动图片体验(图片滑动一半即可看到上一张是否选中)
                //.cropCompressQuality(90)// 裁剪压缩质量 默认100
                //.compressMaxKB()//压缩最大值kb compressGrade()为Luban.CUSTOM_GEAR有效
                //.compressWH() // 压缩宽高比 compressGrade()为Luban.CUSTOM_GEAR有效
                //.cropWH()// 裁剪宽高比，设置如果大于图片本身宽高则无效
                .rotateEnabled(false) // 裁剪是否可旋转图片
                //.scaleEnabled()// 裁剪是否可放大缩小图片
                //.recordVideoSecond()//录制视频秒数 默认60s
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == PictureConfig.CHOOSE_REQUEST && data != null) {

                List<LocalMedia> localMedia = PictureSelector.obtainMultipleResult(data);
                Log.d("tag", "localMedia!=null&&localMedia.size()>0"+(localMedia!=null&&localMedia.size()>0)+data.toString());
                if(localMedia!=null&&localMedia.size()>0){
//                    addUserinfoActivityBinding.checkImage.setVisibility(View.GONE);
//                    addUserinfoActivityBinding.profileImage.setVisibility(View.VISIBLE);
                    //设置图片圆角角度

                    Glide.with(this).load(localMedia.get(0).getCompressPath()).apply(RequestOptions.bitmapTransform(new CircleCrop()))
                            .into(addUserinfoActivityBinding.imageIv);
                    uploadImage(localMedia.get(0).getCompressPath());
                }
        }
    }



    private void uploadImage(String imageFilePath){
        // 构造上传请求
        PutObjectRequest put = new PutObjectRequest("kdtantan", "touxiang", imageFilePath);

// 异步上传时可以设置进度回调
        put.setProgressCallback(new OSSProgressCallback<PutObjectRequest>() {
            @Override
            public void onProgress(PutObjectRequest request, long currentSize, long totalSize) {
                Log.d("PutObject", "currentSize: " + currentSize + " totalSize: " + totalSize);
            }
        });

        OSSAsyncTask task = OSSService.getOss(ossBean).asyncPutObject(put, new OSSCompletedCallback<PutObjectRequest, PutObjectResult>() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                Log.d("PutObject", "UploadSuccess");
                infoModel.setPhotoUrl("http://imagess.icloudinn.net/touxiang");
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientExcepion, ServiceException serviceException) {
                // 请求异常
                if (clientExcepion != null) {
                    // 本地异常如网络异常等
                    clientExcepion.printStackTrace();
                }
                if (serviceException != null) {
                    // 服务异常
                    Log.e("ErrorCode", serviceException.getErrorCode());
                    Log.e("RequestId", serviceException.getRequestId());
                    Log.e("HostId", serviceException.getHostId());
                    Log.e("RawMessage", serviceException.getRawMessage());
                }
            }
        });
        task.waitUntilFinished();
    }


    @Override
    public void loginSuccess(String token) {

    }

    @Override
    public void ossGetTokenSuccess(OssBean ossBean) {
         this.ossBean = ossBean;
    }
}
