package com.xt.xiaotan.login.model;

import java.io.Serializable;

/**
 * Created by tiandi
 * on 2020/11/21
 */
public class CheckCodeMobileRequest implements Serializable {
    private String phone;
    private String code;

    public String getPhone() {
        return phone == null ? "" : phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCode() {
        return code == null ? "" : code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
