package com.xt.xiaotan.login.mvp;

import com.xt.xiaotan.login.model.LoginRequest;
import com.xt.xiaotan.login.model.LoginResults;
import com.xt.xiaotan.login.model.OssBean;
import com.xt.xiaotan.mvp.BaseModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.utils.RetrofitUtils;

import io.reactivex.Observable;

public class LoginModel extends BaseModel implements LoginContract.Model{
    @Override
    public Observable<BaseHttpResult<LoginResults>> login(String telPhone, String passWord) {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setPhone(telPhone);
        loginRequest.setPassword(passWord);



        return RetrofitUtils.getHttpService().loginByVer(loginRequest);
    }

    @Override
    public Observable<BaseHttpResult<OssBean>> oss() {
        return RetrofitUtils.getHttpService().oss();
    }


}
