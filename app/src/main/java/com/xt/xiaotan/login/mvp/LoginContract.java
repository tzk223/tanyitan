package com.xt.xiaotan.login.mvp;

import com.xt.xiaotan.login.model.LoginResults;
import com.xt.xiaotan.login.model.OssBean;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.mvp.IView;
import com.xt.xiaotan.net.BaseHttpResult;

import io.reactivex.Observable;

public interface LoginContract {
    public interface View extends IView{

        void loginSuccess(String token);

        void ossGetTokenSuccess(OssBean ossBean);

    }

    public interface Model extends IModel {
        Observable<BaseHttpResult<LoginResults>> login(String telPhone, String passWord);
        Observable<BaseHttpResult<OssBean>> oss();
    }
}
