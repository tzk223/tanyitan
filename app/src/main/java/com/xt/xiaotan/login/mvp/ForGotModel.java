package com.xt.xiaotan.login.mvp;

import com.xt.xiaotan.login.model.LoginResults;
import com.xt.xiaotan.mvp.BaseModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.utils.RetrofitUtils;

import io.reactivex.Observable;

/**
 * Created by tiandi
 * on 2020/11/23
 */
public class ForGotModel extends BaseModel implements ForGotContract.Model {
    @Override
    public Observable<BaseHttpResult<String>> getMsg(String telPhone) {
        return RetrofitUtils.getHttpService().sendMsg(telPhone);
    }

    @Override
    public Observable<BaseHttpResult<String>> checkNumberisUser(String telPhone) {
        return RetrofitUtils.getHttpService().checkNumberisUser(telPhone);
    }


}
