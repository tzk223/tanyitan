package com.xt.xiaotan.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.trello.rxlifecycle2.android.ActivityEvent;
import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.databinding.VercodeActivityBinding;
import com.xt.xiaotan.first.MainActivity;
import com.xt.xiaotan.login.model.LoginResults;
import com.xt.xiaotan.login.mvp.ForGotPresenter;
import com.xt.xiaotan.login.mvp.VerificationContract;
import com.xt.xiaotan.login.mvp.VerificationPresenter;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.set.InfoModel;
import com.xt.xiaotan.utils.RetrofitUtils;
import com.xt.xiaotan.utils.SharePrefanceUtils;
import com.xt.xiaotan.utils.ToastUtils;
import com.xt.xiaotan.view.CodeEditText;

import java.security.PublicKey;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class VerificationActivity extends BaseMvpActivity<VerificationPresenter> implements VerificationContract.View {
    private VercodeActivityBinding binding;
    private String phone;
    private String code;
    private Disposable countdownDisposable;
    public boolean isCanAgainSendMsg;




    public static void gotoVerificationActivity(Context context,String phone){
        Intent intent = new Intent(context,VerificationActivity.class);
        intent.putExtra("phone",phone);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this,R.layout.vercode_activity);
        binding.setVerificationActivity(this);
        super.onCreate(savedInstanceState);
        getMsgCode();
    }

    @Override
    protected VerificationPresenter createPresenter() {
        return new VerificationPresenter(this);
    }

    @Override
    protected int getLayoutId() {

        return R.layout.vercode_activity;
    }

    @Override
    protected void getIntent(Intent intent) {
        phone = intent.getStringExtra("phone");

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {
        binding.codeEdittext.setOnTextFinishListener(new CodeEditText.OnTextFinishListener() {
            @Override
            public void onTextFinish(CharSequence text, int length) {
                code = text.toString();


            }
        });
       binding.backIv.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               finish();
           }
       });
    }
   //点击确定
    public void setVerification(){
        mPresenter.VerificationMsg(phone,code);
    }

    @Override
    protected void initData() {
      countDown();


    }

    private void countDown(){
        //intervalRange四个参数分别为：从0开始、到60结束、延时0开始，单位时间（NANOSECONDS,MICROSECONDS,MILLISECONDS,SECONDS,MINUTES,HOURS,DAYS）。
        countdownDisposable = Flowable.intervalRange(0, 60, 0, 1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {

                        binding.tvTime.setText((60 - aLong) + "S");
                        binding.tvTime.setEnabled(false);


                    }
                })
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        //倒计时完毕事件处理
                        binding.tvTime.setText(Html.fromHtml("<font color='#03DAC5'>重新发送</font>"));
                        binding.tvTime.setEnabled(true);


                    }
                })
                .subscribe();

    }

    @Override
    public void showError(String msg) {
        ToastUtils.show(msg,2000);

        binding.tvTisi.setText(Html.fromHtml("<font color='#ff0000'>验证码错误请重新输入</font>"));
        binding.codeEdittext.setmStrokeDrawable( getResources().getDrawable(R.drawable.bg_solide_efefef_radius_10_red));
    }

    public void getMsgCode() {
      mPresenter.getMsg(phone);


    }

    @Override
    protected void onStop() {
        super.onStop();
        if(null != countdownDisposable&&!countdownDisposable.isDisposed()){
            countdownDisposable.dispose();
        }
    }

    @Override
    public void verCodeSuccess(LoginResults loginResults) {
        if(loginResults!=null){
            SharePrefanceUtils.putStr(VerificationActivity.this,"token",loginResults.token);
            getUserInfo();

        }
    }
    public void getUserInfo(){
      RetrofitUtils.getHttpService().getInfo().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .compose(this.<BaseHttpResult<InfoModel>>bindUntilEvent(ActivityEvent.STOP))
            .subscribe(new BaseObserver<InfoModel>(null) {
        @Override
        public void onSuccess(BaseHttpResult<InfoModel> result) {
           if(result!=null&&result.getData()!=null){
               if(!TextUtils.isEmpty(result.getData().getUserName())){
                   MainActivity.gotoMainactivity(VerificationActivity.this);
               }else{
                   AddUserInfoActivity.gotoAddUserInfoActivity(VerificationActivity.this);
               }
               VerificationActivity.this.finish();
           }

        }

        @Override
        public void onFailure(String errMsg, boolean isNetError) {
            showError(errMsg);
            AddUserInfoActivity.gotoAddUserInfoActivity(VerificationActivity.this);

        }
    });


}
}
