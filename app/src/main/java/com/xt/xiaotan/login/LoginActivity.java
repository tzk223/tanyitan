package com.xt.xiaotan.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.gyf.immersionbar.ImmersionBar;
import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.databinding.LoginActivityBinding;
import com.xt.xiaotan.first.MainActivity;
import com.xt.xiaotan.login.model.OssBean;
import com.xt.xiaotan.login.mvp.LoginContract;
import com.xt.xiaotan.login.mvp.LoginPresenter;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.utils.LogUtils;
import com.xt.xiaotan.utils.SharePrefanceUtils;
import com.xt.xiaotan.utils.ToastUtils;
import com.xt.xiaotan.utils.cache.CacheManager;

import java.util.logging.Logger;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

public class LoginActivity extends BaseMvpActivity implements LoginContract.View {

    private LoginActivityBinding binding;
    private LoginPresenter loginPresenter;

    public static void gotoLoginActivity(Context context){
        Intent intent = new Intent(context,LoginActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this,R.layout.login_activity);
        binding.setLoginActivity(this);
        super.onCreate(savedInstanceState);
        finishAllAct(this);
        ImmersionBar.with(this).statusBarDarkFont(true).fitsSystemWindows(true).statusBarColor(R.color.color_f9000000).init();
    }

    @Override
    protected BasePresenter createPresenter() {
        loginPresenter = new LoginPresenter(this);
        return loginPresenter;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.login_activity;
    }

    @Override
    protected void getIntent(Intent intent) {

    }

    @Override
    protected void initView() {
//




    }

    @Override
    protected void initListener() {
        binding.titleTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("tag","titleTv");
            }
        });

        binding.loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("tag","onLoginBtn");
                Log.e("tag","onLoginBtn");
                String phone = binding.etPhone.getText().toString();
                String psd = binding.etPsd.getText().toString();
                loginPresenter.login(phone,psd);
            }
        });
        binding.tvForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("tag","tvForgot");
                ForGotPsdActivity.gotForGotActivity(LoginActivity.this);


            }
        });
        binding.notPasswordTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterActivity.gotoRegisterActivity(LoginActivity.this);
            }
        });

        binding.weixinLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        binding.qqLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        binding.weiboLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    protected void initData() {



    }

    @Override
    public void showError(String msg) {
        ToastUtils.show(msg,2000);

    }



    @Override
    public void loginSuccess(String token) {
        SharePrefanceUtils.putStr(this,"token",token);
       String token1 = SharePrefanceUtils.getStr(this,"token");
       Log.d("tag",token1+"--------------------------");
        MainActivity.gotoMainactivity(this);
        finish();
    }

    @Override
    public void ossGetTokenSuccess(OssBean ossBean) {

    }
}
