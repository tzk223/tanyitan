package com.xt.xiaotan.login.mvp;

import com.xt.xiaotan.login.model.CheckCodeMobileRequest;
import com.xt.xiaotan.login.model.LoginResults;
import com.xt.xiaotan.mvp.BaseModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.utils.RetrofitUtils;

import io.reactivex.Observable;

/**
 * Created by tiandi
 * on 2020/11/23
 */
public class VerificationModel extends BaseModel implements VerificationContract.Model {



    @Override
    public Observable<BaseHttpResult<LoginResults>> VerificationMsg(String telPhone, String code) {
        CheckCodeMobileRequest checkCodeMobileRequest = new CheckCodeMobileRequest();
        checkCodeMobileRequest.setCode(code);
        checkCodeMobileRequest.setPhone(telPhone);
        return RetrofitUtils.getHttpService().checkMsg(checkCodeMobileRequest);
    }

    @Override
    public Observable<BaseHttpResult<String>> getMsg(String telPhone) {
        return RetrofitUtils.getHttpService().sendMsg(telPhone);
    }
}
