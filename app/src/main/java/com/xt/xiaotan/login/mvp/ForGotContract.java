package com.xt.xiaotan.login.mvp;

import com.xt.xiaotan.login.model.LoginResults;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.mvp.IView;
import com.xt.xiaotan.net.BaseHttpResult;

import io.reactivex.Observable;

/**
 * Created by tiandi
 * on 2020/11/23
 */
public interface ForGotContract {
    interface View extends IView{
        void getMsgSuccess();

    }
    interface Model extends IModel{
        Observable<BaseHttpResult<String>> getMsg(String telPhone);
        Observable<BaseHttpResult<String>> checkNumberisUser(String telPhone);
    }
}
