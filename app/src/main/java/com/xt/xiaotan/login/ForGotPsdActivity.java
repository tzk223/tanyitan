package com.xt.xiaotan.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.databinding.ForgetpasswordActivityBinding;
import com.xt.xiaotan.databinding.LoginActivityBinding;
import com.xt.xiaotan.first.MainActivity;
import com.xt.xiaotan.login.mvp.ForGotContract;
import com.xt.xiaotan.login.mvp.ForGotPresenter;
import com.xt.xiaotan.login.mvp.LoginContract;
import com.xt.xiaotan.login.mvp.LoginPresenter;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.utils.ToastUtils;
import com.xt.xiaotan.utils.cache.CacheManager;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

public class ForGotPsdActivity extends BaseMvpActivity implements ForGotContract.View {

    private ForgetpasswordActivityBinding binding;
    private ForGotPresenter presenter;
    public boolean isEnable = true;

    public static void gotForGotActivity(Context context){
        Intent intent = new Intent(context,ForGotPsdActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this,R.layout.forgetpassword_activity);
        binding.setForGot(this);
        super.onCreate(savedInstanceState);

    }

    @Override
    protected BasePresenter createPresenter() {
        presenter = new ForGotPresenter(this);
        return presenter;
    }

    @Override
    protected int getLayoutId() {


        return R.layout.forgetpassword_activity;
    }

    @Override
    protected void getIntent(Intent intent) {

    }

    @Override
    protected void initView() {





    }

    @Override
    protected void initListener() {


        binding.loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("tag","onLoginBtn");
                Log.e("tag","onLoginBtn");
                String phone = binding.etPhone.getText().toString();
                isEnable = false;
                presenter.checkNumberisUser(phone);


            }
        });
       binding.backIv.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               finish();
           }
       });

    }

    @Override
    protected void initData() {



    }

    @Override
    public void showError(String msg) {
        isEnable = true;
        ToastUtils.show(msg,2000);
    }




    @Override
    public void getMsgSuccess() {
        VerificationActivity.gotoVerificationActivity(this,binding.etPhone.getText().toString().trim());

    }
}
