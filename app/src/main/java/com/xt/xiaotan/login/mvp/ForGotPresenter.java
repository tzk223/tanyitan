package com.xt.xiaotan.login.mvp;

import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;

/**
 * Created by tiandi
 * on 2020/11/23
 */
public class ForGotPresenter extends BasePresenter {
    private ForGotContract.Model model;
    private ForGotContract.View view;
    @Override
    protected IModel createModel() {
        model = new ForGotModel();
        return model;
    }
    public ForGotPresenter(ForGotContract.View view){
        this.view = view;
    }

    public void getMsg(String phone){
        addDispose(model.getMsg(phone), new BaseObserver(view) {
            @Override
            public void onSuccess(BaseHttpResult result) {
                view.getMsgSuccess();

            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {
               view.showError(errMsg);
            }
        });
    }

    public void checkNumberisUser(final String phone){
        addDispose(model.checkNumberisUser(phone), new BaseObserver(view) {
            @Override
            public void onSuccess(BaseHttpResult result) {
                getMsg(phone);
            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {
                view.showError(errMsg);

            }


        });
    }
}
