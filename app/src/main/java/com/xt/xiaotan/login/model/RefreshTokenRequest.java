package com.xt.xiaotan.login.model;

import java.io.Serializable;

/**
 * Created by tiandi
 * on 2020/11/28
 */
public class RefreshTokenRequest implements Serializable {
    private  String token;

    public String getToken() {
        return token == null ? "" : token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
