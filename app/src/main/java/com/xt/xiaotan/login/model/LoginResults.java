package com.xt.xiaotan.login.model;

import java.io.Serializable;

/**
 * Created by tiandi
 * on 2020/11/20
 */
public class LoginResults implements Serializable {
   public  String token;
   public String expire;//当前时间

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getToken() {
        return token == null ? "" : token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
