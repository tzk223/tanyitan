package com.xt.xiaotan.login.model;

import com.xt.xiaotan.net.BaseRequest;

/**
 * Created by tiandi
 * on 2020/11/20
 */
public class LoginRequest extends BaseRequest {
    private String phone;
    private String password;
     public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password == null ? "" : password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
