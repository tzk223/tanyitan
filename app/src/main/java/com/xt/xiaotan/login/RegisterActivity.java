package com.xt.xiaotan.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.databinding.RegisterActivityBinding;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.utils.ToastUtils;

public class RegisterActivity extends BaseMvpActivity {
    public RegisterActivityBinding binding;

    public static void gotoRegisterActivity(Context context){
        Intent intent = new Intent(context,RegisterActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.register_activity);
        binding.setRegister(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void getIntent(Intent intent) {

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {
       binding.notPasswordTv.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               finish();
           }
       });

       binding.loginBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               String phoneStr = binding.phoneNumber.getText().toString();
               if(TextUtils.isEmpty(phoneStr)){
                   ToastUtils.show("请输入手机号码",1000);
                   return;
               }
               VerificationActivity.gotoVerificationActivity(RegisterActivity.this,phoneStr);
               finish();
           }
       });
    }

    @Override
    protected void initData() {

    }

    @Override
    public void showError(String msg) {

    }
}
