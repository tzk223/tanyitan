package com.xt.xiaotan.login.mvp;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.util.Log;

import com.xt.xiaotan.login.LoginActivity;
import com.xt.xiaotan.login.model.LoginResults;
import com.xt.xiaotan.login.model.OssBean;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.utils.SharePrefanceUtils;
import com.xt.xiaotan.utils.cache.CacheManager;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

public class LoginPresenter extends BasePresenter {
    private LoginContract.View view;
    private LoginModel loginModel;
    @Override
    protected IModel createModel() {
        loginModel = new LoginModel();
        return loginModel;
    }
    public LoginPresenter(LoginContract.View view){
        this.view = view;

    }


    @SuppressLint("CheckResult")
    public void login(final String phone, String psd) {
        addDispose(loginModel.login(phone, psd), new BaseObserver<LoginResults>(view) {
            @Override
            public void onSuccess(BaseHttpResult<LoginResults> result) {
                LoginResults loginResults = result.getData();
                CacheManager.getInStance().put(CacheManager.TOKENKEY, loginResults.getToken());
                view.loginSuccess(loginResults.getToken());
            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {
                Log.e("tag", "ErrMsg = " + errMsg);
                view.showError(errMsg);


            }
        });
    }


    public void oss(){
        addDispose(loginModel.oss(), new BaseObserver<OssBean>(view) {

            @Override
            public void onSuccess(BaseHttpResult<OssBean> result) {
               view.ossGetTokenSuccess(result.getData());
            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {
                Log.e("tag","ErrMsg = "+errMsg);
                view.showError(errMsg);


            }
        });

    }




}
