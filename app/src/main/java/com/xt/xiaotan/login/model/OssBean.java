package com.xt.xiaotan.login.model;

import java.io.Serializable;

public class OssBean implements Serializable {
   public Credentials credentials;
    public static class Credentials implements Serializable{
        public String  securityToken;

        public String accessKeyId;

        public String accessKeySecret;

        @Override
        public String toString() {
            return "Credentials{" +
                    "securityToken='" + securityToken + '\'' +
                    ", accessKeyId='" + accessKeyId + '\'' +
                    ", accessKeySecret='" + accessKeySecret + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "OssBean{" +
                "credentials=" + credentials.toString() +
                '}';
    }
}
