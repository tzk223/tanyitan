package com.xt.xiaotan.login.mvp;

import com.xt.xiaotan.login.model.LoginResults;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.mvp.IModel;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;

/**
 * Created by tiandi
 * on 2020/11/23
 */
public class VerificationPresenter extends BasePresenter {
    private VerificationContract.Model model;
    private VerificationContract.View view;
    @Override
    protected IModel createModel() {
        model = new VerificationModel();
        return model;
    }
    public VerificationPresenter(VerificationContract.View view){
        this.view = view;
    }

    public void VerificationMsg(String phone,String code){
        addDispose(model.VerificationMsg(phone,code), new BaseObserver<LoginResults>(view) {
            @Override
            public void onSuccess(BaseHttpResult<LoginResults> result) {
                view.verCodeSuccess(result.getData());
            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {
               view.showError(errMsg);
            }
        });
    }

    public void getMsg(String phone){
        addDispose(model.getMsg(phone), new BaseObserver(view) {
            @Override
            public void onSuccess(BaseHttpResult result) {


            }

            @Override
            public void onFailure(String errMsg, boolean isNetError) {
                view.showError(errMsg);
            }
        });
    }




}
