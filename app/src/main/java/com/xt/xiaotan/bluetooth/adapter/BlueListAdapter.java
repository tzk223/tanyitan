package com.xt.xiaotan.bluetooth.adapter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.hc.bluetoothlibrary.DeviceModule;
import com.xt.xiaotan.R;
import com.xt.xiaotan.goods.bean.TypeBean;

import java.util.List;

public class BlueListAdapter extends RecyclerView.Adapter<BlueListAdapter.ViewHolder> {

    public Context context;

    private List<DeviceModule> list;

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public BlueListAdapter(Context context){
        this.context = context;
    }

    public void setData(List<DeviceModule> list){
        this.list = list;
        notifyDataSetChanged();
    }


    public interface OnItemClickListener{
        void onItemClick(int  position,int type);
    }
    @NonNull
    @Override
    public BlueListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.blue_list_item, parent, false);
        return new BlueListAdapter.ViewHolder(view);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull BlueListAdapter.ViewHolder holder, int position) {
        final DeviceModule deviceModule = list.get(position);
        final BluetoothDevice typeBean = deviceModule.getDevice();
        if(typeBean.getName()!=null){
            holder.name_tv.setText("设备号："+typeBean.getName());
        }
        if(typeBean.getBondState() == BluetoothDevice.BOND_BONDED){
            holder.clone_tv.setText("已连接");
            holder.clone_tv.setBackground(null);
            holder.clone_tv.setTextColor(Color.parseColor("#f36612"));
        }else{
            holder.clone_tv.setText("连接");
            holder.clone_tv.setBackground(context.getDrawable(R.drawable.bg_xiaotan_fff36612));
            holder.clone_tv.setTextColor(Color.parseColor("#ffffff"));
        }
        holder.clone_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  onItemClickListener.onItemClick(position,1);
            }
        });
        holder.check_type_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(position,1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list!=null?list.size():0;
    }
    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView name_tv;
        public TextView time_tv;

        public TextView clone_tv;

        public LinearLayout check_type_ll;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name_tv = itemView.findViewById(R.id.name_tv);
            time_tv = itemView.findViewById(R.id.time_tv);
            clone_tv = itemView.findViewById(R.id.clone_tv);
            check_type_ll = itemView.findViewById(R.id.check_type_ll);
        }
    }
}
