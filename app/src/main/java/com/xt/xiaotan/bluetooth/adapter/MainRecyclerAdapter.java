package com.xt.xiaotan.bluetooth.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.widget.TextView;


import androidx.annotation.RequiresApi;

import com.hc.bluetoothlibrary.DeviceModule;
import com.xt.xiaotan.R;
import com.xt.xiaotan.bluetooth.recyclerAdapterBasic.ItemClickListener;
import com.xt.xiaotan.bluetooth.recyclerAdapterBasic.ViewHolder;

import java.util.List;

public class MainRecyclerAdapter extends RecyclerCommonAdapter<DeviceModule> {
    private Context context;
    public MainRecyclerAdapter(Context context, List<DeviceModule> strings, int layoutId){
        super(context,strings,layoutId);
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void convert(ViewHolder holder, DeviceModule typeBean, int position, ItemClickListener itemClickListener) {

        if(typeBean.getName()!=null){
            ((TextView)holder.getView(R.id.name_tv)).setText("设备号："+typeBean.getName());
        }
        if(typeBean.isBeenConnected()){
            ((TextView)holder.getView(R.id.clone_tv)).setText("已连接");
            ((TextView)holder.getView(R.id.clone_tv)).setBackground(null);
            ((TextView)holder.getView(R.id.clone_tv)).setTextColor(Color.parseColor("#f36612"));
        }else{
            ((TextView)holder.getView(R.id.clone_tv)).setText("连接");
            ((TextView)holder.getView(R.id.clone_tv)).setBackground(context.getDrawable(R.drawable.bg_xiaotan_fff36612));
            ((TextView)holder.getView(R.id.clone_tv)).setTextColor(Color.parseColor("#ffffff"));
        }
    }

}
