package com.xt.xiaotan.bluetooth;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gyf.immersionbar.ImmersionBar;
import com.hc.bluetoothlibrary.DeviceModule;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.bluetooth.bean.FragmentMessageItem;
import com.xt.xiaotan.databinding.BluetoochSearchActivityBinding;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.search.model.GoodEntityBean;
import com.xt.xiaotan.search.model.GoodsListBean;
import com.xt.xiaotan.utils.RetrofitUtils;
import com.xt.xiaotan.utils.ToastUtils;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Set;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import pub.devrel.easypermissions.EasyPermissions;



public class BlueToothSearchActivity extends BaseMvpActivity implements EasyPermissions.PermissionCallbacks, IMessageInterface {
    private static final int PERMISSIONS_ACCESS_BLUE = 30;

    public static final String BLE_KEY = "BLE_KEY_POP_WINDOW";
    private BluetoochSearchActivityBinding binding;

    public static final String sendTal = "cmd:startread;";



    private ObjectAnimator rotateAnimation;


    private Storage mStorage;


    private String goodId;

    private GoodEntityBean goodEntityBean;

    private List<DeviceModule> modules;
    private HoldBluetooth mHoldBluetooth;

    private IMessageInterface mMessage,mCustom,mThree,mLog;

    private DeviceModule mErrorDisconnect;

    public boolean isSendHex = true;

    public boolean isReadHex = true;

    public boolean isShowTime = true;

    public boolean isShowMyData = true;

    public boolean readGetDevice = false;

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    public static void gotoBlueToothSearchActivity(Context context,String goodId) {
        Intent intent = new Intent(context, BlueToothSearchActivity.class);
        intent.putExtra("goodId",goodId);
        context.startActivity(intent);
    }
    public static void gotoBlueToothSearchActivity(Context context, GoodEntityBean goodEntityBean) {
        Intent intent = new Intent(context, BlueToothSearchActivity.class);
        intent.putExtra("goodEntityBean",goodEntityBean);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.bluetooch_search_activity);
        binding.setBlueToothSearchActivity(this);
        super.onCreate(savedInstanceState);
        ImmersionBar.with(this).statusBarDarkFont(true).fitsSystemWindows(true).statusBarColor(R.color.color333333).init();
        rotateAnim();
        initBlue();
        bloothSetting();
        binding.blueDescTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BlueListActivity.gotoBlueListActivity(BlueToothSearchActivity.this);
            }
        });
    }

    private void setDefaultData(GoodEntityBean goodEntityBean ){
        Glide.with(this).load(goodEntityBean.photoUrl1).apply(new RequestOptions().error(R.drawable.ic_launcher_background))
                .into(binding.iamge);
        binding.name.setText(goodEntityBean.goodsName);
        binding.position.setText(goodEntityBean.positionName+"-"+goodEntityBean.furnitureName);
        binding.time.setText(goodEntityBean.createDateStr);
    }

    private void showBlueTrue(){
        binding.bluePipeTv.setVisibility(View.VISIBLE);
        binding.blueDescTv.setVisibility(View.GONE);
        binding.bluetoolSignLl.setBackgroundResource(R.drawable.bg_dcdcdc_ovel);
        binding.searchDescTv.setTextColor(Color.parseColor("#FF9143"));
        binding.searchDescTv.setText("搜索中");
    }

    private Handler mFragmentHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            FragmentMessageItem item = (FragmentMessageItem) msg.obj;
            mHoldBluetooth.sendData(item.getModule(),item.getByteData().clone());
            return false;
        }
    });
    //初始化蓝牙数据的监听
    private void initDataListener() {
        HoldBluetooth.OnReadDataListener dataListener = new HoldBluetooth.OnReadDataListener() {
            @Override
            public void readData(String mac, byte[] data) {
                FragmentMessageItem fragmentMessageItem =  new FragmentMessageItem(Analysis.getByteToString(data,isReadHex), isShowTime?Analysis.getTime():null, false, modules.get(0),isShowMyData);
                if("ok".equals(fragmentMessageItem.getData())){
                    readGetDevice = true;
                }else if(readGetDevice){
                    if(fragmentMessageItem.getData().contains(goodEntityBean.goodsRfid)){
                        showSuccess();
                    }
                }
            }

            @Override
            public void reading(boolean isStart) {

            }

            @Override
            public void connectSucceed() {
                modules = mHoldBluetooth.getConnectedArray();
                ToastUtils.show("连接蓝牙设备成功",1000);
                FragmentMessageItem fragmentMessageItem =   new FragmentMessageItem(isSendHex, Analysis.getBytes(sendTal, isSendHex), isShowTime ? Analysis.getTime() : null, true, modules.get(0), isShowMyData);
                mHoldBluetooth.sendData(fragmentMessageItem.getModule(),fragmentMessageItem.getByteData().clone());
                showBlueTrue();
            }

            @Override
            public void errorDisconnect(final DeviceModule deviceModule) {//蓝牙异常断开
                if (mErrorDisconnect == null) {//判断是否已经重复连接
                    mErrorDisconnect = deviceModule;
                    if (mHoldBluetooth != null && deviceModule != null) {
                        mFragmentHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mHoldBluetooth.setDevelopmentMode(BlueToothSearchActivity.this);
                                mHoldBluetooth.connect(deviceModule);

                            }
                        },2000);
                        return;
                    }
                }

            }

            @Override
            public void readNumber(int number) {

            }

            @Override
            public void readLog(String className, String data, String lv) {

            }
        };
        mHoldBluetooth.setOnReadListener(dataListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 5){
//            DeviceModule deviceModule =data.getParcelableExtra("DeviceModule");
//            mHoldBluetooth.setDevelopmentMode(BlueToothSearchActivity.this);
//            mHoldBluetooth.connect(deviceModule);
        }
    }

    private void initBlue(){
        mHoldBluetooth = HoldBluetooth.getInstance();
        mHoldBluetooth.initHoldBluetooth(BlueToothSearchActivity.this, new HoldBluetooth.UpdateList() {
            @Override
            public void update(boolean isStart, DeviceModule deviceModule) {

            }

            @Override
            public void updateMessyCode(boolean isStart, DeviceModule deviceModule) {

            }
        });
        initDataListener();
    }

    public void initAll() {
        goodId = getIntent().getStringExtra("goodId");
        if(!TextUtils.isEmpty(goodId)){
            searchGoods(goodId);
        }
        goodEntityBean = (GoodEntityBean) getIntent().getSerializableExtra("goodEntityBean");
        if(goodEntityBean!=null){
            setDefaultData(goodEntityBean);

        }

        mStorage = new Storage(this);//sp存储

        //初始化View
        initView();
    }

    private void initPermission() {
        String[] perms = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
        if (EasyPermissions.hasPermissions(this, perms)) {
        } else {
            EasyPermissions.requestPermissions(this, "需要蓝牙权限", PERMISSIONS_ACCESS_BLUE, perms);
        }
    }



    private void showSuccess(){
        binding.bluetoolSignLl.setBackgroundResource(R.drawable.bg_a2ff4b_ovel);
        binding.searchDescTv.setTextColor(Color.parseColor("#A2FF4B"));
        binding.searchDescTv.setText("搜索成功");
    }
    public void rotateAnim(){
        rotateAnimation = ObjectAnimator.ofFloat(binding.progress,
                "rotation", 0.0F, 360.0F);
        rotateAnimation.setDuration(3000);//设置旋转时间
        rotateAnimation.setRepeatCount(-1);
        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.start();//开始执行动画（顺时针旋转动画）
    }

    @Override
    protected void getIntent(Intent intent) {

    }


    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {

    }


    @Override
    protected void initData() {

    }

    @Override
    public void showError(String msg) {

    }

    @Override
    protected void onPause() {
        super.onPause();

    }
    private void searchGoods(String id){
            RetrofitUtils.getHttpService().getGoodMsg(id).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .compose(this.<BaseHttpResult<GoodEntityBean>>bindUntilEvent(ActivityEvent.STOP))
                    .subscribe(new BaseObserver<GoodEntityBean>(null) {
                        @Override
                        public void onSuccess(BaseHttpResult<GoodEntityBean> result) {
                            Log.d("tag",result.getData().toString());
                        }

                        @Override
                        public void onFailure(String errMsg, boolean isNetError) {
                            showError(errMsg);

                        }
                    });

    }
    private void bloothSetting(){
        RetrofitUtils.getHttpService().bluetoothconfigList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.<BaseHttpResult>bindUntilEvent(ActivityEvent.STOP))
                .subscribe(new BaseObserver(null) {
                    @Override
                    public void onSuccess(BaseHttpResult result) {
                        Log.d("tag",result.getData().toString());
                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {
                        showError(errMsg);

                    }
                });

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(rotateAnimation!=null){
            rotateAnimation = null;
        }
        if(modules!=null&&modules.size()>0){
            mHoldBluetooth.disconnect(modules.get(0));
        }

    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void setHandler(Handler handler) {

    }

    @Override
    public void readData(int state, Object o, byte[] data) {

    }

    @Override
    public void updateState(int state) {

    }
}
