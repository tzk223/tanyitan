package com.xt.xiaotan.bluetooth.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.mvp.BasePresenter;

public class BlueDescActivity extends BaseMvpActivity {

    public static void gotoBlueDescActivity(Context context){
        Intent intent = new Intent(context,BlueDescActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(getLayoutId());
        super.onCreate(savedInstanceState);

    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.blue_desc_activity;
    }

    @Override
    protected void getIntent(Intent intent) {

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {
         findViewById(R.id.back_iv).setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 finish();
             }
         });
    }

    @Override
    protected void initData() {

    }

    @Override
    public void showError(String msg) {

    }
}
