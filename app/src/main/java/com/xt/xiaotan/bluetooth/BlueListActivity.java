package com.xt.xiaotan.bluetooth;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.gyf.immersionbar.ImmersionBar;
import com.hc.bluetoothlibrary.DeviceModule;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.xt.xiaotan.R;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.bluetooth.adapter.BlueDescActivity;
import com.xt.xiaotan.bluetooth.adapter.BlueListAdapter;
import com.xt.xiaotan.databinding.BlueListActivityBinding;
import com.xt.xiaotan.mvp.BasePresenter;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.utils.RetrofitUtils;
import com.xt.xiaotan.utils.ToastUtils;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import pub.devrel.easypermissions.EasyPermissions;

import static java.lang.Math.abs;
import static java.lang.Math.pow;

public class BlueListActivity extends BaseMvpActivity implements EasyPermissions.PermissionCallbacks{
    private static final int PERMISSIONS_ACCESS_BLUE = 30;
    private BlueListActivityBinding binding;
    private BluetoothAdapter mBluetoothAdapter;

    private BlueListAdapter blueListAdapter;

    private List<DeviceModule> deviceModules = new ArrayList<>();

    private HoldBluetooth mHoldBluetooth;


    public static  void gotoBlueListActivity(Activity context){
         Intent intent = new Intent(context,BlueListActivity.class);
         context.startActivityForResult(intent,5);
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.blue_list_activity);
        binding.setBlueListActivity(this);
        super.onCreate(savedInstanceState);
        ImmersionBar.with(this).statusBarDarkFont(true).fitsSystemWindows(true).statusBarColor(R.color.color_FF6F3C).init();
        setrv();
        initHoldBluetooth();
        getDeviceList();

        binding.backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.blueDescTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BlueDescActivity.gotoBlueDescActivity(BlueListActivity.this);
            }
        });

        binding.beginTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.beginTv.getText().toString().equals("开始扫描")){
                    initPermission();

                }else if(binding.beginTv.getText().toString().equals("停止扫描")){
                    mHoldBluetooth.stopScan();
                    binding.beginTv.setText("开始扫描");
                }
            }
        });
    }
    private void initPermission() {
        String[] perms = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
        if (EasyPermissions.hasPermissions(this, perms)) {
            deviceModules.clear();
            mHoldBluetooth.scan(false);
            binding.beginTv.setText("停止扫描");
        } else {
            EasyPermissions.requestPermissions(this, "需要蓝牙权限", PERMISSIONS_ACCESS_BLUE, perms);
        }
    }
    private void initHoldBluetooth() {
        mHoldBluetooth = HoldBluetooth.getInstance();
        final HoldBluetooth.UpdateList updateList = new HoldBluetooth.UpdateList() {
            @Override
            public void update(boolean isStart,DeviceModule deviceModule) {
                Log.d("tag",isStart+"-------");
                if (isStart){
                    deviceModule.isCollectName(BlueListActivity.this);
                    deviceModules.add(deviceModule);
                }else {
                    blueListAdapter.setData(deviceModules);
                }
            }

            @Override
            public void updateMessyCode(boolean isStart, DeviceModule deviceModule) {

            }
        };
        mHoldBluetooth.initHoldBluetooth(BlueListActivity.this,updateList);
    }


    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }


    private void setrv(){
        binding.recyleV.setLayoutManager(new LinearLayoutManager(this));
        blueListAdapter = new BlueListAdapter(this);
        binding.recyleV.setAdapter(blueListAdapter);
        blueListAdapter.setOnItemClickListener(new BlueListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int typeBean ,int type) {
                  mHoldBluetooth.connect(deviceModules.get(typeBean));
//                  Intent intent = new Intent();
//                  intent.putExtra("DeviceModule",deviceModules.get(typeBean));
//                  setResult(RESULT_OK,intent);
                  finish();
            }
        });
    }



    @Override
    protected void getIntent(Intent intent) {

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
      //  unregisterReceiver(searchDevices);
        mHoldBluetooth.stopScan();
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public void showError(String msg) {

    }
    private void getDeviceList(){
        RetrofitUtils.getHttpService().deviceList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.<BaseHttpResult>bindUntilEvent(ActivityEvent.STOP))
                .subscribe(new BaseObserver(null) {
                    @Override
                    public void onSuccess(BaseHttpResult result) {
                        Log.d("tag",result.getData().toString());
                    }

                    @Override
                    public void onFailure(String errMsg, boolean isNetError) {
                        showError(errMsg);

                    }
                });

    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        mHoldBluetooth.scan(true);
        binding.beginTv.setText("停止扫描");
    }


    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        mHoldBluetooth.scan(true);
        binding.beginTv.setText("停止扫描");
    }
}
