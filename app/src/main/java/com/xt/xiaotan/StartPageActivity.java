package com.xt.xiaotan;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.trello.rxlifecycle2.android.ActivityEvent;
import com.xt.xiaotan.base.BaseActivity;
import com.xt.xiaotan.base.BaseMvpActivity;
import com.xt.xiaotan.first.MainActivity;
import com.xt.xiaotan.login.LoginActivity;
import com.xt.xiaotan.login.model.LoginResults;
import com.xt.xiaotan.login.model.RefreshTokenRequest;
import com.xt.xiaotan.net.BaseHttpResult;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.utils.RetrofitUtils;
import com.xt.xiaotan.utils.SharePrefanceUtils;
import com.xt.xiaotan.utils.cache.CacheManager;

import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;

/**
 * Created by tiandi
 * on 2020/11/28
 */
public class StartPageActivity extends BaseActivity {
    private Disposable disposable;
    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected void getIntent(Intent intent) {

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {

    }

    @SuppressLint("CheckResult")
    @Override
    protected void initData() {
        Log.e("tag","initData");
        Log.e("tag","TextUtils.isEmpty(CacheManager.getInStance().getAsString(CacheManager.TOKENKEY) " +
                "= "+TextUtils.isEmpty(CacheManager.getInStance().getAsString(CacheManager.TOKENKEY)));
        if(!TextUtils.isEmpty(SharePrefanceUtils.getStr(MyApplication.getContext(),"token"))){
            String oldToken = SharePrefanceUtils.getStr(MyApplication.getContext(),"token");
            RefreshTokenRequest refreshTokenRequest = new RefreshTokenRequest();
            refreshTokenRequest.setToken(oldToken);
           RetrofitUtils.getHttpService().refreshToken(refreshTokenRequest).subscribeOn(Schedulers.io())
                   .compose(this.<BaseHttpResult<LoginResults>>bindUntilEvent(ActivityEvent.STOP))
            .observeOn(AndroidSchedulers.mainThread()).subscribe(new BaseObserver<LoginResults>(null) {
                @Override
                public void onSuccess(BaseHttpResult<LoginResults> result) {
                    LoginResults loginResults = result.getData();
                    SharePrefanceUtils.putStr(StartPageActivity.this,"token",loginResults.getToken());
                    MainActivity.gotoMainactivity(StartPageActivity.this);
                    finish();

                }

                @Override
                public void onFailure(String errMsg, boolean isNetError) {
                    LoginActivity.gotoLoginActivity(StartPageActivity.this);
                    finish();

                }
            });



        }else{
            LoginActivity.gotoLoginActivity(this);
            finish();
        }

    }


}
