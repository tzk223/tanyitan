package com.xt.xiaotan.mvp;

import android.app.Activity;

import com.trello.rxlifecycle2.LifecycleProvider;
import com.xt.xiaotan.net.BaseObserver;
import com.xt.xiaotan.utils.Preconditions;


import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public abstract class BasePresenter<M extends IModel,V extends IView> implements IPresenter<V> {

    private  V mView;
    private  M mModel;

    private CompositeDisposable compositeDisposable;

    public V getView() {
        Preconditions.checkNotNull(mView, "%s cannot be null", IView.class.getName());
        return mView;
    }

    public BasePresenter() {
        this.mModel =createModel();
    }

    public M getModel() {
        Preconditions.checkNotNull(mModel, "%s cannot be null", IModel.class.getName());
        return mModel;
    }

    /**
     * 获取 Model
     * @return
     */
    protected abstract M createModel();

    /**
     * 绑定 View
     * @param mView
     */
    @Override
    public void attachView(V mView) {
       this.mView = mView;
    }

    /**
     * 解除绑定
     */
    @Override
    public void detachView() {
        unDispose();
        mView = null;
    }

    /**
     * 检查View是否存在
     */
    protected boolean isViewAttached() {
        return mView != null;
    }

    /**
     * &#x5c06; {@link Disposable} &#x6dfb;&#x52a0;&#x5230; {@link CompositeDisposable} &#x4e2d;&#x7edf;&#x4e00;&#x7ba1;&#x7406;
     * &#x53ef;&#x5728; {@link Activity#onDestroy()} &#x4e2d;&#x4f7f;&#x7528; {@link #unDispose()} &#x505c;&#x6b62;&#x6b63;&#x5728;&#x6267;&#x884c;&#x7684; RxJava &#x4efb;&#x52a1;&#xff0c;&#x907f;&#x514d;&#x5185;&#x5b58;&#x6cc4;&#x6f0f;(&#x6846;&#x67b6;&#x5df2;&#x81ea;&#x884c;&#x5904;&#x7406;)
     *
     * @param disposable
     */
    public void addDispose(Observable<?> observable, BaseObserver baseObserver) {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        compositeDisposable.add(observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread()).subscribeWith(baseObserver));// 将所有 Disposable 放入集中处理
    }

    /**
     * 停止集合中正在执行的 RxJava 任务
     */
    public void unDispose() {
        if (compositeDisposable != null) {
            compositeDisposable.clear();// 保证 Activity 结束时取消所有正在执行的订阅
        }
    }


    protected <T> LifecycleProvider<T> getLifecycleProvider() {
        LifecycleProvider<T> provider = null;
        if (null != mView && mView instanceof LifecycleProvider) {
            provider = (LifecycleProvider<T>) mView;
        }
        return provider;
    }





}
