package com.hc.bluetoothlibrary.tootl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;


import java.util.ArrayList;
import java.util.Calendar;

import java.util.List;


import static android.os.ParcelFileDescriptor.MODE_APPEND;


public class DataMemory implements Parcelable {

    private SharedPreferences sp;
    private String collect = "collect";
    private String parameters = "parameters";
    private String level = "ModuleLevel";
    @SuppressLint("WrongConstant")
    public DataMemory(Context context){
        sp = context.getSharedPreferences("data",
                        MODE_APPEND| Context.MODE_PRIVATE);
    }

    protected DataMemory(Parcel in) {
        collect = in.readString();
        parameters = in.readString();
        level = in.readString();
    }

    public static final Creator<DataMemory> CREATOR = new Creator<DataMemory>() {
        @Override
        public DataMemory createFromParcel(Parcel in) {
            return new DataMemory(in);
        }

        @Override
        public DataMemory[] newArray(int size) {
            return new DataMemory[size];
        }
    };

    public void saveData(String mac, String name){
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(mac,name);
        editor.apply();
    }

    public String getData(String mac){
        return sp.getString(mac,null);
    }

    public void saveCollectData(String mac, String name){
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(collect+mac,name);
        editor.apply();
    }

    public String getCollectData(String mac){
        return sp.getString(collect+mac,null);
    }

    public void saveParameters(String data){
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(parameters,data);
        editor.apply();
    }

    public void saveModuleLevel(int value){
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(level,value);
        editor.apply();
    }

    public int getModuleLevel(){
        return sp.getInt(level,0);
    }

    public String getParameters(){
        return sp.getString(parameters,null);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(collect);
        dest.writeString(parameters);
        dest.writeString(level);
    }
}
